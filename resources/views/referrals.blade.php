@extends('layouts.app')

@push('css')
    <style>
        .jOrgChart {
            margin: 10px;
            padding: 20px;
        }

        .jOrgChart .node {
            font-size: 14px;
            background-color: #35363B;
            border-radius: 8px;
            border: 5px solid white;
            color: #F38630;
            -moz-border-radius: 8px;
        }

        .node label {
            font-family: tahoma;
            font-size: 14px;
            line-height: 11px;
            padding-top: 30px;
        }
    </style>
@endpush

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-3">
                <div class="card">
                    <div class="card-header">Dashboard</div>

                    <div class="card-body">
                        <ul>
                            <li>
                                <a href="{{ route('home') }}">Dashboard</a>
                            </li>
                            <li>
                                <a href="">Transcriptions</a>
                            </li>
                            <li>
                                <a href="">Withdraws</a>
                            </li>
                            <li>
                                <a href="{{ route('referrals') }}">Referrals</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col-md-9">

                <ul id="org" style="display:none">
                    <li>
                        <label>{{ Auth::user()->name }}</label>
                        {!! $tree !!}
                    </li>
                </ul>


                <div id="chart" style="height:500px;padding-bottom: 80px;" class="orgChart">
                    <div class="zoom">
                        <span class="zoom_control">+</span>

                        <div id="zoom_slider"></div>
                        <span class="zoom_control">-</span>
                    </div>
                </div>


            </div>
        </div>
    </div>
@endsection

@push('js')
    <script type="text/javascript">
        $(document).ready(function () {

            $("#org").jOrgChart({
                chartElement: '#chart',
                dragAndDrop: false,
                slider: true
            });

            $('#chart .cgsnode').tooltip();

            $('#chart').kinetic();
        });
    </script>
@endpush

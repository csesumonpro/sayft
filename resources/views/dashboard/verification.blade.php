@extends('layouts.home') 
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <blockquote class="blockquote">
                        <p><i>Account Verification</i></p>
                    </blockquote>
                </div>
            </div>
            @if(Auth::user()->verification_status=='pending')
            <h3 class="text-success">Your Account Verification Pending</h3>
            @elseif(Auth::user()->verification_status=='confirm')
            <h3 class="text-primary">Your Account Successfully Verified</h3>
            @elseif(Auth::user()->verification_status=='rejected')
            <h3 class="text-warning">Your Account Verification Rejected Please Submit Again</h3>
            <form action="{{route('dashboard.verify.request')}}" enctype="multipart/form-data" method="post">
                @csrf
                <h4 class="text-primary">Select At Least One For Verify Your Account</h4>
                <div class="form-group ">
                    <div class="form-check form-check-inline">
                        <label class="form-check-label" for="nid">
                                        <input class="form-check-input" type="radio" name="verification_type" id="nid" value="nid"> National ID Card
                                    </label>
                    </div>
                </div>
                <div class="form-group ">
                    <div class="form-check form-check-inline">
                        <label class="form-check-label" for="passport">
                                        <input class="form-check-input" type="radio" name="verification_type" id="Passport" value="national_passport">National Passport 
                                    </label>
                    </div>
                </div>
                <div class="form-group ">
                    <div class="form-check form-check-inline">
                        <label class="form-check-label" for="passport">
                                        <input class="form-check-input" type="radio" name="verification_type" id="Passport" value="international_passport">International Passport 
                                    </label>
                    </div>
                </div>
                <div class="form-group ">
                    <div class="form-check form-check-inline">
                        <label class="form-check-label" for="dl">
                                        <input class="form-check-input" type="radio" name="verification_type" id="dl" value="dl">Driving License 
                                    </label> @if ($errors->has('verification_type'))
                        <span class="invalid-feedback{{ $errors->has('verification_type') ? ' is-invalid' : '' }}" role="alert">
                                        <strong>{{ $errors->first('verification_type') }}</strong>
                                    </span> @endif
                    </div>
                </div>
                <div class="form-group">
                    <label for="verify_file">Upload File</label>
                    <input type="file" class="form-control-file {{ $errors->has('verification_image') ? ' is-invalid' : '' }}" name="verification_image"
                        id="verify_file"> @if ($errors->has('verification_image'))
                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('verification_image') }}</strong>
                                </span> @endif @if(Auth::user()->verification_image!=NULL)
                    <div>
                        <img src="/storage/{{Auth::user()->verification_image}}" alt="" width="100" height="100">
                    </div>
                    @endif
                </div>

                <div>
                    <button type="submit" class="btn btn-success">Submit</button>
                </div>
            </form>
            @else
            <form action="{{route('dashboard.verify.request')}}" enctype="multipart/form-data" method="post">
                @csrf
                <h4 class="text-primary">Select At Least One For Verify Your Account</h4>
                <div class="form-group ">
                    <div class="form-check form-check-inline">
                        <label class="form-check-label" for="nid">
                            <input class="form-check-input" type="radio" name="verification_type" id="nid" value="nid"> National ID Card
                        </label>
                    </div>
                </div>
                <div class="form-group ">
                    <div class="form-check form-check-inline">
                        <label class="form-check-label" for="passport">
                            <input class="form-check-input" type="radio" name="verification_type" id="Passport" value="national_passport">National Passport 
                        </label>
                    </div>
                </div>
                <div class="form-group ">
                    <div class="form-check form-check-inline">
                        <label class="form-check-label" for="passport">
                            <input class="form-check-input" type="radio" name="verification_type" id="Passport" value="international_passport">International Passport 
                        </label>
                    </div>
                </div>
                <div class="form-group ">
                    <div class="form-check form-check-inline">
                        <label class="form-check-label" for="dl">
                            <input class="form-check-input" type="radio" name="verification_type" id="dl" value="dl">Driving License 
                        </label> @if ($errors->has('verification_type'))
                        <span class="invalid-feedback{{ $errors->has('verification_type') ? ' is-invalid' : '' }}" role="alert">
                            <strong>{{ $errors->first('verification_type') }}</strong>
                        </span> @endif
                    </div>
                </div>
                <div class="form-group">
                    <label for="verify_file">Upload File</label>
                    <input type="file" class="form-control-file {{ $errors->has('verification_image') ? ' is-invalid' : '' }}" name="verification_image"
                        id="verify_file"> 
                        @if ($errors->has('verification_image'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('verification_image') }}</strong>
                    </span> 
                    @endif 
                    @if(Auth::user()->verification_image!=NULL)
                    <div>
                        <img src="/storage/{{Auth::user()->verification_image}}" alt="" width="100" height="100">
                    </div>
                    @endif
                </div>
                <div>
                    <button type="submit" class="btn btn-success">Submit</button>
                </div>
            </form>
            @endif
        </div>
    </div>
</div>
@endsection
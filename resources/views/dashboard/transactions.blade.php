@extends('layouts.home')


@section('content')
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="btn-group btn-breadcrumb breadcrumb-success">
                <a href="/dashboard" class="btn btn-successes"><i class="glyphicon glyphicon-home"></i></a>
                <a href="/dashboard" class="btn btn-successes visible-lg-block visible-md-block">Dashboard</a>
                <div class="btn btn-default visible-xs-block hidden-xs visible-sm-block ">...</div>
                <div class="btn mybtn" style="text-decoration: none;"><b>Transaction</b></div>
            </div>
            {{-- <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Transactions</h4></div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="#">Transactions</a></li>
                </ol>
            </div> --}}
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->

        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    {{-- <h3 class="box-title">All transactions</h3>
                    <p class="text-muted">Transactions list</p>
 --}}
                    <div class="table-responsive mytable">
                        <div class="tbl-header">
                            <table  cellpadding="0" cellspacing="0" border="0" style="background: #460101">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Amount</th>
                                    <th>Amount Type</th>
                                    <th>Type</th>
                                    <th>From</th>
                                    <th>To</th>
                                    <th>Status</th>
                                    <th>Date</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                        <div class="tbl-content">
                            <table cellpadding="0" cellspacing="0" border="0">
                            @foreach($transactions as $transaction)
                                <tr class="@if($transaction->id == $id) bg-warning @endif">
                                    <td>{{ $transaction->id }}</td>
                                    <td><strong>{{env('CURRENCY')}}{{ $transaction->amount }}</strong></td>
                                    <td>
                                        @if($transaction->amount_type == 'token')
                                            Cash
                                        @elseif($transaction->amount_type == 'coupon')
                                            Voucher
                                        @endif
                                    </td>
                                    <td>@if($transaction->type == 'referral_in') Referral
                                        Earn @elseif($transaction->type == 'withdraw')
                                            Withdraw @else {{ $transaction->type }} @endif</td>
                                    <td>{{ $transaction->from_user->username }}</td>
                                    <td>{{ $transaction->to_user ? $transaction->to_user->username : '' }}</td>
                                    <td><span
                                            class="label @if($transaction->status == 'pending') label-warning @elseif($transaction->status == 'accepted') label-success @elseif($transaction->status == 'rejected') label-danger @endif">{{ $transaction->status }}</span>
                                    </td>
                                    <td>{{ \Carbon\Carbon::parse($transaction->created_at)->toDateTimeString() }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->

    </div>
@endsection

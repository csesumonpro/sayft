@extends('layouts.home')

@section('content')
    <div class="container-fluid">
        <div class="row bg-title">
            {{-- <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Guidelines</h4></div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="#">Guidelines</a></li>
                </ol>
            </div> --}}

            <div class="btn-group btn-breadcrumb breadcrumb-success">
                <a href="/dashboard" class="btn btn-successes"><i class="glyphicon glyphicon-home"></i></a>
                <a href="/dashboard" class="btn btn-successes visible-lg-block visible-md-block">Dashboard</a>
                <div class="btn btn-default visible-xs-block hidden-xs visible-sm-block ">...</div>
                <div class="btn mybtn" style="text-decoration: none;"><b>Guidelines</b></div>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->

        <!-- .row -->
        <div class="row">
            @foreach($guidelines as $guideline)
                <div class="col-lg-4 col-sm-6 col-xs-12">
                   <!--  <div class="white-box analytics-info">
                        <h3 class="box-title">{{ $guideline->caption }}</h3>
                        <p>
                            {!! str_limit($guideline->content, 200) !!}
                        </p>
                        <p class="text-right">
                            <a href="{{ route('dashboard.read.details',[$guideline->id]) }}"
                               class="btn btn-sm btn-primary">Read more</a>
                        </p>
                    </div> -->

                     <div class="offer offer-radius offer-info">
                        <div class="shape">
                            <div class="shape-text">
                                learn                            
                            </div>
                        </div>
                        <div class="offer-content">
                            <h3 class="lead">
                               {{ $guideline->caption }}
                            </h3>                       
                            <p>
                                 {!! str_limit($guideline->content, 200) !!}
                               
                            </p>
                            <p class="text-right">
                                <a href="{{ route('dashboard.read.details',[$guideline->id]) }}"
                               class="btn btn-sm">Read more</a>
                            </p>
                        </div>
                    </div>

                </div>
            @endforeach
        </div>
        <!--/.row -->

    </div>
@endsection



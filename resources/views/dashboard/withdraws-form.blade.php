@extends('layouts.home')


@section('content')
    <div class="container-fluid">
        <div class="row bg-title">
           {{--  <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Withdraws</h4></div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="#">Withdraws</a></li>
                </ol>
            </div> --}}

            <div class="btn-group btn-breadcrumb breadcrumb-success">
                <a href="/dashboard" class="btn btn-successes"><i class="glyphicon glyphicon-home"></i></a>
                <a href="/dashboard" class="btn btn-successes visible-lg-block visible-md-block">Dashboard</a>
                <div class="btn btn-default visible-xs-block hidden-xs visible-sm-block ">...</div>
                <div class="btn mybtn" style="text-decoration: none;"><b>Withdraw</b></div>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->

        <!-- .row -->
        <div class="row">
            <div class="col-md-6 col-md-offset-1">
                <form action="{{ route('dashboard.withdraws.form.post', [$type,$amount_type,$coupon]) }}"
                      method="post">
                    @csrf
                    @if($type=='gift')
                        <div class="form-group">
                            <label for="username">Username</label>
                            <input type="text" required class="form-control" name="username" id="username"
                                   placeholder="eg. jonedao">
                        </div>
                    @endif

                    <div class="form-group">
                        <label for="amount">Amount</label>
                        <input type="text" required name="amount" class="form-control" id="amount"
                               placeholder="eg. 100.00" @if($coupon) readonly="" value="{{ $coupon->amount }}" @endif>
                    </div>

                    <div>
                        <button type="submit" class="btn btn-danger">@if($type=='gift') Send @else
                                Withdraw @endif</button>
                    </div>

                </form>
            </div>
        </div>
        <!--/.row -->
    </div>
@endsection

@extends('layouts.home') 
@section('content')
<div class="container-fluid">
    <div class="row bg-title">
        {{--
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Support</h4>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="#">Support</a></li>
            </ol>
        </div> --}}

        <div class="btn-group btn-breadcrumb breadcrumb-success">
            <a href="/dashboard" class="btn btn-successes"><i class="glyphicon glyphicon-home"></i></a>
            <a href="/dashboard" class="btn btn-successes visible-lg-block visible-md-block">Dashboard</a>
            <div class="btn btn-default visible-xs-block hidden-xs visible-sm-block ">...</div>
            <div class="btn mybtn" style="text-decoration: none;"><b>Support</b></div>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->

    <!-- .row -->
    <div class="row">
        <div class="col-md-6 col-md-offset-1">
            <h4 class="text-center  alert-success">{{Session::get('message_success')}}</h4>
            <h4 class="text-center  alert-danger">{{Session::get('message_error')}}</h4>
            <form action="{{ route('dashboard.updatepassword') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="password">Current Password</label>
                    <input placeholder="Current Password" id="password" type="text" class="form-control{{ $errors->has('old_password') ? ' is-invalid' : '' }}"
                        name="old_password" required autofocus> @if ($errors->has('old_password'))
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('old_password') }}</strong>
                    </span> @endif
                </div>
                <div class="form-group">
                    <label for="new_password">New Password</label>
                    <input placeholder="New Password" id="new_password" type="text" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                        name="password" required autofocus> @if ($errors->has('password'))
                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('password') }}</strong>
                    </span> @endif
                </div>
                <div class="form-group">
                    <label for="confirm_password">Confirm Password</label>
                    <input placeholder="Confirm Password" id="confirm_password" type="text" class="form-control{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}"
                        name="password_confirmation" required autofocus> @if ($errors->has('password_confirmation'))
                    <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span> @endif
                </div>

                <div>
                    <button type="submit" class="btn btn-success">Submit</button>
                </div>

            </form>
        </div>
    </div>
    <!--/.row -->

</div>
@endsection
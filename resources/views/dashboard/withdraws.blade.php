@extends('layouts.home')


@section('content')
    <div class="container-fluid">
        <div class="row bg-title">
            {{-- <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Withdraws</h4></div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="#">Withdraws</a></li>
                </ol>
            </div> --}}

            <div class="btn-group btn-breadcrumb breadcrumb-success">
                <a href="/dashboard" class="btn btn-successes"><i class="glyphicon glyphicon-home"></i></a>
                <a href="/dashboard" class="btn btn-successes visible-lg-block visible-md-block">Dashboard</a>
                <div class="btn btn-default visible-xs-block hidden-xs visible-sm-block ">...</div>
                <div class="btn mybtn" style="text-decoration: none;"><b>Withdraw</b></div>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->

        <!-- .row -->
        <div class="row">
            <div class="col-lg-4 col-sm-6 col-xs-12">
                <div class="white-box analytics-info">
                    <h3 class="box-title">Total Cash</h3>
                    <ul class="list-inline two-part">
                        <li>
                            <div id="sparklinedash"></div>
                        </li>
                        <li class="text-right"><i class="ti-arrow-up text-success"></i> <span
                                class="counter text-success">{{ sprintf('%0.2f', $token) }}</span></li>
                        <li></li>
                        <li>
                            <a href="{{ route('dashboard.withdraws.form', ['type' => 'withdraw','amount_type' => 'token']) }}">
                                <button class="btn btn-xs btn-success">Withdraw</button>
                            </a>
                        {{--    <a href="{{ route('dashboard.withdraws.form', ['type' => 'gift','amount_type' => 'token']) }}">
                                <button class="btn btn-xs btn-primary">Gift</button>
                            </a>--}}
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6 col-xs-12">
                <div class="white-box analytics-info">
                    <h3 class="box-title">Total Voucher</h3>
                    <ul class="list-inline two-part">
                        <li>
                            <div id="sparklinedash2"></div>
                        </li>
                        <li class="text-right"><i class="ti-arrow-up text-purple"></i> <span
                                class="counter text-purple">{{ sprintf('%0.2f', $coupons->sum('amount')) }}</span></li>
                        <li></li>
                        <li>
                            <a href="{{ route('dashboard.withdraws.form', ['type' => 'withdraw','amount_type' => 'coupon']) }}">
                                <button class="btn btn-xs btn-success">Withdraw</button>
                            </a>
                            {{-- <a href="{{ route('dashboard.withdraws.form', ['type' => 'gift','amount_type' => 'coupon']) }}">
                                 <button class="btn btn-xs btn-primary">Gift</button>
                             </a>--}}
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!--/.row -->

        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <h3 class="box-title">All voucher</h3>
                    <p class="text-muted">voucher list</p>
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Amount</th>
                                <th>Start</th>
                                <th>End</th>
                                <th>Note</th>
                                <th>Earned at</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($coupons as $coupon)
                                <tr>
                                    <td>{{ $coupon->id }}</td>
                                    <td><strong>{{env('CURRENCY')}}{{ $coupon->amount }}</strong></td>
                                    <td>{{ $coupon->start }}</td>
                                    <td>{{ $coupon->end }}</td>
                                    <td>{{ $coupon->note }}</td>
                                    <td>{{ \Carbon\Carbon::parse($coupon->created_at)->toDateTimeString() }}</td>
                                    <td>
                                        <a href="{{ route('dashboard.withdraws.form', ['type' => 'gift','amount_type' => 'coupon','coupon' => $coupon->id]) }}">
                                            <button class="btn btn-xs btn-primary">Gift</button>
                                        </a>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="7" class="text-center">No entries found</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
    </div>
@endsection

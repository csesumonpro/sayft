@extends('layouts.home')


@section('content')
    <div class="container-fluid">
        <div class="row bg-title">
            {{-- <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Dashboard</h4></div> --}}

                <div class="btn-group btn-breadcrumb breadcrumb-success">
                    <a href="/dashboard" class="btn btn-successes"><i class="glyphicon glyphicon-home"></i></a>
                    <div class="btn mybtn" style="text-decoration: none;"><b>Dashboard</b></div>
                    
                    <div class="btn btn-default visible-xs-block hidden-xs visible-sm-block ">...</div>
                </div>

                {{-- <div class="btn-group btn-breadcrumb breadcrumb-default">
                    <a href="#" class="btn btn-default"><i class="glyphicon glyphicon-home"></i></a>
                    
                    <a href="#" class="btn btn-default visible-lg-block visible-md-block">Snippets</a>
                    
                    <div class="btn btn-default visible-xs-block hidden-xs visible-sm-block ">...</div>
                    <div class="btn btn-info"><b>Item Actual</b></div>
                </div> --}}
                
            <!-- /.col-lg-12 -->
        </div>

        <div class="row">
            
        </div>
        <!-- /.row -->
        <!-- ============================================================== -->
        <!-- Different data widgets -->
        <!-- ============================================================== -->
        <!-- .row -->
        <div class="row">
            <div class="col-lg-4 col-sm-6 col-xs-12">
                <div class="white-box analytics-info">
                    <h3 class="box-title">Cash</h3>
                    <ul class="list-inline two-part">
                        <li>
                            <div id="sparklinedash"></div>
                        </li>
                        <li class="text-right"><i class="ti-arrow-up text-success"></i> <span
                                class="counter text-success">{{ $token }}</span></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6 col-xs-12">
                <div class="white-box analytics-info">
                    <h3 class="box-title">Voucher</h3>
                    <ul class="list-inline two-part">
                        <li>
                            <div id="sparklinedash2"></div>
                        </li>
                        <li class="text-right"><i class="ti-arrow-up text-purple"></i> <span
                                class="counter text-purple">{{ sprintf('%0.2f', $coupons->sum('amount')) }}</span></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6 col-xs-12">
                <div class="white-box analytics-info">
                    <h3 class="box-title">Total Earn with bonus</h3>
                    <ul class="list-inline two-part">
                        <li>
                            <div id="sparklinedash3"></div>
                        </li>
                        <li class="text-right"><i class="ti-arrow-up text-info"></i> <span
                                class="counter text-info">{{ $totalReturn }}</span></li>
                    </ul>
                </div>
            </div>


            <div class="col-lg-12 col-sm-12 col-xs-12">
                <div class="white-box analytics-info text-center">
                    <h3 class="box-title">Total Received in lifetime</h3>
                    <h1 class="text-success">&pound;{{ $totalEarn }}</h1>
                </div>
            </div>

        </div>
        <!--/.row -->
        <!--row -->
        <!-- /.row -->


        <!-- .row -->
        <div class="row">
            @foreach($news as $item)

                <div class="col-lg-4 col-sm-6 col-xs-12">
                   <!--  <section class="panel panel-success">
                      <header class="panel-heading">
                       <h5 class="panel-title">{{ $item->title }}</h5>
                      </header>
                      <div class="panel-body">
                       <p>{!! str_limit($item->content, 200) !!}</p>
                       <a href="{{ route('dashboard.read.details', [$item->id, 'news']) }}"
                               class="btn btn-sm btn-primary">Read more</a>
                      </div>
                    </section> -->

                    <div class="offer offer-radius offer-primary">
                        <div class="shape">
                            <div class="shape-text">
                                news                             
                            </div>
                        </div>
                        <div class="offer-content">
                            <h3 class="lead">
                                {{ $item->title }}
                            </h3>                       
                            <p>
                                {!! str_limit($item->content, 200) !!}
                               
                            </p>
                            <p class="text-right">
                                 <a href="{{ route('dashboard.read.details', [$item->id, 'news']) }}"
                               class="btn btn-sm">Read more</a>
                            </p>
                        </div>
                    </div>

                   <!--  <div class="white-box analytics-info">
                        <h3 class="box-title">{{ $item->title }}</h3>
                        <p>
                            {!! str_limit($item->content, 200) !!}
                        </p>
                        <p class="text-right">
                            <a href="{{ route('dashboard.read.details', [$item->id, 'news']) }}"
                               class="btn btn-sm btn-primary">Read more</a>
                        </p>
                    </div> -->
                </div>
            @endforeach
        </div>
        <!--/.row -->


    </div>
    <!-- /.container-fluid -->
@endsection

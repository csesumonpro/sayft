@extends('layouts.home')

@section('content')
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="btn-group btn-breadcrumb breadcrumb-success">
                <a href="/dashboard" class="btn btn-successes"><i class="glyphicon glyphicon-home"></i></a>
                <a href="/dashboard" class="btn btn-successes visible-lg-block visible-md-block">Dashboard</a>
                <div class="btn btn-default visible-xs-block hidden-xs visible-sm-block ">...</div>
                <div class="btn mybtn" style="text-decoration: none;"><b>Details</b></div>
            </div>
            {{-- <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Details</h4></div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="#">Details</a></li>
                </ol>
            </div> --}}
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->

        <!-- .row -->
        <div class="row">
            <div class="col-lg-12 col-sm-12 col-xs-12">

               
                <div class="white-box analytics-info">
                    <h3 class="box-title">{{ $type == 'news'? $item->title : $item->caption }}</h3>
                    <div>
                        {!! $item->content !!}
                    </div>
                </div>
            </div>
        </div>
        <!--/.row -->

    </div>
@endsection



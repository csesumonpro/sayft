@extends('layouts.home')


@section('content')
    <div class="container-fluid">
        <div class="row bg-title">
            {{-- <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Support</h4></div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="#">Support</a></li>
                </ol>
            </div> --}}

            <div class="btn-group btn-breadcrumb breadcrumb-success">
                <a href="/dashboard" class="btn btn-successes"><i class="glyphicon glyphicon-home"></i></a>
                <a href="/dashboard" class="btn btn-successes visible-lg-block visible-md-block">Dashboard</a>
                <div class="btn btn-default visible-xs-block hidden-xs visible-sm-block ">...</div>
                <div class="btn mybtn" style="text-decoration: none;"><b>Support</b></div>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->

        <!-- .row -->
        <div class="row">
            <div class="col-md-6 col-md-offset-1">
                <form action="{{ route('dashboard.support.post') }}"
                      method="post">
                    @csrf
                    <div class="form-group">
                        <label for="subject">Subject</label>
                        <select name="subject" id="subject" class="form-control">
                            <option value="payment">Payment Issue</option>
                            <option value="general">General Issue</option>
                            <option value="other">Others</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="description">Description</label>

                        <textarea name="description" id="description" cols="30" rows="5"
                                  class="form-control"></textarea>
                    </div>

                    <div>
                        <button type="submit" class="btn btn-success">Submit</button>
                    </div>

                </form>
            </div>
        </div>
        <!--/.row -->

    </div>
@endsection

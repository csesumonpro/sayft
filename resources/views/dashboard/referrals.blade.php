@extends('layouts.home')

@push('css')
    <style>

/* Basic styling */
/* Draw the lines */
.jOrgChart .line {
  height                : 20px;
  width                 : 4px;
}

.jOrgChart .down {
  background-color      : black;    
  margin                : 0px auto;
}

.jOrgChart .top {
  border-top          : 3px solid black;
}

.jOrgChart .left {
  border-right          : 2px solid black;
}

.jOrgChart .right {
  border-left           : 2px solid black;
}

/* node cell */
.jOrgChart td {
  text-align            : center;
  vertical-align        : top;
  padding               : 0;
}

/* The node */
.jOrgChart .node {
  background-color      : #35363B;
  display               : inline-block;
  width                 : 96px;
  height                : 60px;
  z-index               : 10;
  margin               : 0 2px;
  width: 150px;
height: 150px;
}

/* jQuery drag 'n drop */

.drag-active {
  border-style          : dotted !important;
}

.drop-hover {
  border-style          : solid !important;
  border-color          : #E05E00 !important;
}


        .jOrgChart {
            margin: 20px;
            padding: 20px;
        }

        .jOrgChart table {
            margin: auto;
        }

        .jOrgChart .node {
/*            font-size: 14px;*/
            background-color: transparent;
/*            border-radius: 8px;
            border: 5px solid white;
            color: #F38630;
            -moz-border-radius: 8px;

            text-align: center;*/
        }

        .node label {
    font-family: tahoma;
    font-size: 14px;
    line-height: 11px;
    padding-top: 10px;
    padding-bottom: 10px;
    text-align: center;
}
    </style>
@endpush

@section('content')

    <div class="container-fluid">
        <div class="row bg-title">
          <div class="btn-group btn-breadcrumb breadcrumb-success">
                <a href="/dashboard" class="btn btn-successes"><i class="glyphicon glyphicon-home"></i></a>
                <a href="/dashboard" class="btn btn-successes visible-lg-block visible-md-block">Dashboard</a>
                <div class="btn btn-default visible-xs-block hidden-xs visible-sm-block ">...</div>
                <div class="btn mybtn" style="text-decoration: none;"><b>My Team</b></div>
            </div>
            {{-- <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">My Team</h4></div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="#">My Team</a></li>
                </ol>
            </div> --}}
        </div>


        <div class="row">
            <div>
                <ul id="org" style="display:none">
                    <li>
                        <label>{{ Auth::user()->name }} <br>

<img src="{{ asset('storage'.'/'.Auth::user()->invest_amount->package->badge) }}"
                                width="100px"
                                height="100px">

                        </label>
                        {!! $tree !!}
                    </li>
                </ul>

                <div id="chart" style="height:500px;padding-bottom: 80px;" class="orgChart">
                    <div class="zoom">
                        <span class="zoom_control">+</span>

                        <div id="zoom_slider"></div>
                        <span class="zoom_control">-</span>
                    </div>
                </div>
            </div>

        </div>

        <div class="row">
            <div class="container">
                <h5>Specification</h5>
                <img src="/storage/images/all-package.png"
                                
                                height="100px">
            </div>
        </div>


    </div>
@endsection

@push('js')
    <script type="text/javascript">
        $(document).ready(function () {

            $("#org").jOrgChart({
                chartElement: '#chart',
                dragAndDrop: false,
                slider: true
            });


            $('#chart .cgsnode').tooltip();

            $('#chart').kinetic();
        });
    </script>
@endpush




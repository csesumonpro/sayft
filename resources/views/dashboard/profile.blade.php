@extends('layouts.home') 
@section('content')
<div class="container-fluid">
    <div class="row bg-title">
        {{--
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Support</h4>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="#">Support</a></li>
            </ol>
        </div> --}}

        <div class="btn-group btn-breadcrumb breadcrumb-success">
            <a href="/dashboard" class="btn btn-successes"><i class="glyphicon glyphicon-home"></i></a>
            <a href="/dashboard" class="btn btn-successes visible-lg-block visible-md-block">Dashboard</a>
            <div class="btn btn-default visible-xs-block hidden-xs visible-sm-block ">...</div>
            <div class="btn mybtn" style="text-decoration: none;"><b>Support</b></div>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->

    <!-- .row -->
    <div class="row">
        <div class="col-md-6 col-md-offset-1">
            <form action="{{ route('dashboard.profile.update') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="first_name">First Name</label>
                    <input id="first_name" type="text" class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}" name="first_name"
                        value="{{$user->first_name}}" required autofocus> @if ($errors->has('first_name'))
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                    </span> @endif
                </div>
                <div class="form-group">
                    <label for="last_name">Last Name</label>
                    <input id="last_name" type="text" class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name"
                        value="{{$user->last_name}}" required autofocus> @if ($errors->has('last_name'))
                    <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('last_name') }}</strong>
                            </span> @endif
                </div>
                <div class="form-group">
                    <label for="phone">Phone</label>
                    <input id="phone" type="number" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" value="{{$user->phone}}"
                        required autofocus> @if ($errors->has('phone'))
                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span> @endif
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input id="email" readonly type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email"
                        value="{{$user->email}}" autofocus> @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span> @endif
                </div>

                <div class="form-group">
                    <label for="photo">Photo</label>
                    <input class="form-control{{ $errors->has('photo') ? ' is-invalid' : '' }}" type="file" name="photo" id="photo">                    @if ($errors->has('photo'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('photo') }}</strong>
                </span> @endif @if ($user->photo!=NULL)
                    <img src="/storage/{{$user->photo}}" alt="{{$user->first_name}}" class="img-responsive" width="100" height="100">                    @else
                    <i class="fa fa-user fa-4x" aria-hidden="true"></i> @endif
                </div>
                <div>
                    <button type="submit" class="btn btn-success">Submit</button>
                </div>

            </form>
        </div>
    </div>
    <!--/.row -->

</div>
@endsection
@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-3">
                <div class="card">
                    <div class="card-header">Dashboard</div>

                    <div class="card-body">
                        <ul>
                            <li>
                                <a href="{{ route('home') }}">Dashboard</a>
                            </li>
                            <li>
                                <a href="">Transcriptions</a>
                            </li>
                            <li>
                                <a href="">Withdraws</a>
                            </li>
                            <li>
                                <a href="{{ route('referrals') }}">Referrals</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col-md-9">
                <h1>Latest news</h1>

                <section>
                    @foreach(\App\News::all() as $news)
                        <h3>{{ $news->title }}</h3>
                        {!! $news->content !!}
                        <hr>
                    @endforeach
                </section>

                <hr>

                <h1>Training Contents</h1>

                <section>
                    @foreach(\App\TrainingContent::all() as $content)
                        <h2>{{ $content->caption }}</h2>
                        {!! $content->content !!}
                        <hr>
                    @endforeach
                </section>

            </div>
        </div>
    </div>
@endsection

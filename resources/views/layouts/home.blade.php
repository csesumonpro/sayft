<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Sayft | Anwar & Rono">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('images/sayft-logo.jpg') }}">
    <title>Sayft | Member Dashboard</title>
    <!-- Bootstrap Core CSS -->
    <link href="{{ asset('front/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Menu CSS -->
    <link href="{{ asset('front/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css') }}" rel="stylesheet">
    <!-- toast CSS -->
    <link href="{{ asset('front/plugins/bower_components/toast-master/css/jquery.toast.css') }}" rel="stylesheet">
    <!-- morris CSS -->
    <link href="{{ asset('front/plugins/bower_components/morrisjs/morris.css') }}" rel="stylesheet">
    <!-- chartist CSS -->
    <link href="{{ asset('front/plugins/bower_components/chartist-js/dist/chartist.min.css') }}" rel="stylesheet">
    <link href="{{ asset('front/plugins/bower_components/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css') }}"
        rel="stylesheet">
    <!-- animation CSS -->
    <link href="{{ asset('front/css/animate.css') }}" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="{{ asset('front/css/style.css') }}" rel="stylesheet">

    <!-- color CSS -->
    <link href="{{ asset('front/css/colors/default.css') }}" id="theme" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script src="{{ asset('js/app.js') }}" defer></script>

    @stack('css')
</head>

<body class="fix-header">
    <!-- ============================================================== -->
    <!-- Preloader -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/>
    </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Wrapper -->
    <!-- ============================================================== -->
    <div id="wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <nav class="navbar navbar-default navbar-static-top m-b-0">
            <div class="navbar-header" style="    background: #800000;">
                <div class="top-left-part" style="background: #800000;text-align: center;">
                    <!-- Logo -->
                    <a class="logo" href="{{ url('/') }}">
                    <img src="{{ asset('images/sayft-logo.jpg') }}"
                         alt="home"
                         class="dark-logo"/><!--This is light logo text--><img
                        src="{{ asset('images/sayft-logo.jpg') }}" alt="home"
                        class="light-logo"/>
                    </span> </a>
                </div>
                <!-- /Logo -->
                <ul class="nav navbar-top-links navbar-right pull-right">
                    <li>
                        <form role="search" class="app-search hidden-sm hidden-xs m-r-10">
                            <input type="text" placeholder="Search..." class="form-control"> <a href=""><i
                                class="fa fa-search"></i></a></form>
                    </li>

                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" @if(Auth::user()->unreadNotifications->count()) @click="readAll()" @endif>
                        <i class="fa fa-bell-o"></i>

                        @if(Auth::user()->unreadNotifications->count())
                            <span class="label label-warning">
                                {{ Auth::user()->unreadNotifications->count() }}
                            </span>
                        @endif

                    </a>
                        <ul class="dropdown-menu notification-menu fadeInDown" style="width: 300px;">
                            <li class="header">
                                <h3 class="text-center notification">Notification</h3>


                            </li>
                            <li>
                                <!-- inner menu: contains the actual data -->
                                <ul class="menu notification-menu-item">

                                    @foreach(Auth::user()->notifications as $notification)
                                    <li>
                                        <a href="{{ route('dashboard.transactions', $notification->data['id']) }}">
                                            {{-- <i class="fa fa-users text-aqua"></i> --}} {{ $notification->data['title'] }}

                                            <h6 class="time"> <i class="fa fa-clock-o text-aqua"></i> {{ \Carbon\Carbon::parse($notification->created_at)->diffForHumans() }}</h6>
                                        </a> {{-- <br> --}}


                                    </li>
                                    @endforeach
                                </ul>
                            </li>

                            <li>

                                <p class="text-center" style="margin-top: 10px;">
                                    @if(Auth::user()->unreadNotifications->count()) You have {{ Auth::user()->unreadNotifications->count() }} unread notifications
                                    @else You don't have any new notification @endif
                                </p>
                            </li>
                            {{--
                            <li class="footer"><a href="#">View all</a></li>--}}
                        </ul>
                    </li>



                    <li class="nav-item dropdown">

                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="" role="button" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false" v-pre>
                                <span class="caret"></span>
                               @if (Auth::user()->photo!=NULL)
                                   <img src="{{ asset('storage'.'/'.Auth::user()->photo) }}" alt="user-img" width="36" class="img-circle"><b class="hidden-xs">{{ Auth::user()->name }}</b>
                               @else
                                    <i class="fa fa-user fa-2x" aria-hidden="true"></i><b class="hidden-xs">{{ Auth::user()->name }}</b>
                               @endif
                            </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                                         document.getElementById('logout-form').submit();">
                                                        {{ __('Logout') }}
                                </a>
                            <a class="dropdown-item" href="{{ route('dashboard.profile') }}"> {{ __('Profile') }} </a>
                            <a class="dropdown-item" href="{{ route('dashboard.changepassword') }}"> 
                            {{ __('Change Password') }} </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-header -->
            <!-- /.navbar-top-links -->
            <!-- /.navbar-static-side -->
        </nav>
        <!-- End Top Navigation -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->

        <style>
            .nav>li>a.active,
            .nav>li>a {
                color: #ffffff !important;
            }
        </style>
        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav slimscrollsidebar" style="background: #640000;">
                <div class="sidebar-head">
                    <h3><span class="fa-fw open-close"><i class="ti-close ti-menu"></i></span> <span class="hide-menu">Navigation</span>
                    </h3>
                </div>
                <ul class="nav" id="side-menu">
                    <li style="padding: 70px 0 0;">
                        <a href="{{ route('dashboard.dash') }}" class="waves-effect"><i class="fa fa-clock-o fa-fw"
                                                                                    aria-hidden="true"></i>Dashboard</a>
                    </li>
                    <li>
                        <a href="{{ route('dashboard.transactions') }}" class="waves-effect"><i class="fa fa-user fa-fw"
                                                                                            aria-hidden="true"></i>Transactions</a>
                    </li>
                    <li>
                        <a href="{{ route('dashboard.withdraws') }}" class="waves-effect"><i class="fa fa-table fa-fw"
                                                                                         aria-hidden="true"></i>Withdraws</a>
                    </li>
                    <li>
                        <a href="{{ route('dashboard.referrals') }}" class="waves-effect"><i class="fa fa-font fa-fw"
                                                                                         aria-hidden="true"></i>My Team</a>
                    </li>

                    <li>
                        <a href="{{ route('dashboard.guideline') }}" class="waves-effect"><i class="fa fa-book fa-fw"
                                                                                         aria-hidden="true"></i>Guideline</a>
                    </li>
                    <li>
                        <a href="{{ route('dashboard.verify') }}" class="waves-effect"><i class="fa fa-user fa-fw"
                                                                                                             aria-hidden="true"></i>Verify Account</a>
                    </li>

                    <li>
                        <a href="{{ route('dashboard.support') }}" class="waves-effect"><i class="fa fa-book fa-fw"
                                                                                       aria-hidden="true"></i>Support</a>
                    </li>

                </ul>
                <div class="center p-20 logout-btn">
                    <a href="{{ route('logout') }}" class="btn btn-danger btn-block " onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">Logout</a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
            </div>

        </div>
        <!-- ============================================================== -->
        <!-- End Left Sidebar -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page Content -->
        <!-- ============================================================== -->
        <div id="page-wrapper">
            @yield('content')
            <footer class="footer text-center"> 2018 &copy; sayft.co.uk</footer>
        </div>
        <!-- ============================================================== -->
        <!-- End Page Content -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="{{ asset('front/plugins/bower_components/jquery/dist/jquery.min.js') }}"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="{{ asset('front/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <!-- Menu Plugin JavaScript -->
    <script src="{{ asset('front/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js') }}"></script>
    <!--slimscroll JavaScript -->
    <script src="{{ asset('front/js/jquery.slimscroll.js') }}"></script>
    <!--Wave Effects -->
    <script src="{{ asset('front/js/waves.js') }}"></script>
    <!--Counter js -->
    <script src="{{ asset('front/plugins/bower_components/waypoints/lib/jquery.waypoints.js') }}"></script>
    <script src="{{ asset('front/plugins/bower_components/counterup/jquery.counterup.min.js') }}"></script>
    <!-- chartist chart -->
    <script src="{{ asset('front/plugins/bower_components/chartist-js/dist/chartist.min.js') }}"></script>
    <script src="{{ asset('front/plugins/bower_components/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js') }}"></script>
    <!-- Sparkline chart JavaScript -->
    <script src="{{ asset('front/plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js') }}"></script>
    <!-- Custom Theme JavaScript -->
    <script src="{{ asset('front/js/custom.min.js') }}"></script>
    <script src="{{ asset('front/js/dashboard1.js') }}"></script>
    <script src="{{ asset('front/plugins/bower_components/toast-master/js/jquery.toast.js') }}"></script>

    <script src='{{ asset(' js/jquery.jOrgChart.js ') }}'></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.9/jquery-ui.min.js"></script>
    <script src='{{ asset(' js/tree.js ') }}'></script>
    <script src='{{ asset(' js/jquery.kinetic.js ') }}'></script>

    @stack('js')
</body>

</html>
<div class="box">
    <div class="box-header">
        <h3 class="box-title"></h3>

        <div class="box-tools">
            <div class="input-group input-group-sm" style="width: 150px;">
                <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                </div>
            </div>
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body table-responsive no-padding">
        <table class="table table-hover">
            <tr>
                <th>Name</th>
                <th>Phone</th>
                <th>Email</th>
                <th>Username</th>
                <th>Joining Date</th>
                <th>Amount</th>
                <th>amount remaining</th>
                <th>Last Month Paid</th>
                <th>Total made including amount</th>
                <th>percentage</th>
                <th>Actions</th>
            </tr>

            @foreach(\App\User::all() as $investor)
                <tr>
                    <td>{{ $investor->name }}</td>
                    <td>{{ $investor->phone }}</td>
                    <td>{{ $investor->email }}</td>
                    <td>{{ $investor->username }}</td>
                    <td>{{ \Carbon\Carbon::parse($investor->created_at)->toDateString() }}</td>
                    <td>${{ $investor->invest_amount->package->amount }}<span
                            class="label label-warning">{{ $investor->invest_amount->package->title }}</span></td>

                    @php
                        $totalReturn = $investor->additional_returns()->sum('amount') ?? 0.00;
                        $lastPaid = $investor->additional_returns()->latest()->first()->amount ?? '-.--';
                    @endphp

                    <td>
                        ${{ $investor->invest_amount->package->amount - $totalReturn }}</td>
                    <td>${{$lastPaid}}
                        @if($investor->additional_returns()->latest()->first())
                            <span
                                class="label label-primary">{{ \Carbon\Carbon::parse($investor->additional_returns()->latest()->first()->month)->format('M') }}</span>
                        @endif
                    </td>
                    <td>${{ $totalReturn }}</td>
                    <td>
                        {{ $totalReturn / $investor->invest_amount->package->amount * 100 }}
                        %
                    </td>
                    <td>
                        <a href="{{ route('user.returns.details', $investor->username) }}">
                            <button class="btn btn-sm btn-danger"><i class="fa fa-edit"></i></button>
                        </a>
                    </td>
                </tr>
            @endforeach

        </table>
    </div>
    <!-- /.box-body -->
</div>

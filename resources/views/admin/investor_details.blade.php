<div class="row">
    <div class="col-md-3">

        <!-- Profile Image -->
        <div class="box box-primary">
            <div class="box-body box-profile">
                <img class="profile-user-img img-responsive img-circle"
                     src="{{ asset('storage'.'/'.$investor->photo) }}"
                     alt="User profile picture">

                <h3 class="profile-username text-center">{{ $investor->name }}</h3>

                <p class="text-muted text-center">{{ $investor->username }}</p>

                <ul class="list-group list-group-unbordered">
                    <li class="list-group-item">
                        <b>Tokens</b> <a class="pull-right">${{ $investor->token->balance }}</a>
                    </li>
                    <li class="list-group-item">
                        <b>Coupons</b> <a
                            class="pull-right">${{ sprintf('%0.2f', $investor->coupons()->sum('amount')) }}</a>
                    </li>
                    <li class="list-group-item">
                        <b>Amount with bonus</b> <a
                            class="pull-right">£{{ $investor->additional_returns()->sum('amount') }}</a>
                    </li>
                </ul>

                <a href="#" class="btn btn-primary btn-block"><b>Follow</b></a>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->

        <!-- About Me Box -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">About Investor</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <strong><i class="fa fa-book margin-r-5"></i> Contacts</strong>

                <p class="text-muted">
                    Mobile: {{ $investor->phone  }} <br>
                    Email: {{ $investor->email }}
                </p>

                <hr>

                <strong><i class="fa fa-book margin-r-5"></i> Package Information</strong>

                <p class="text-muted">
                    Package Name: £{{ $investor->invest_amount->package->amount  }} <span
                        class="label label-warning">{{ $investor->invest_amount->package->title  }}</span>
                </p>

                <p class="text-muted">
                    Additional Returns: {{ $investor->invest_amount->package->additional_returns  }} <span
                        class="label label-primary">{{ $investor->invest_amount->package->total_months  }} months</span>
                </p>

                <p>
                    Referral Bonus: <br>
                    £{{$investor->invest_amount->package->referral_bonus['first_level']}} <span
                        class="label label-danger">1<sup>st</sup></span> <br>
                    £{{$investor->invest_amount->package->referral_bonus['second_level']}} <span
                        class="label label-warning">2<sup>nd</sup></span> <br>
                    £{{$investor->invest_amount->package->referral_bonus['third_level']}} <span
                        class="label label-info">3<sup>rd</sup></span>
                </p>

                <hr>

                {{--<strong><i class="fa fa-map-marker margin-r-5"></i> Location</strong>

                <p class="text-muted">Malibu, California</p>

                <hr>

                <strong><i class="fa fa-pencil margin-r-5"></i> Skills</strong>

                <p>
                    <span class="label label-danger">UI Design</span>
                    <span class="label label-success">Coding</span>
                    <span class="label label-info">Javascript</span>
                    <span class="label label-warning">PHP</span>
                    <span class="label label-primary">Node.js</span>
                </p>

                <hr>--}}

                {{-- <strong><i class="fa fa-file-text-o margin-r-5"></i> Notes</strong>

                 <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam fermentum enim neque.</p>--}}
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
    <div class="col-md-9">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#timeline" data-toggle="tab">History</a></li>
                <li><a href="#settings" data-toggle="tab">Settings</a></li>
            </ul>
            <div class="tab-content">
                <!-- /.tab-pane -->
                <div class="active tab-pane" id="timeline">
                    <!-- The timeline -->
                    <ul class="timeline timeline-inverse">

                    @foreach($investor->additional_returns as $return)
                        <!-- timeline time label -->
                            <li class="time-label">
                        <span class="bg-red">
                          {{--10 Feb. 2014--}}
                            {{ $return->created_at }}
                        </span>
                            </li>
                            <!-- /.timeline-label -->
                            <!-- timeline item -->
                            <li>
                                <i class="fa fa-envelope bg-blue"></i>

                                <div class="timeline-item">
                                    <span class="time"><i class="fa fa-clock-o"></i> 12:05</span>

                                    <h3 class="timeline-header">Returns with bonus <strong
                                            class="text-success">${{ $return->amount }}</strong></h3>

                                    {{--<div class="timeline-body">
                                        Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles,
                                        weebly ning heekya handango imeem plugg dopplr jibjab, movity
                                        jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle
                                        quora plaxo ideeli hulu weebly balihoo...
                                    </div>
                                    <div class="timeline-footer">
                                        <a class="btn btn-primary btn-xs">Read more</a>
                                        <a class="btn btn-danger btn-xs">Delete</a>
                                    </div>--}}
                                </div>
                            </li>
                            <!-- END timeline item -->
                        @endforeach


                        <li>
                            <i class="fa fa-clock-o bg-gray"></i>
                        </li>
                    </ul>
                </div>
                <!-- /.tab-pane -->

                <div class="tab-pane" id="settings">
                    <form class="form-horizontal" method="post"
                          action="{{ route('user.returns.post', $investor->username) }}">
                        @csrf

                        <div class="form-group">
                            <label for="inputName" class="col-sm-2 control-label">Month</label>

                            <div class="col-sm-10">
                                <select name="month">
                                    @foreach($results as $result)
                                        <option value="{{ $result }}"> {{ $result }} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail" class="col-sm-2 control-label">Amount</label>

                            <div class="col-sm-10">
                                <input type="number" class="form-control" id="amount" name="amount"
                                       placeholder="Amount">
                            </div>
                        </div>

                        {{--    <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox"> I agree to the <a href="#">terms and conditions</a>
                                        </label>
                                    </div>
                                </div>
                            </div>--}}
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-danger">Send</button>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
        </div>
        <!-- /.nav-tabs-custom -->
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->

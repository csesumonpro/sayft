<li class="dropdown notifications-menu">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
        <i class="fa fa-bell-o"></i>

        @if(Admin::user()->unreadNotifications->count())
            <span class="label label-warning">
                {{ Admin::user()->unreadNotifications->count() }}
            </span>
        @endif

    </a>
    <ul class="dropdown-menu">
        <li class="header">
            <p class="text-center">
                @if(Admin::user()->unreadNotifications->count())
                    You have {{ Admin::user()->unreadNotifications->count() }} unread notifications
                @else
                    You have not have any notification
                @endif
            </p>
        </li>
        <li>
            <!-- inner menu: contains the actual data -->
            <ul class="menu">

                @foreach(Admin::user()->notifications as $notification)
                    <li>
                        <a href="#">
                            <i class="fa fa-users text-aqua"></i> 5 new members joined today
                        </a>
                    </li>
                @endforeach

            </ul>
        </li>
        <li class="footer"><a href="#">View all</a></li>
    </ul>
</li>

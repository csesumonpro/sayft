<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['verify' => true]);

Route::get('register/{username}', function ($username) {
    return redirect()->route('register')->withInput(['referral_code' => $username]);
})->name('register.with.referral');

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/referrals', 'HomeController@referrals')->name('referrals');

Route::prefix('dashboard')->middleware('verified')->group(function () {
    Route::get('/transactions/{id?}', 'HomeController@transactions')->name('dashboard.transactions');
    Route::get('/withdraws/{type}/{amount_type}/{coupon?}', 'HomeController@withdrawsForm')->name('dashboard.withdraws.form');
    Route::post('/withdraws/{type}/{amount_type}/{coupon?}', 'HomeController@withdrawsFormPost')->name('dashboard.withdraws.form.post');
    Route::get('/withdraws', 'HomeController@withdraws')->name('dashboard.withdraws');

    Route::get('/referrals', 'HomeController@referralList')->name('dashboard.referrals');
    Route::get('/guidelines', 'HomeController@guideline')->name('dashboard.guideline');
    Route::get('/support', 'HomeController@support')->name('dashboard.support');
    Route::post('/support', 'HomeController@supportPost')->name('dashboard.support.post');
    Route::get('/read_details/{id}/{type?}', 'HomeController@read')->name('dashboard.read.details');
    Route::post('/mark-all-as-read', 'HomeController@readAll')->name('dashboard.notification.read.all');
    //dashboard
    Route::get('/', 'HomeController@dashboard')->name('dashboard.dash');
    Route::get('/profile', 'HomeController@userProfile')->name('dashboard.profile');
    Route::post('/profile/update', 'HomeController@profileUpdate')->name('dashboard.profile.update');
    Route::get('/change/password', 'HomeController@changePassword')->name('dashboard.changepassword');
    Route::post('/update/password', 'HomeController@updatePassword')->name('dashboard.updatepassword');
    Route::get('/verification', 'HomeController@verifyAccount')->name('dashboard.verify');
    Route::post('/verify', 'HomeController@verifyRequest')->name('dashboard.verify.request');

});

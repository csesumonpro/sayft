-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 14, 2018 at 02:16 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sayft`
--

-- --------------------------------------------------------

--
-- Table structure for table `additional_returns`
--

CREATE TABLE `additional_returns` (
  `id` int(10) UNSIGNED NOT NULL,
  `amount` decimal(8,2) NOT NULL,
  `month` date NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `additional_returns`
--

INSERT INTO `additional_returns` (`id`, `amount`, `month`, `user_id`, `created_at`, `updated_at`) VALUES
(1, '120.00', '2018-12-18', 22, '2018-12-10 03:38:10', '2018-12-10 03:38:10'),
(2, '220.00', '2018-12-18', 22, '2018-12-10 06:22:21', '2018-12-10 06:22:21');

-- --------------------------------------------------------

--
-- Table structure for table `admin_menu`
--

CREATE TABLE `admin_menu` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `order` int(11) NOT NULL DEFAULT '0',
  `title` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uri` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permission` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_menu`
--

INSERT INTO `admin_menu` (`id`, `parent_id`, `order`, `title`, `icon`, `uri`, `permission`, `created_at`, `updated_at`) VALUES
(1, 0, 1, 'Dashboard', 'fa-bar-chart', '/', NULL, NULL, '2018-11-24 02:59:57'),
(2, 0, 2, 'Admin', 'fa-tasks', '', NULL, NULL, NULL),
(3, 2, 3, 'Users', 'fa-users', 'auth/users', NULL, NULL, NULL),
(4, 2, 4, 'Roles', 'fa-user', 'auth/roles', NULL, NULL, NULL),
(5, 2, 5, 'Permission', 'fa-ban', 'auth/permissions', NULL, NULL, NULL),
(6, 2, 6, 'Menu', 'fa-bars', 'auth/menu', NULL, NULL, NULL),
(7, 2, 7, 'Operation log', 'fa-history', 'auth/logs', NULL, NULL, NULL),
(8, 0, 8, 'Packages', 'fa-shopping-bag', 'packages', '*', '2018-11-19 02:01:57', '2018-11-19 02:40:41'),
(9, 0, 9, 'Members', 'fa-gbp', 'users', '*', '2018-11-19 02:39:56', '2018-12-05 00:27:49'),
(10, 0, 0, 'Return with bonus', 'fa-percent', 'additional_returns', '*', '2018-11-24 00:23:31', '2018-12-05 00:28:24'),
(11, 0, 0, 'News', 'fa-feed', 'news', '*', '2018-11-24 00:30:19', '2018-11-24 00:30:19'),
(12, 0, 0, 'Traning Contents', 'fa-hourglass-start', 'training_contents', '*', '2018-11-24 00:32:13', '2018-11-26 02:52:57'),
(13, 0, 0, 'Transactions', 'fa-angle-double-right', 'transactions', '*', '2018-12-05 01:47:57', '2018-12-05 01:47:57'),
(14, 0, 0, 'Supports', 'fa-bars', 'supports', '*', '2018-12-11 06:03:47', '2018-12-11 06:03:47');

-- --------------------------------------------------------

--
-- Table structure for table `admin_operation_log`
--

CREATE TABLE `admin_operation_log` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `method` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `input` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_operation_log`
--

INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(1, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-11-16 09:55:45', '2018-11-16 09:55:45'),
(2, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-11-16 09:57:09', '2018-11-16 09:57:09'),
(3, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-16 09:57:13', '2018-11-16 09:57:13'),
(4, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-16 09:57:14', '2018-11-16 09:57:14'),
(5, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-16 09:57:15', '2018-11-16 09:57:15'),
(6, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-16 09:57:16', '2018-11-16 09:57:16'),
(7, 1, 'admin/auth/setting', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-16 09:57:19', '2018-11-16 09:57:19'),
(8, 1, 'admin/auth/setting', 'GET', '127.0.0.1', '[]', '2018-11-16 09:57:43', '2018-11-16 09:57:43'),
(9, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-16 09:57:46', '2018-11-16 09:57:46'),
(10, 1, 'admin/auth/setting', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-16 09:57:49', '2018-11-16 09:57:49'),
(11, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-16 09:57:52', '2018-11-16 09:57:52'),
(12, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-16 09:57:56', '2018-11-16 09:57:56'),
(13, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-16 09:58:00', '2018-11-16 09:58:00'),
(14, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-11-16 09:58:02', '2018-11-16 09:58:02'),
(15, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-16 09:58:07', '2018-11-16 09:58:07'),
(16, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-11-16 11:06:23', '2018-11-16 11:06:23'),
(17, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-11-16 11:06:29', '2018-11-16 11:06:29'),
(18, 1, 'admin/users/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-16 11:06:33', '2018-11-16 11:06:33'),
(19, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-16 11:06:39', '2018-11-16 11:06:39'),
(20, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-16 11:33:26', '2018-11-16 11:33:26'),
(21, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-16 11:33:28', '2018-11-16 11:33:28'),
(22, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-16 11:33:34', '2018-11-16 11:33:34'),
(23, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-16 11:33:37', '2018-11-16 11:33:37'),
(24, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-16 11:33:39', '2018-11-16 11:33:39'),
(25, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-16 11:33:52', '2018-11-16 11:33:52'),
(26, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-16 11:34:32', '2018-11-16 11:34:32'),
(27, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-16 11:34:34', '2018-11-16 11:34:34'),
(28, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-11-17 10:35:00', '2018-11-17 10:35:00'),
(29, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-17 10:35:04', '2018-11-17 10:35:04'),
(30, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-11-17 10:39:29', '2018-11-17 10:39:29'),
(31, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-17 10:39:33', '2018-11-17 10:39:33'),
(32, 1, 'admin/auth/setting', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-17 10:39:38', '2018-11-17 10:39:38'),
(33, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-17 10:39:39', '2018-11-17 10:39:39'),
(34, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-11-18 11:33:46', '2018-11-18 11:33:46'),
(35, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-18 11:33:53', '2018-11-18 11:33:53'),
(36, 1, 'admin/auth/setting', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-18 11:33:57', '2018-11-18 11:33:57'),
(37, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-18 11:34:00', '2018-11-18 11:34:00'),
(38, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-11-19 01:51:20', '2018-11-19 01:51:20'),
(39, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-11-19 01:52:01', '2018-11-19 01:52:01'),
(40, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 01:52:03', '2018-11-19 01:52:03'),
(41, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 02:00:32', '2018-11-19 02:00:32'),
(42, 1, 'admin/auth/menu', 'POST', '127.0.0.1', '{\"parent_id\":\"0\",\"title\":\"Packages\",\"icon\":\"fa-shopping-bag\",\"uri\":\"packages\",\"roles\":[\"1\",null],\"permission\":\"*\",\"_token\":\"FGDxVTIcEbNKbyBjVjmGHxvVSGLODt3AehrcJpWO\"}', '2018-11-19 02:01:57', '2018-11-19 02:01:57'),
(43, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-11-19 02:01:57', '2018-11-19 02:01:57'),
(44, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-11-19 02:02:00', '2018-11-19 02:02:00'),
(45, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 02:02:04', '2018-11-19 02:02:04'),
(46, 1, 'admin/packages/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 02:02:06', '2018-11-19 02:02:06'),
(47, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 02:02:08', '2018-11-19 02:02:08'),
(48, 1, 'admin/packages', 'GET', '127.0.0.1', '[]', '2018-11-19 02:03:48', '2018-11-19 02:03:48'),
(49, 1, 'admin/packages/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 02:03:51', '2018-11-19 02:03:51'),
(50, 1, 'admin/packages/create', 'GET', '127.0.0.1', '[]', '2018-11-19 02:04:27', '2018-11-19 02:04:27'),
(51, 1, 'admin/packages/create', 'GET', '127.0.0.1', '[]', '2018-11-19 02:09:06', '2018-11-19 02:09:06'),
(52, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 02:09:17', '2018-11-19 02:09:17'),
(53, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 02:09:19', '2018-11-19 02:09:19'),
(54, 1, 'admin/packages/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 02:09:21', '2018-11-19 02:09:21'),
(55, 1, 'admin/packages/create', 'GET', '127.0.0.1', '[]', '2018-11-19 02:09:52', '2018-11-19 02:09:52'),
(56, 1, 'admin/packages/create', 'GET', '127.0.0.1', '[]', '2018-11-19 02:11:15', '2018-11-19 02:11:15'),
(57, 1, 'admin/packages/create', 'GET', '127.0.0.1', '[]', '2018-11-19 02:11:56', '2018-11-19 02:11:56'),
(58, 1, 'admin/packages/create', 'GET', '127.0.0.1', '[]', '2018-11-19 02:12:14', '2018-11-19 02:12:14'),
(59, 1, 'admin/packages/create', 'GET', '127.0.0.1', '[]', '2018-11-19 02:12:37', '2018-11-19 02:12:37'),
(60, 1, 'admin/packages/create', 'GET', '127.0.0.1', '[]', '2018-11-19 02:13:04', '2018-11-19 02:13:04'),
(61, 1, 'admin/packages/create', 'GET', '127.0.0.1', '[]', '2018-11-19 02:19:31', '2018-11-19 02:19:31'),
(62, 1, 'admin/packages', 'POST', '127.0.0.1', '{\"title\":\"Silver\",\"amount\":\"5000.00\",\"total_months\":\"36\",\"additional_returns\":\"$150 - $300\",\"referral_bonus\":{\"first_level\":\"200.00\",\"second_level\":\"50.00\",\"third_level\":\"20.00\"},\"_token\":\"FGDxVTIcEbNKbyBjVjmGHxvVSGLODt3AehrcJpWO\"}', '2018-11-19 02:21:10', '2018-11-19 02:21:10'),
(63, 1, 'admin/packages', 'GET', '127.0.0.1', '[]', '2018-11-19 02:21:11', '2018-11-19 02:21:11'),
(64, 1, 'admin/packages/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 02:21:19', '2018-11-19 02:21:19'),
(65, 1, 'admin/packages', 'POST', '127.0.0.1', '{\"title\":\"Gold\",\"amount\":\"10000.00\",\"total_months\":\"36\",\"additional_returns\":\"$300 - $600\",\"referral_bonus\":{\"first_level\":\"500.00\",\"second_level\":\"100.00\",\"third_level\":\"40.00\"},\"_token\":\"FGDxVTIcEbNKbyBjVjmGHxvVSGLODt3AehrcJpWO\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/packages\"}', '2018-11-19 02:22:23', '2018-11-19 02:22:23'),
(66, 1, 'admin/packages', 'GET', '127.0.0.1', '[]', '2018-11-19 02:22:24', '2018-11-19 02:22:24'),
(67, 1, 'admin/packages/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 02:22:26', '2018-11-19 02:22:26'),
(68, 1, 'admin/packages', 'POST', '127.0.0.1', '{\"title\":\"Diamond\",\"amount\":\"20000.00\",\"total_months\":\"36\",\"additional_returns\":\"$600 - $1200\",\"referral_bonus\":{\"first_level\":\"1100.00\",\"second_level\":\"250.00\",\"third_level\":\"100.00\"},\"_token\":\"FGDxVTIcEbNKbyBjVjmGHxvVSGLODt3AehrcJpWO\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/packages\"}', '2018-11-19 02:23:33', '2018-11-19 02:23:33'),
(69, 1, 'admin/packages', 'GET', '127.0.0.1', '[]', '2018-11-19 02:23:34', '2018-11-19 02:23:34'),
(70, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 02:23:38', '2018-11-19 02:23:38'),
(71, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 02:23:39', '2018-11-19 02:23:39'),
(72, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 02:23:51', '2018-11-19 02:23:51'),
(73, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 02:25:06', '2018-11-19 02:25:06'),
(74, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 02:25:22', '2018-11-19 02:25:22'),
(75, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 02:26:54', '2018-11-19 02:26:54'),
(76, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 02:27:12', '2018-11-19 02:27:12'),
(77, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-11-19 02:29:17', '2018-11-19 02:29:17'),
(78, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 02:29:55', '2018-11-19 02:29:55'),
(79, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 02:29:59', '2018-11-19 02:29:59'),
(80, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 02:30:01', '2018-11-19 02:30:01'),
(81, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 02:30:05', '2018-11-19 02:30:05'),
(82, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 02:30:10', '2018-11-19 02:30:10'),
(83, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 02:30:14', '2018-11-19 02:30:14'),
(84, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-11-19 02:39:12', '2018-11-19 02:39:12'),
(85, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 02:39:19', '2018-11-19 02:39:19'),
(86, 1, 'admin/auth/menu', 'POST', '127.0.0.1', '{\"parent_id\":\"0\",\"title\":\"Investors\",\"icon\":\"fa-gbp\",\"uri\":\"users\",\"roles\":[\"1\",null],\"permission\":\"*\",\"_token\":\"FGDxVTIcEbNKbyBjVjmGHxvVSGLODt3AehrcJpWO\"}', '2018-11-19 02:39:56', '2018-11-19 02:39:56'),
(87, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-11-19 02:39:56', '2018-11-19 02:39:56'),
(88, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 02:40:00', '2018-11-19 02:40:00'),
(89, 1, 'admin/auth/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 02:40:08', '2018-11-19 02:40:08'),
(90, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 02:40:28', '2018-11-19 02:40:28'),
(91, 1, 'admin/auth/menu', 'POST', '127.0.0.1', '{\"_token\":\"FGDxVTIcEbNKbyBjVjmGHxvVSGLODt3AehrcJpWO\",\"_order\":\"[{\\\"id\\\":1},{\\\"id\\\":2,\\\"children\\\":[{\\\"id\\\":3},{\\\"id\\\":4},{\\\"id\\\":5},{\\\"id\\\":6},{\\\"id\\\":7}]},{\\\"id\\\":8},{\\\"id\\\":9}]\"}', '2018-11-19 02:40:41', '2018-11-19 02:40:41'),
(92, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 02:40:41', '2018-11-19 02:40:41'),
(93, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-11-19 02:40:43', '2018-11-19 02:40:43'),
(94, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-11-19 02:41:10', '2018-11-19 02:41:10'),
(95, 1, 'admin/users/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 02:41:19', '2018-11-19 02:41:19'),
(96, 1, 'admin/users/create', 'GET', '127.0.0.1', '[]', '2018-11-19 02:44:15', '2018-11-19 02:44:15'),
(97, 1, 'admin/users/create', 'GET', '127.0.0.1', '[]', '2018-11-19 02:45:06', '2018-11-19 02:45:06'),
(98, 1, 'admin/users/create', 'GET', '127.0.0.1', '[]', '2018-11-19 02:47:31', '2018-11-19 02:47:31'),
(99, 1, 'admin/users/create', 'GET', '127.0.0.1', '[]', '2018-11-19 02:49:01', '2018-11-19 02:49:01'),
(100, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 02:49:30', '2018-11-19 02:49:30'),
(101, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 02:49:31', '2018-11-19 02:49:31'),
(102, 1, 'admin/users/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 02:49:33', '2018-11-19 02:49:33'),
(103, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 02:49:54', '2018-11-19 02:49:54'),
(104, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 02:49:56', '2018-11-19 02:49:56'),
(105, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-11-19 02:49:58', '2018-11-19 02:49:58'),
(106, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 02:50:01', '2018-11-19 02:50:01'),
(107, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 02:50:02', '2018-11-19 02:50:02'),
(108, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 02:50:03', '2018-11-19 02:50:03'),
(109, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 02:50:06', '2018-11-19 02:50:06'),
(110, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-11-19 03:08:18', '2018-11-19 03:08:18'),
(111, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 03:08:21', '2018-11-19 03:08:21'),
(112, 1, 'admin/users/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 03:08:26', '2018-11-19 03:08:26'),
(113, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 03:09:38', '2018-11-19 03:09:38'),
(114, 1, 'admin/users/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 03:09:40', '2018-11-19 03:09:40'),
(115, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 03:10:50', '2018-11-19 03:10:50'),
(116, 1, 'admin/users/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 03:10:51', '2018-11-19 03:10:51'),
(117, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 03:11:58', '2018-11-19 03:11:58'),
(118, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 03:12:01', '2018-11-19 03:12:01'),
(119, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 03:12:01', '2018-11-19 03:12:01'),
(120, 1, 'admin/users/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 03:12:03', '2018-11-19 03:12:03'),
(121, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 03:12:13', '2018-11-19 03:12:13'),
(122, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 03:12:16', '2018-11-19 03:12:16'),
(123, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-11-19 04:28:10', '2018-11-19 04:28:10'),
(124, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 04:28:14', '2018-11-19 04:28:14'),
(125, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 04:28:15', '2018-11-19 04:28:15'),
(126, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 04:28:16', '2018-11-19 04:28:16'),
(127, 1, 'admin/users/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 04:28:17', '2018-11-19 04:28:17'),
(128, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 04:57:29', '2018-11-19 04:57:29'),
(129, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 04:57:31', '2018-11-19 04:57:31'),
(130, 1, 'admin/users/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 04:57:33', '2018-11-19 04:57:33'),
(131, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 04:59:41', '2018-11-19 04:59:41'),
(132, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 04:59:44', '2018-11-19 04:59:44'),
(133, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 04:59:45', '2018-11-19 04:59:45'),
(134, 1, 'admin/users/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 05:00:09', '2018-11-19 05:00:09'),
(135, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 05:01:38', '2018-11-19 05:01:38'),
(136, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 05:01:40', '2018-11-19 05:01:40'),
(137, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 05:01:42', '2018-11-19 05:01:42'),
(138, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 05:02:32', '2018-11-19 05:02:32'),
(139, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 05:02:36', '2018-11-19 05:02:36'),
(140, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 05:02:36', '2018-11-19 05:02:36'),
(141, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 05:02:57', '2018-11-19 05:02:57'),
(142, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 05:03:08', '2018-11-19 05:03:08'),
(143, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 05:03:11', '2018-11-19 05:03:11'),
(144, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 05:03:15', '2018-11-19 05:03:15'),
(145, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 05:03:16', '2018-11-19 05:03:16'),
(146, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 05:03:18', '2018-11-19 05:03:18'),
(147, 1, 'admin/users/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 05:03:25', '2018-11-19 05:03:25'),
(148, 1, 'admin/users', 'POST', '127.0.0.1', '{\"first_name\":\"Anwar\",\"last_name\":\"Hussen\",\"username\":\"janedoe\",\"phone\":\"01722635124\",\"email\":\"anwar.husesn4@gmail.com\",\"password\":\"123456\",\"invest_amount\":{\"package_id\":\"1\",\"referral_code\":null,\"status\":\"pending\"},\"_token\":\"NWZJHuQxT74xnse7a0BwhbAd1nSqhQGpLZFctWWt\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/users\"}', '2018-11-19 05:04:05', '2018-11-19 05:04:05'),
(149, 1, 'admin/users/create', 'GET', '127.0.0.1', '[]', '2018-11-19 05:04:06', '2018-11-19 05:04:06'),
(150, 1, 'admin/users', 'POST', '127.0.0.1', '{\"first_name\":\"Anwar\",\"last_name\":\"Hussen\",\"username\":\"janedoe\",\"phone\":\"01722635124\",\"email\":\"anwar.husesn4@gmail.com\",\"password\":\"123456\",\"invest_amount\":{\"package_id\":\"1\",\"referral_code\":null,\"status\":\"pending\"},\"_token\":\"NWZJHuQxT74xnse7a0BwhbAd1nSqhQGpLZFctWWt\"}', '2018-11-19 05:04:19', '2018-11-19 05:04:19'),
(151, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-11-19 05:04:19', '2018-11-19 05:04:19'),
(152, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-11-19 05:05:25', '2018-11-19 05:05:25'),
(153, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-11-19 05:06:12', '2018-11-19 05:06:12'),
(154, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-11-19 05:06:34', '2018-11-19 05:06:34'),
(155, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-11-19 05:07:01', '2018-11-19 05:07:01'),
(156, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-11-19 05:07:13', '2018-11-19 05:07:13'),
(157, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-11-19 05:08:00', '2018-11-19 05:08:00'),
(158, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 05:08:21', '2018-11-19 05:08:21'),
(159, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 05:08:23', '2018-11-19 05:08:23'),
(160, 1, 'admin/users/1', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 05:08:28', '2018-11-19 05:08:28'),
(161, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 05:08:32', '2018-11-19 05:08:32'),
(162, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-11-19 05:09:09', '2018-11-19 05:09:09'),
(163, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-11-19 05:10:44', '2018-11-19 05:10:44'),
(164, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-11-19 05:11:14', '2018-11-19 05:11:14'),
(165, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-11-19 05:12:36', '2018-11-19 05:12:36'),
(166, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-11-19 05:26:19', '2018-11-19 05:26:19'),
(167, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-11-19 05:29:54', '2018-11-19 05:29:54'),
(168, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-11-19 05:30:59', '2018-11-19 05:30:59'),
(169, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-11-19 05:32:42', '2018-11-19 05:32:42'),
(170, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-11-19 05:32:54', '2018-11-19 05:32:54'),
(171, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-11-19 05:33:20', '2018-11-19 05:33:20'),
(172, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-11-19 05:33:54', '2018-11-19 05:33:54'),
(173, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-11-19 05:34:45', '2018-11-19 05:34:45'),
(174, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-11-19 05:36:08', '2018-11-19 05:36:08'),
(175, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-11-19 05:37:22', '2018-11-19 05:37:22'),
(176, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-11-19 05:37:56', '2018-11-19 05:37:56'),
(177, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-11-19 05:38:25', '2018-11-19 05:38:25'),
(178, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-11-19 05:38:34', '2018-11-19 05:38:34'),
(179, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 05:39:13', '2018-11-19 05:39:13'),
(180, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 05:39:14', '2018-11-19 05:39:14'),
(181, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 05:39:16', '2018-11-19 05:39:16'),
(182, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-11-19 05:41:08', '2018-11-19 05:41:08'),
(183, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-11-19 05:41:28', '2018-11-19 05:41:28'),
(184, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-11-19 05:46:04', '2018-11-19 05:46:04'),
(185, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 05:46:08', '2018-11-19 05:46:08'),
(186, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 05:46:10', '2018-11-19 05:46:10'),
(187, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 05:46:12', '2018-11-19 05:46:12'),
(188, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 05:46:16', '2018-11-19 05:46:16'),
(189, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 05:46:17', '2018-11-19 05:46:17'),
(190, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-11-19 05:46:44', '2018-11-19 05:46:44'),
(191, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-11-19 05:48:29', '2018-11-19 05:48:29'),
(192, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-11-19 05:49:04', '2018-11-19 05:49:04'),
(193, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 05:50:16', '2018-11-19 05:50:16'),
(194, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 05:50:21', '2018-11-19 05:50:21'),
(195, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 05:50:22', '2018-11-19 05:50:22'),
(196, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 05:50:24', '2018-11-19 05:50:24'),
(197, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-11-19 05:51:23', '2018-11-19 05:51:23'),
(198, 1, 'admin/users/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 05:51:26', '2018-11-19 05:51:26'),
(199, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 05:51:34', '2018-11-19 05:51:34'),
(200, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 05:51:37', '2018-11-19 05:51:37'),
(201, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-11-19 06:09:04', '2018-11-19 06:09:04'),
(202, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 06:09:12', '2018-11-19 06:09:12'),
(203, 1, 'admin/users/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 06:09:16', '2018-11-19 06:09:16'),
(204, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 06:09:25', '2018-11-19 06:09:25'),
(205, 1, 'admin/users/1', 'DELETE', '127.0.0.1', '{\"_method\":\"delete\",\"_token\":\"NWZJHuQxT74xnse7a0BwhbAd1nSqhQGpLZFctWWt\"}', '2018-11-19 06:09:33', '2018-11-19 06:09:33'),
(206, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 06:09:33', '2018-11-19 06:09:33'),
(207, 1, 'admin/users/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 06:09:36', '2018-11-19 06:09:36'),
(208, 1, 'admin/users', 'POST', '127.0.0.1', '{\"first_name\":\"Anwar\",\"last_name\":\"Hussen\",\"username\":\"anwar0101\",\"phone\":\"01722635124\",\"email\":\"anwar.hussen4@gmail.com\",\"password\":\"123456\",\"invest_amount\":{\"package_id\":\"1\",\"referral_code\":null,\"status\":\"paid\"},\"_token\":\"NWZJHuQxT74xnse7a0BwhbAd1nSqhQGpLZFctWWt\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/users\"}', '2018-11-19 06:10:41', '2018-11-19 06:10:41'),
(209, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-11-19 06:10:42', '2018-11-19 06:10:42'),
(210, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 06:10:56', '2018-11-19 06:10:56'),
(211, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 06:11:07', '2018-11-19 06:11:07'),
(212, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-11-19 06:13:58', '2018-11-19 06:13:58'),
(213, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-11-19 06:17:51', '2018-11-19 06:17:51'),
(214, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-11-19 06:17:56', '2018-11-19 06:17:56'),
(215, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-11-19 06:18:15', '2018-11-19 06:18:15'),
(216, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-11-19 06:18:40', '2018-11-19 06:18:40'),
(217, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-11-19 06:19:12', '2018-11-19 06:19:12'),
(218, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-11-19 06:19:14', '2018-11-19 06:19:14'),
(219, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-11-19 06:19:30', '2018-11-19 06:19:30'),
(220, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-11-19 06:19:33', '2018-11-19 06:19:33'),
(221, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-11-19 06:19:35', '2018-11-19 06:19:35'),
(222, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 06:19:52', '2018-11-19 06:19:52'),
(223, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-11-19 06:20:43', '2018-11-19 06:20:43'),
(224, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 06:20:46', '2018-11-19 06:20:46'),
(225, 1, 'admin/users/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 06:21:07', '2018-11-19 06:21:07'),
(226, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 06:42:55', '2018-11-19 06:42:55'),
(227, 1, 'admin/users/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 06:42:58', '2018-11-19 06:42:58'),
(228, 1, 'admin/users', 'POST', '127.0.0.1', '{\"first_name\":\"Anwar\",\"last_name\":\"Hussen\",\"username\":\"anwar0x09\",\"phone\":\"01722635457\",\"email\":\"anwar.hussen4@gmail.com\",\"password\":\"123456\",\"invest_amount\":{\"package_id\":\"1\",\"referral_code\":\"anwar0101\",\"status\":\"paid\"},\"_token\":\"NWZJHuQxT74xnse7a0BwhbAd1nSqhQGpLZFctWWt\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/users\"}', '2018-11-19 06:44:00', '2018-11-19 06:44:00'),
(229, 1, 'admin/users/create', 'GET', '127.0.0.1', '[]', '2018-11-19 06:44:00', '2018-11-19 06:44:00'),
(230, 1, 'admin/users', 'POST', '127.0.0.1', '{\"first_name\":\"Anwar\",\"last_name\":\"Hussen\",\"username\":\"anwar0x09\",\"phone\":\"01722635457\",\"email\":\"anwar.hussen2@gmail.com\",\"password\":\"123456\",\"invest_amount\":{\"package_id\":\"1\",\"referral_code\":\"anwar0101\",\"status\":\"paid\"},\"_token\":\"NWZJHuQxT74xnse7a0BwhbAd1nSqhQGpLZFctWWt\"}', '2018-11-19 06:44:20', '2018-11-19 06:44:20'),
(231, 1, 'admin/users/create', 'GET', '127.0.0.1', '[]', '2018-11-19 06:44:20', '2018-11-19 06:44:20'),
(232, 1, 'admin/users', 'POST', '127.0.0.1', '{\"first_name\":\"Anwar\",\"last_name\":\"Hussen\",\"username\":\"anwar0x09\",\"phone\":\"01722635457\",\"email\":\"anwar.hussen2@gmail.com\",\"password\":\"123456\",\"invest_amount\":{\"package_id\":\"1\",\"referral_code\":\"anwar0101\",\"status\":\"paid\"},\"_token\":\"NWZJHuQxT74xnse7a0BwhbAd1nSqhQGpLZFctWWt\"}', '2018-11-19 06:45:31', '2018-11-19 06:45:31'),
(233, 1, 'admin/users/create', 'GET', '127.0.0.1', '[]', '2018-11-19 06:45:31', '2018-11-19 06:45:31'),
(234, 1, 'admin/users', 'POST', '127.0.0.1', '{\"first_name\":\"Anwar\",\"last_name\":\"Hussen\",\"username\":\"anwar0x09\",\"phone\":\"01722635457\",\"email\":\"anwar.hussen5@gmail.com\",\"password\":\"123456\",\"invest_amount\":{\"package_id\":\"1\",\"referral_code\":\"anwar0101\",\"status\":\"paid\"},\"_token\":\"NWZJHuQxT74xnse7a0BwhbAd1nSqhQGpLZFctWWt\"}', '2018-11-19 06:45:52', '2018-11-19 06:45:52'),
(235, 1, 'admin/users/create', 'GET', '127.0.0.1', '[]', '2018-11-19 06:45:53', '2018-11-19 06:45:53'),
(236, 1, 'admin/users', 'POST', '127.0.0.1', '{\"first_name\":\"Anwar\",\"last_name\":\"Hussen\",\"username\":\"anwar0x07\",\"phone\":\"01722635457\",\"email\":\"anwar.hussen5@gmail.com\",\"password\":\"123456\",\"invest_amount\":{\"package_id\":\"1\",\"referral_code\":\"anwar0101\",\"status\":\"paid\"},\"_token\":\"NWZJHuQxT74xnse7a0BwhbAd1nSqhQGpLZFctWWt\"}', '2018-11-19 06:46:15', '2018-11-19 06:46:15'),
(237, 1, 'admin/users/create', 'GET', '127.0.0.1', '[]', '2018-11-19 06:46:15', '2018-11-19 06:46:15'),
(238, 1, 'admin/users', 'POST', '127.0.0.1', '{\"first_name\":\"Anwar\",\"last_name\":\"Hussen\",\"username\":\"anwar0x07\",\"phone\":\"01722635457\",\"email\":\"anwar.hussen5@gmail.com\",\"password\":\"123456\",\"invest_amount\":{\"package_id\":\"1\",\"referral_code\":\"anwar0101\",\"status\":\"paid\"},\"_token\":\"NWZJHuQxT74xnse7a0BwhbAd1nSqhQGpLZFctWWt\"}', '2018-11-19 06:49:58', '2018-11-19 06:49:58'),
(239, 1, 'admin/users/create', 'GET', '127.0.0.1', '[]', '2018-11-19 06:49:59', '2018-11-19 06:49:59'),
(240, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-11-19 06:50:04', '2018-11-19 06:50:04'),
(241, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 06:50:07', '2018-11-19 06:50:07'),
(242, 1, 'admin/users/6', 'DELETE', '127.0.0.1', '{\"_method\":\"delete\",\"_token\":\"NWZJHuQxT74xnse7a0BwhbAd1nSqhQGpLZFctWWt\"}', '2018-11-19 06:56:43', '2018-11-19 06:56:43'),
(243, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 06:56:44', '2018-11-19 06:56:44'),
(244, 1, 'admin/users/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 06:56:48', '2018-11-19 06:56:48'),
(245, 1, 'admin/users', 'POST', '127.0.0.1', '{\"first_name\":\"Anwar\",\"last_name\":\"Hussen\",\"username\":\"janedoe\",\"phone\":\"01722635124\",\"email\":\"admin@admin.com\",\"password\":\"123456\",\"invest_amount\":{\"package_id\":\"1\",\"referral_code\":\"anwar0101\",\"status\":\"paid\"},\"_token\":\"NWZJHuQxT74xnse7a0BwhbAd1nSqhQGpLZFctWWt\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/users\"}', '2018-11-19 06:57:27', '2018-11-19 06:57:27'),
(246, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-11-19 06:57:27', '2018-11-19 06:57:27'),
(247, 1, 'admin/users/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 07:02:04', '2018-11-19 07:02:04'),
(248, 1, 'admin/users', 'POST', '127.0.0.1', '{\"first_name\":\"Anwar\",\"last_name\":\"Hussen\",\"username\":\"anwar0x011\",\"phone\":\"01722665456\",\"email\":\"aa@aa.com\",\"password\":\"123456\",\"invest_amount\":{\"package_id\":\"1\",\"referral_code\":\"anwar0101\",\"status\":\"paid\"},\"_token\":\"NWZJHuQxT74xnse7a0BwhbAd1nSqhQGpLZFctWWt\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/users\"}', '2018-11-19 07:02:42', '2018-11-19 07:02:42'),
(249, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-11-19 07:02:42', '2018-11-19 07:02:42'),
(250, 1, 'admin/users/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 07:05:48', '2018-11-19 07:05:48'),
(251, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 07:05:50', '2018-11-19 07:05:50'),
(252, 1, 'admin/users/2,7,8', 'DELETE', '127.0.0.1', '{\"_method\":\"delete\",\"_token\":\"NWZJHuQxT74xnse7a0BwhbAd1nSqhQGpLZFctWWt\"}', '2018-11-19 07:06:05', '2018-11-19 07:06:05'),
(253, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 07:06:05', '2018-11-19 07:06:05'),
(254, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 07:06:08', '2018-11-19 07:06:08'),
(255, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 07:06:10', '2018-11-19 07:06:10'),
(256, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 07:06:16', '2018-11-19 07:06:16'),
(257, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 07:06:19', '2018-11-19 07:06:19'),
(258, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 07:06:21', '2018-11-19 07:06:21'),
(259, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 07:06:21', '2018-11-19 07:06:21'),
(260, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 07:06:23', '2018-11-19 07:06:23'),
(261, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 07:06:39', '2018-11-19 07:06:39'),
(262, 1, 'admin/auth/setting', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 07:06:41', '2018-11-19 07:06:41'),
(263, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 07:06:43', '2018-11-19 07:06:43'),
(264, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-19 07:06:45', '2018-11-19 07:06:45'),
(265, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-11-20 02:53:04', '2018-11-20 02:53:04'),
(266, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-20 02:53:09', '2018-11-20 02:53:09'),
(267, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-20 02:53:19', '2018-11-20 02:53:19'),
(268, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-20 02:53:20', '2018-11-20 02:53:20'),
(269, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-20 02:53:25', '2018-11-20 02:53:25'),
(270, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-20 03:00:17', '2018-11-20 03:00:17'),
(271, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-20 03:00:21', '2018-11-20 03:00:21'),
(272, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-11-20 03:01:03', '2018-11-20 03:01:03'),
(273, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-20 03:01:06', '2018-11-20 03:01:06'),
(274, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-11-20 03:01:21', '2018-11-20 03:01:21'),
(275, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-20 03:01:26', '2018-11-20 03:01:26'),
(276, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-20 03:01:27', '2018-11-20 03:01:27'),
(277, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-20 03:01:28', '2018-11-20 03:01:28'),
(278, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-20 03:01:48', '2018-11-20 03:01:48'),
(279, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-20 03:01:49', '2018-11-20 03:01:49'),
(280, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-11-23 23:59:54', '2018-11-23 23:59:54'),
(281, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-11-24 00:01:54', '2018-11-24 00:01:54'),
(282, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-11-24 00:02:01', '2018-11-24 00:02:01'),
(283, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 00:03:14', '2018-11-24 00:03:14'),
(284, 1, 'admin/auth/setting', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 00:03:17', '2018-11-24 00:03:17'),
(285, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 00:03:20', '2018-11-24 00:03:20'),
(286, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 00:03:34', '2018-11-24 00:03:34'),
(287, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-11-24 00:03:45', '2018-11-24 00:03:45'),
(288, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 00:03:50', '2018-11-24 00:03:50'),
(289, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 00:04:00', '2018-11-24 00:04:00'),
(290, 1, 'admin/packages/2', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 00:04:32', '2018-11-24 00:04:32'),
(291, 1, 'admin/packages/2', 'GET', '127.0.0.1', '[]', '2018-11-24 00:04:33', '2018-11-24 00:04:33'),
(292, 1, 'admin/packages', 'GET', '127.0.0.1', '[]', '2018-11-24 00:04:55', '2018-11-24 00:04:55'),
(293, 1, 'admin/packages/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 00:05:06', '2018-11-24 00:05:06'),
(294, 1, 'admin/packages/1/edit', 'GET', '127.0.0.1', '[]', '2018-11-24 00:06:31', '2018-11-24 00:06:31'),
(295, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 00:06:41', '2018-11-24 00:06:41'),
(296, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 00:06:42', '2018-11-24 00:06:42'),
(297, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 00:06:49', '2018-11-24 00:06:49'),
(298, 1, 'admin/users/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 00:07:05', '2018-11-24 00:07:05'),
(299, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 00:07:10', '2018-11-24 00:07:10'),
(300, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 00:07:12', '2018-11-24 00:07:12'),
(301, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-11-24 00:09:11', '2018-11-24 00:09:11'),
(302, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 00:09:17', '2018-11-24 00:09:17'),
(303, 1, 'admin/users/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 00:09:19', '2018-11-24 00:09:19'),
(304, 1, 'admin/users', 'POST', '127.0.0.1', '{\"first_name\":\"Anwar\",\"last_name\":\"Hussen\",\"username\":\"janedoe\",\"phone\":\"17564564654\",\"email\":\"info@teamblackhole.com\",\"password\":\"123456\",\"invest_amount\":{\"package_id\":\"1\",\"referral_code\":null,\"status\":\"paid\"},\"_token\":\"bYDO7B9jsGZcKYYcPzN6hf01okrGXGXG0dO5G6nj\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/users\"}', '2018-11-24 00:13:03', '2018-11-24 00:13:03'),
(305, 1, 'admin/users/create', 'GET', '127.0.0.1', '[]', '2018-11-24 00:13:04', '2018-11-24 00:13:04'),
(306, 1, 'admin/users', 'POST', '127.0.0.1', '{\"first_name\":\"Anwar\",\"last_name\":\"Hussen\",\"username\":\"janedoe\",\"phone\":\"17564564654\",\"email\":\"info@teamblackhole.com\",\"password\":\"123456\",\"invest_amount\":{\"package_id\":\"1\",\"referral_code\":null,\"status\":\"paid\"},\"_token\":\"bYDO7B9jsGZcKYYcPzN6hf01okrGXGXG0dO5G6nj\"}', '2018-11-24 00:13:49', '2018-11-24 00:13:49'),
(307, 1, 'admin/users/create', 'GET', '127.0.0.1', '[]', '2018-11-24 00:13:50', '2018-11-24 00:13:50'),
(308, 1, 'admin/users', 'POST', '127.0.0.1', '{\"first_name\":\"Anwar\",\"last_name\":\"Hussen\",\"username\":\"janedoe\",\"phone\":\"17564564654\",\"email\":\"info@teamblackhole.com\",\"password\":\"123456\",\"invest_amount\":{\"package_id\":\"1\",\"referral_code\":null,\"status\":\"paid\"},\"_token\":\"bYDO7B9jsGZcKYYcPzN6hf01okrGXGXG0dO5G6nj\"}', '2018-11-24 00:15:10', '2018-11-24 00:15:10'),
(309, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-11-24 00:15:10', '2018-11-24 00:15:10'),
(310, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 00:16:02', '2018-11-24 00:16:02'),
(311, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 00:16:05', '2018-11-24 00:16:05'),
(312, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 00:16:06', '2018-11-24 00:16:06'),
(313, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-11-24 00:17:27', '2018-11-24 00:17:27'),
(314, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-11-24 00:17:44', '2018-11-24 00:17:44'),
(315, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 00:17:48', '2018-11-24 00:17:48'),
(316, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 00:17:49', '2018-11-24 00:17:49'),
(317, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 00:17:50', '2018-11-24 00:17:50'),
(318, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 00:18:09', '2018-11-24 00:18:09'),
(319, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 00:18:14', '2018-11-24 00:18:14'),
(320, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 00:18:16', '2018-11-24 00:18:16'),
(321, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 00:18:29', '2018-11-24 00:18:29'),
(322, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 00:19:15', '2018-11-24 00:19:15'),
(323, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 00:19:16', '2018-11-24 00:19:16'),
(324, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 00:19:17', '2018-11-24 00:19:17'),
(325, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 00:19:33', '2018-11-24 00:19:33'),
(326, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 00:19:35', '2018-11-24 00:19:35'),
(327, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 00:19:37', '2018-11-24 00:19:37'),
(328, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 00:19:38', '2018-11-24 00:19:38'),
(329, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 00:21:38', '2018-11-24 00:21:38'),
(330, 1, 'admin/auth/menu', 'POST', '127.0.0.1', '{\"parent_id\":\"0\",\"title\":\"Additional Returns\",\"icon\":\"fa-percent\",\"uri\":\"additional_returns\",\"roles\":[\"1\",null],\"permission\":\"*\",\"_token\":\"bYDO7B9jsGZcKYYcPzN6hf01okrGXGXG0dO5G6nj\"}', '2018-11-24 00:23:31', '2018-11-24 00:23:31'),
(331, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-11-24 00:23:31', '2018-11-24 00:23:31'),
(332, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-11-24 00:23:36', '2018-11-24 00:23:36'),
(333, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-11-24 00:23:43', '2018-11-24 00:23:43'),
(334, 1, 'admin/auth/menu/10/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 00:23:54', '2018-11-24 00:23:54'),
(335, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 00:26:31', '2018-11-24 00:26:31'),
(336, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 00:26:33', '2018-11-24 00:26:33'),
(337, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 00:26:36', '2018-11-24 00:26:36'),
(338, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 00:26:37', '2018-11-24 00:26:37'),
(339, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 00:26:38', '2018-11-24 00:26:38'),
(340, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 00:26:46', '2018-11-24 00:26:46'),
(341, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 00:26:47', '2018-11-24 00:26:47'),
(342, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 00:26:49', '2018-11-24 00:26:49'),
(343, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 00:29:38', '2018-11-24 00:29:38'),
(344, 1, 'admin/auth/menu', 'POST', '127.0.0.1', '{\"parent_id\":\"0\",\"title\":\"News\",\"icon\":\"fa-feed\",\"uri\":\"news\",\"roles\":[\"1\",null],\"permission\":\"*\",\"_token\":\"bYDO7B9jsGZcKYYcPzN6hf01okrGXGXG0dO5G6nj\"}', '2018-11-24 00:30:19', '2018-11-24 00:30:19'),
(345, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-11-24 00:30:19', '2018-11-24 00:30:19'),
(346, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-11-24 00:30:25', '2018-11-24 00:30:25'),
(347, 1, 'admin/auth/menu', 'POST', '127.0.0.1', '{\"parent_id\":\"0\",\"title\":\"Traning Contents\",\"icon\":\"fa-hourglass-start\",\"uri\":\"traning_contents\",\"roles\":[\"1\",null],\"permission\":\"*\",\"_token\":\"bYDO7B9jsGZcKYYcPzN6hf01okrGXGXG0dO5G6nj\"}', '2018-11-24 00:32:13', '2018-11-24 00:32:13'),
(348, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-11-24 00:32:13', '2018-11-24 00:32:13'),
(349, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-11-24 00:32:17', '2018-11-24 00:32:17'),
(350, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 00:34:44', '2018-11-24 00:34:44'),
(351, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 00:34:45', '2018-11-24 00:34:45'),
(352, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 00:34:46', '2018-11-24 00:34:46'),
(353, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 00:34:48', '2018-11-24 00:34:48'),
(354, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-24 00:38:11', '2018-11-24 00:38:11'),
(355, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-24 00:38:31', '2018-11-24 00:38:31'),
(356, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-24 00:54:18', '2018-11-24 00:54:18'),
(357, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-24 00:54:31', '2018-11-24 00:54:31'),
(358, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 00:55:07', '2018-11-24 00:55:07'),
(359, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 00:55:08', '2018-11-24 00:55:08'),
(360, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 00:55:10', '2018-11-24 00:55:10'),
(361, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 00:55:12', '2018-11-24 00:55:12'),
(362, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 00:55:13', '2018-11-24 00:55:13'),
(363, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-11-24 01:21:17', '2018-11-24 01:21:17'),
(364, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 01:21:25', '2018-11-24 01:21:25'),
(365, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-24 01:21:39', '2018-11-24 01:21:39'),
(366, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 01:22:33', '2018-11-24 01:22:33'),
(367, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 01:22:36', '2018-11-24 01:22:36'),
(368, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 01:23:07', '2018-11-24 01:23:07'),
(369, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 01:23:09', '2018-11-24 01:23:09'),
(370, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 01:23:11', '2018-11-24 01:23:11'),
(371, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 01:23:12', '2018-11-24 01:23:12'),
(372, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 01:23:13', '2018-11-24 01:23:13'),
(373, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 01:23:14', '2018-11-24 01:23:14'),
(374, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 01:23:15', '2018-11-24 01:23:15'),
(375, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 01:23:22', '2018-11-24 01:23:22'),
(376, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-11-24 01:54:09', '2018-11-24 01:54:09'),
(377, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 01:54:13', '2018-11-24 01:54:13'),
(378, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 01:54:19', '2018-11-24 01:54:19'),
(379, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 01:56:06', '2018-11-24 01:56:06');
INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(380, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 01:56:08', '2018-11-24 01:56:08'),
(381, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 01:57:28', '2018-11-24 01:57:28'),
(382, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 02:01:34', '2018-11-24 02:01:34'),
(383, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 02:01:37', '2018-11-24 02:01:37'),
(384, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 02:01:37', '2018-11-24 02:01:37'),
(385, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 02:01:39', '2018-11-24 02:01:39'),
(386, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 02:01:41', '2018-11-24 02:01:41'),
(387, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 02:01:42', '2018-11-24 02:01:42'),
(388, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 02:02:09', '2018-11-24 02:02:09'),
(389, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-24 02:27:14', '2018-11-24 02:27:14'),
(390, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-24 02:27:17', '2018-11-24 02:27:17'),
(391, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-24 02:27:24', '2018-11-24 02:27:24'),
(392, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-24 02:29:57', '2018-11-24 02:29:57'),
(393, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-24 02:32:28', '2018-11-24 02:32:28'),
(394, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-24 02:32:57', '2018-11-24 02:32:57'),
(395, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-24 02:35:48', '2018-11-24 02:35:48'),
(396, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-24 02:36:49', '2018-11-24 02:36:49'),
(397, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-24 02:37:31', '2018-11-24 02:37:31'),
(398, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-24 02:37:53', '2018-11-24 02:37:53'),
(399, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-24 02:39:43', '2018-11-24 02:39:43'),
(400, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 02:39:49', '2018-11-24 02:39:49'),
(401, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 02:39:51', '2018-11-24 02:39:51'),
(402, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-24 02:40:34', '2018-11-24 02:40:34'),
(403, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-24 02:42:06', '2018-11-24 02:42:06'),
(404, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-24 02:42:09', '2018-11-24 02:42:09'),
(405, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-24 02:42:20', '2018-11-24 02:42:20'),
(406, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-24 02:42:22', '2018-11-24 02:42:22'),
(407, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-24 02:42:35', '2018-11-24 02:42:35'),
(408, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-24 02:43:11', '2018-11-24 02:43:11'),
(409, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-24 02:43:42', '2018-11-24 02:43:42'),
(410, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-24 02:44:22', '2018-11-24 02:44:22'),
(411, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-24 02:45:23', '2018-11-24 02:45:23'),
(412, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-24 02:46:59', '2018-11-24 02:46:59'),
(413, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-24 02:47:24', '2018-11-24 02:47:24'),
(414, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-24 02:47:36', '2018-11-24 02:47:36'),
(415, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-24 02:49:39', '2018-11-24 02:49:39'),
(416, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-24 02:50:01', '2018-11-24 02:50:01'),
(417, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-24 02:51:02', '2018-11-24 02:51:02'),
(418, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-24 02:51:19', '2018-11-24 02:51:19'),
(419, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-24 02:51:30', '2018-11-24 02:51:30'),
(420, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-24 02:51:50', '2018-11-24 02:51:50'),
(421, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 02:52:00', '2018-11-24 02:52:00'),
(422, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 02:52:08', '2018-11-24 02:52:08'),
(423, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-11-24 02:52:52', '2018-11-24 02:52:52'),
(424, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 02:52:56', '2018-11-24 02:52:56'),
(425, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-24 02:53:21', '2018-11-24 02:53:21'),
(426, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 02:54:10', '2018-11-24 02:54:10'),
(427, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 02:54:13', '2018-11-24 02:54:13'),
(428, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-24 02:54:42', '2018-11-24 02:54:42'),
(429, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-24 02:55:02', '2018-11-24 02:55:02'),
(430, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-24 02:55:24', '2018-11-24 02:55:24'),
(431, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 02:55:44', '2018-11-24 02:55:44'),
(432, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 02:55:47', '2018-11-24 02:55:47'),
(433, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 02:55:49', '2018-11-24 02:55:49'),
(434, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-24 02:55:56', '2018-11-24 02:55:56'),
(435, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 02:55:59', '2018-11-24 02:55:59'),
(436, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-24 02:57:15', '2018-11-24 02:57:15'),
(437, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-24 02:58:15', '2018-11-24 02:58:15'),
(438, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 02:59:02', '2018-11-24 02:59:02'),
(439, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 02:59:04', '2018-11-24 02:59:04'),
(440, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-24 02:59:33', '2018-11-24 02:59:33'),
(441, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 02:59:36', '2018-11-24 02:59:36'),
(442, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 02:59:42', '2018-11-24 02:59:42'),
(443, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 02:59:48', '2018-11-24 02:59:48'),
(444, 1, 'admin/auth/menu/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 02:59:50', '2018-11-24 02:59:50'),
(445, 1, 'admin/auth/menu/1', 'PUT', '127.0.0.1', '{\"parent_id\":\"0\",\"title\":\"Dashboard\",\"icon\":\"fa-bar-chart\",\"uri\":\"\\/\",\"roles\":[null],\"permission\":null,\"_token\":\"bYDO7B9jsGZcKYYcPzN6hf01okrGXGXG0dO5G6nj\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/auth\\/menu\"}', '2018-11-24 02:59:57', '2018-11-24 02:59:57'),
(446, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-11-24 02:59:57', '2018-11-24 02:59:57'),
(447, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-11-24 03:00:03', '2018-11-24 03:00:03'),
(448, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-11-24 03:00:08', '2018-11-24 03:00:08'),
(449, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 03:00:20', '2018-11-24 03:00:20'),
(450, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 03:03:02', '2018-11-24 03:03:02'),
(451, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 03:03:05', '2018-11-24 03:03:05'),
(452, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 03:03:08', '2018-11-24 03:03:08'),
(453, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 03:03:40', '2018-11-24 03:03:40'),
(454, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-11-24 03:03:44', '2018-11-24 03:03:44'),
(455, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 03:03:48', '2018-11-24 03:03:48'),
(456, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-24 03:03:51', '2018-11-24 03:03:51'),
(457, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 03:03:55', '2018-11-24 03:03:55'),
(458, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 03:03:57', '2018-11-24 03:03:57'),
(459, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 03:03:58', '2018-11-24 03:03:58'),
(460, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 03:03:59', '2018-11-24 03:03:59'),
(461, 1, 'admin/packages/1', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 03:04:06', '2018-11-24 03:04:06'),
(462, 1, 'admin/packages/1', 'GET', '127.0.0.1', '[]', '2018-11-24 03:04:07', '2018-11-24 03:04:07'),
(463, 1, 'admin/packages', 'GET', '127.0.0.1', '[]', '2018-11-24 03:04:13', '2018-11-24 03:04:13'),
(464, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 03:05:12', '2018-11-24 03:05:12'),
(465, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 03:05:14', '2018-11-24 03:05:14'),
(466, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 03:06:01', '2018-11-24 03:06:01'),
(467, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 03:06:04', '2018-11-24 03:06:04'),
(468, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 03:06:10', '2018-11-24 03:06:10'),
(469, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 03:06:12', '2018-11-24 03:06:12'),
(470, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 03:06:14', '2018-11-24 03:06:14'),
(471, 1, 'admin/packages', 'GET', '127.0.0.1', '[]', '2018-11-24 03:06:18', '2018-11-24 03:06:18'),
(472, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 03:06:23', '2018-11-24 03:06:23'),
(473, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 03:06:38', '2018-11-24 03:06:38'),
(474, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 03:06:42', '2018-11-24 03:06:42'),
(475, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 03:06:43', '2018-11-24 03:06:43'),
(476, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 03:06:43', '2018-11-24 03:06:43'),
(477, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-24 03:06:47', '2018-11-24 03:06:47'),
(478, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 03:07:10', '2018-11-24 03:07:10'),
(479, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-24 03:12:20', '2018-11-24 03:12:20'),
(480, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 03:12:23', '2018-11-24 03:12:23'),
(481, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 03:12:25', '2018-11-24 03:12:25'),
(482, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 03:12:25', '2018-11-24 03:12:25'),
(483, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 03:12:26', '2018-11-24 03:12:26'),
(484, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 03:12:27', '2018-11-24 03:12:27'),
(485, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 03:12:28', '2018-11-24 03:12:28'),
(486, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 03:12:28', '2018-11-24 03:12:28'),
(487, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 03:12:29', '2018-11-24 03:12:29'),
(488, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 03:12:31', '2018-11-24 03:12:31'),
(489, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 03:12:31', '2018-11-24 03:12:31'),
(490, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 03:12:32', '2018-11-24 03:12:32'),
(491, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 03:12:33', '2018-11-24 03:12:33'),
(492, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 03:12:34', '2018-11-24 03:12:34'),
(493, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 03:13:48', '2018-11-24 03:13:48'),
(494, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 03:13:50', '2018-11-24 03:13:50'),
(495, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 03:13:50', '2018-11-24 03:13:50'),
(496, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 03:13:51', '2018-11-24 03:13:51'),
(497, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-24 03:13:54', '2018-11-24 03:13:54'),
(498, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 03:14:01', '2018-11-24 03:14:01'),
(499, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-24 03:14:04', '2018-11-24 03:14:04'),
(500, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 03:23:32', '2018-11-24 03:23:32'),
(501, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '[]', '2018-11-24 03:57:24', '2018-11-24 03:57:24'),
(502, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 03:57:28', '2018-11-24 03:57:28'),
(503, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '[]', '2018-11-24 03:57:44', '2018-11-24 03:57:44'),
(504, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '[]', '2018-11-24 03:58:13', '2018-11-24 03:58:13'),
(505, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '[]', '2018-11-24 03:58:16', '2018-11-24 03:58:16'),
(506, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '[]', '2018-11-24 03:58:45', '2018-11-24 03:58:45'),
(507, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '[]', '2018-11-24 04:00:15', '2018-11-24 04:00:15'),
(508, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 04:00:43', '2018-11-24 04:00:43'),
(509, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 04:00:57', '2018-11-24 04:00:57'),
(510, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 04:00:59', '2018-11-24 04:00:59'),
(511, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 04:01:35', '2018-11-24 04:01:35'),
(512, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-11-24 04:17:13', '2018-11-24 04:17:13'),
(513, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 04:17:16', '2018-11-24 04:17:16'),
(514, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 04:17:18', '2018-11-24 04:17:18'),
(515, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '[]', '2018-11-24 04:21:04', '2018-11-24 04:21:04'),
(516, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-11-24 04:23:02', '2018-11-24 04:23:02'),
(517, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 04:23:08', '2018-11-24 04:23:08'),
(518, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 04:23:09', '2018-11-24 04:23:09'),
(519, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 04:23:09', '2018-11-24 04:23:09'),
(520, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-24 04:23:12', '2018-11-24 04:23:12'),
(521, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-24 04:23:21', '2018-11-24 04:23:21'),
(522, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 04:23:31', '2018-11-24 04:23:31'),
(523, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 04:23:35', '2018-11-24 04:23:35'),
(524, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '[]', '2018-11-24 04:25:52', '2018-11-24 04:25:52'),
(525, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '[]', '2018-11-24 04:29:40', '2018-11-24 04:29:40'),
(526, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '[]', '2018-11-24 04:30:13', '2018-11-24 04:30:13'),
(527, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '[]', '2018-11-24 04:30:59', '2018-11-24 04:30:59'),
(528, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '[]', '2018-11-24 04:31:27', '2018-11-24 04:31:27'),
(529, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '[]', '2018-11-24 04:31:48', '2018-11-24 04:31:48'),
(530, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '[]', '2018-11-24 04:32:03', '2018-11-24 04:32:03'),
(531, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '[]', '2018-11-24 04:35:43', '2018-11-24 04:35:43'),
(532, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '[]', '2018-11-24 04:36:19', '2018-11-24 04:36:19'),
(533, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '[]', '2018-11-24 04:36:37', '2018-11-24 04:36:37'),
(534, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '[]', '2018-11-24 04:37:01', '2018-11-24 04:37:01'),
(535, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '[]', '2018-11-24 04:37:33', '2018-11-24 04:37:33'),
(536, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '[]', '2018-11-24 04:39:57', '2018-11-24 04:39:57'),
(537, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '[]', '2018-11-24 04:40:36', '2018-11-24 04:40:36'),
(538, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '[]', '2018-11-24 04:41:19', '2018-11-24 04:41:19'),
(539, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '[]', '2018-11-24 04:41:36', '2018-11-24 04:41:36'),
(540, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '[]', '2018-11-24 04:42:11', '2018-11-24 04:42:11'),
(541, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-24 04:43:02', '2018-11-24 04:43:02'),
(542, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-11-26 02:15:40', '2018-11-26 02:15:40'),
(543, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-26 02:19:44', '2018-11-26 02:19:44'),
(544, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-26 02:19:52', '2018-11-26 02:19:52'),
(545, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-26 02:19:54', '2018-11-26 02:19:54'),
(546, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-26 02:20:17', '2018-11-26 02:20:17'),
(547, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-11-26 02:20:21', '2018-11-26 02:20:21'),
(548, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-26 02:52:11', '2018-11-26 02:52:11'),
(549, 1, 'admin/news', 'GET', '127.0.0.1', '[]', '2018-11-26 02:52:24', '2018-11-26 02:52:24'),
(550, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-26 02:52:31', '2018-11-26 02:52:31'),
(551, 1, 'admin/auth/menu/12/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-26 02:52:39', '2018-11-26 02:52:39'),
(552, 1, 'admin/auth/menu/12', 'PUT', '127.0.0.1', '{\"parent_id\":\"0\",\"title\":\"Traning Contents\",\"icon\":\"fa-hourglass-start\",\"uri\":\"training_contents\",\"roles\":[\"1\",null],\"permission\":\"*\",\"_token\":\"5i68C9kq5g9Ez9BMY2UXNJOuw50bDik0g6MIhULZ\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/auth\\/menu\"}', '2018-11-26 02:52:57', '2018-11-26 02:52:57'),
(553, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-11-26 02:52:57', '2018-11-26 02:52:57'),
(554, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-26 02:53:10', '2018-11-26 02:53:10'),
(555, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-11-26 02:53:50', '2018-11-26 02:53:50'),
(556, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-11-26 02:53:52', '2018-11-26 02:53:52'),
(557, 1, 'admin/training_contents', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-26 02:53:55', '2018-11-26 02:53:55'),
(558, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-26 02:54:03', '2018-11-26 02:54:03'),
(559, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-26 02:54:09', '2018-11-26 02:54:09'),
(560, 1, 'admin/auth/setting', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-26 02:54:17', '2018-11-26 02:54:17'),
(561, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-26 02:54:19', '2018-11-26 02:54:19'),
(562, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-26 02:54:20', '2018-11-26 02:54:20'),
(563, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-26 02:54:21', '2018-11-26 02:54:21'),
(564, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-26 02:54:22', '2018-11-26 02:54:22'),
(565, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-26 02:54:23', '2018-11-26 02:54:23'),
(566, 1, 'admin/training_contents', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-26 02:54:24', '2018-11-26 02:54:24'),
(567, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-26 02:55:27', '2018-11-26 02:55:27'),
(568, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-26 02:55:32', '2018-11-26 02:55:32'),
(569, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-26 02:55:34', '2018-11-26 02:55:34'),
(570, 1, 'admin/training_contents', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-26 02:55:35', '2018-11-26 02:55:35'),
(571, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-26 02:55:36', '2018-11-26 02:55:36'),
(572, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-26 02:55:38', '2018-11-26 02:55:38'),
(573, 1, 'admin/training_contents', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-26 02:55:38', '2018-11-26 02:55:38'),
(574, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-26 02:55:39', '2018-11-26 02:55:39'),
(575, 1, 'admin/training_contents', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-26 02:55:42', '2018-11-26 02:55:42'),
(576, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-26 02:55:42', '2018-11-26 02:55:42'),
(577, 1, 'admin/news', 'GET', '127.0.0.1', '[]', '2018-11-26 02:58:55', '2018-11-26 02:58:55'),
(578, 1, 'admin/news/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-26 02:59:15', '2018-11-26 02:59:15'),
(579, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-26 03:00:30', '2018-11-26 03:00:30'),
(580, 1, 'admin/news/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-26 03:00:31', '2018-11-26 03:00:31'),
(581, 1, 'admin/training_contents', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-26 03:00:35', '2018-11-26 03:00:35'),
(582, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-26 03:00:37', '2018-11-26 03:00:37'),
(583, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-26 03:00:39', '2018-11-26 03:00:39'),
(584, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-26 03:00:43', '2018-11-26 03:00:43'),
(585, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-26 03:00:43', '2018-11-26 03:00:43'),
(586, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-26 03:00:44', '2018-11-26 03:00:44'),
(587, 1, 'admin/training_contents', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-26 03:00:45', '2018-11-26 03:00:45'),
(588, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-26 03:00:46', '2018-11-26 03:00:46'),
(589, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-26 03:00:47', '2018-11-26 03:00:47'),
(590, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-26 03:00:51', '2018-11-26 03:00:51'),
(591, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-11-26 03:10:43', '2018-11-26 03:10:43'),
(592, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-26 03:10:46', '2018-11-26 03:10:46'),
(593, 1, 'admin/news/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-26 03:10:47', '2018-11-26 03:10:47'),
(594, 1, 'admin/news', 'POST', '127.0.0.1', '{\"title\":\"What is Lorem Ipsum?\",\"content\":\"<p><strong>Lorem Ipsum<\\/strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<\\/p>\",\"_token\":\"2UDJhhK1dSzzFrcQKu3P8Z84SCQ8YMJHIaOgedfM\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/news\"}', '2018-11-26 03:11:07', '2018-11-26 03:11:07'),
(595, 1, 'admin/news', 'GET', '127.0.0.1', '[]', '2018-11-26 03:11:07', '2018-11-26 03:11:07'),
(596, 1, 'admin/news/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-26 03:11:20', '2018-11-26 03:11:20'),
(597, 1, 'admin/news', 'POST', '127.0.0.1', '{\"title\":\"Where does it come from?\",\"content\":\"<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot; (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32.<\\/p>\\r\\n\\r\\n<p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from &quot;de Finibus Bonorum et Malorum&quot; by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.<\\/p>\",\"_token\":\"2UDJhhK1dSzzFrcQKu3P8Z84SCQ8YMJHIaOgedfM\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/news\"}', '2018-11-26 03:11:38', '2018-11-26 03:11:38'),
(598, 1, 'admin/news', 'GET', '127.0.0.1', '[]', '2018-11-26 03:11:38', '2018-11-26 03:11:38'),
(599, 1, 'admin/news', 'GET', '127.0.0.1', '[]', '2018-11-26 03:12:35', '2018-11-26 03:12:35'),
(600, 1, 'admin/training_contents', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-26 03:12:42', '2018-11-26 03:12:42'),
(601, 1, 'admin/training_contents/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-26 03:12:43', '2018-11-26 03:12:43'),
(602, 1, 'admin/training_contents', 'POST', '127.0.0.1', '{\"caption\":\"Where can I get some?\",\"content\":\"<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don&#39;t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn&#39;t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.<\\/p>\",\"_token\":\"2UDJhhK1dSzzFrcQKu3P8Z84SCQ8YMJHIaOgedfM\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/training_contents\"}', '2018-11-26 03:13:10', '2018-11-26 03:13:10'),
(603, 1, 'admin/training_contents', 'GET', '127.0.0.1', '[]', '2018-11-26 03:13:11', '2018-11-26 03:13:11'),
(604, 1, 'admin/training_contents/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-26 03:13:20', '2018-11-26 03:13:20'),
(605, 1, 'admin/training_contents', 'POST', '127.0.0.1', '{\"caption\":\"Why do we use it?\",\"content\":\"<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).<\\/p>\",\"_token\":\"2UDJhhK1dSzzFrcQKu3P8Z84SCQ8YMJHIaOgedfM\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/training_contents\"}', '2018-11-26 03:13:31', '2018-11-26 03:13:31'),
(606, 1, 'admin/training_contents', 'GET', '127.0.0.1', '[]', '2018-11-26 03:13:32', '2018-11-26 03:13:32'),
(607, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-26 03:13:35', '2018-11-26 03:13:35'),
(608, 1, 'admin/training_contents', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-26 03:13:36', '2018-11-26 03:13:36'),
(609, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-26 03:13:41', '2018-11-26 03:13:41'),
(610, 1, 'admin/training_contents', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-26 03:13:41', '2018-11-26 03:13:41'),
(611, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-26 03:13:42', '2018-11-26 03:13:42'),
(612, 1, 'admin/training_contents', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-26 03:13:43', '2018-11-26 03:13:43'),
(613, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-26 03:13:45', '2018-11-26 03:13:45'),
(614, 1, 'admin/training_contents', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-26 03:13:46', '2018-11-26 03:13:46'),
(615, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-26 03:13:47', '2018-11-26 03:13:47'),
(616, 1, 'admin/auth/setting', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-26 03:13:49', '2018-11-26 03:13:49'),
(617, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-26 03:13:51', '2018-11-26 03:13:51'),
(618, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-26 03:13:53', '2018-11-26 03:13:53'),
(619, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-26 03:13:54', '2018-11-26 03:13:54'),
(620, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-26 03:13:54', '2018-11-26 03:13:54'),
(621, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-26 03:13:56', '2018-11-26 03:13:56'),
(622, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-26 03:13:57', '2018-11-26 03:13:57'),
(623, 1, 'admin/training_contents', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-26 03:13:58', '2018-11-26 03:13:58'),
(624, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-26 03:14:03', '2018-11-26 03:14:03'),
(625, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-26 03:14:04', '2018-11-26 03:14:04'),
(626, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-26 03:14:04', '2018-11-26 03:14:04'),
(627, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-26 03:14:05', '2018-11-26 03:14:05'),
(628, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-26 03:14:07', '2018-11-26 03:14:07'),
(629, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-26 03:14:08', '2018-11-26 03:14:08'),
(630, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-26 03:14:08', '2018-11-26 03:14:08'),
(631, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-26 03:14:09', '2018-11-26 03:14:09'),
(632, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-26 03:14:10', '2018-11-26 03:14:10'),
(633, 1, 'admin/training_contents', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-26 03:14:11', '2018-11-26 03:14:11'),
(634, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-11-27 02:08:56', '2018-11-27 02:08:56'),
(635, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 02:09:03', '2018-11-27 02:09:03'),
(636, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 02:09:04', '2018-11-27 02:09:04'),
(637, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 02:09:07', '2018-11-27 02:09:07'),
(638, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 02:09:12', '2018-11-27 02:09:12'),
(639, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '[]', '2018-11-27 02:14:25', '2018-11-27 02:14:25'),
(640, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '[]', '2018-11-27 02:16:00', '2018-11-27 02:16:00'),
(641, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 02:16:18', '2018-11-27 02:16:18'),
(642, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 02:16:22', '2018-11-27 02:16:22'),
(643, 1, 'admin/users/1', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 02:16:24', '2018-11-27 02:16:24'),
(644, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 02:16:28', '2018-11-27 02:16:28'),
(645, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 02:16:29', '2018-11-27 02:16:29'),
(646, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 02:16:29', '2018-11-27 02:16:29'),
(647, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 02:16:31', '2018-11-27 02:16:31'),
(648, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '[]', '2018-11-27 02:17:49', '2018-11-27 02:17:49'),
(649, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '[]', '2018-11-27 02:18:03', '2018-11-27 02:18:03'),
(650, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '[]', '2018-11-27 02:18:44', '2018-11-27 02:18:44'),
(651, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '[]', '2018-11-27 02:19:03', '2018-11-27 02:19:03'),
(652, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '[]', '2018-11-27 02:19:36', '2018-11-27 02:19:36'),
(653, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '[]', '2018-11-27 02:19:56', '2018-11-27 02:19:56'),
(654, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '[]', '2018-11-27 02:20:09', '2018-11-27 02:20:09'),
(655, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '[]', '2018-11-27 02:20:25', '2018-11-27 02:20:25'),
(656, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '[]', '2018-11-27 02:21:20', '2018-11-27 02:21:20'),
(657, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '[]', '2018-11-27 02:30:39', '2018-11-27 02:30:39'),
(658, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '[]', '2018-11-27 02:33:09', '2018-11-27 02:33:09'),
(659, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '[]', '2018-11-27 02:43:27', '2018-11-27 02:43:27'),
(660, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '[]', '2018-11-27 02:44:12', '2018-11-27 02:44:12'),
(661, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 02:44:40', '2018-11-27 02:44:40'),
(662, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 02:44:44', '2018-11-27 02:44:44'),
(663, 1, 'admin/training_contents', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 02:44:45', '2018-11-27 02:44:45'),
(664, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 02:44:47', '2018-11-27 02:44:47'),
(665, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 02:46:23', '2018-11-27 02:46:23'),
(666, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 02:46:24', '2018-11-27 02:46:24'),
(667, 1, 'admin/additional_returns/janedoe', 'POST', '127.0.0.1', '{\"_token\":\"7MsTVWBCNlH6NGDFOdvXpJiBd2UTOWc9M3l95cgr\",\"month\":\"November - 18\"}', '2018-11-27 02:46:28', '2018-11-27 02:46:28'),
(668, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '[]', '2018-11-27 02:46:56', '2018-11-27 02:46:56'),
(669, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '[]', '2018-11-27 02:46:59', '2018-11-27 02:46:59'),
(670, 1, 'admin/additional_returns/janedoe', 'POST', '127.0.0.1', '{\"_token\":\"7MsTVWBCNlH6NGDFOdvXpJiBd2UTOWc9M3l95cgr\",\"month\":\"November - 18\"}', '2018-11-27 02:47:05', '2018-11-27 02:47:05'),
(671, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '[]', '2018-11-27 02:47:16', '2018-11-27 02:47:16'),
(672, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '[]', '2018-11-27 02:47:19', '2018-11-27 02:47:19'),
(673, 1, 'admin/additional_returns/janedoe', 'POST', '127.0.0.1', '{\"_token\":\"7MsTVWBCNlH6NGDFOdvXpJiBd2UTOWc9M3l95cgr\",\"month\":\"November - 18\"}', '2018-11-27 02:47:22', '2018-11-27 02:47:22'),
(674, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '[]', '2018-11-27 02:47:24', '2018-11-27 02:47:24'),
(675, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-27 02:47:29', '2018-11-27 02:47:29'),
(676, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-27 02:47:31', '2018-11-27 02:47:31'),
(677, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-27 02:47:34', '2018-11-27 02:47:34'),
(678, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 02:47:38', '2018-11-27 02:47:38'),
(679, 1, 'admin/additional_returns/janedoe', 'POST', '127.0.0.1', '{\"_token\":\"7MsTVWBCNlH6NGDFOdvXpJiBd2UTOWc9M3l95cgr\",\"month\":\"November - 18\"}', '2018-11-27 02:47:56', '2018-11-27 02:47:56'),
(680, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '[]', '2018-11-27 02:48:10', '2018-11-27 02:48:10'),
(681, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '[]', '2018-11-27 02:48:12', '2018-11-27 02:48:12'),
(682, 1, 'admin/additional_returns/janedoe', 'POST', '127.0.0.1', '{\"_token\":\"7MsTVWBCNlH6NGDFOdvXpJiBd2UTOWc9M3l95cgr\",\"month\":\"November - 18\",\"amount\":\"100\"}', '2018-11-27 02:48:16', '2018-11-27 02:48:16'),
(683, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '[]', '2018-11-27 02:57:12', '2018-11-27 02:57:12'),
(684, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '[]', '2018-11-27 02:57:15', '2018-11-27 02:57:15'),
(685, 1, 'admin/additional_returns/janedoe', 'POST', '127.0.0.1', '{\"_token\":\"7MsTVWBCNlH6NGDFOdvXpJiBd2UTOWc9M3l95cgr\",\"month\":\"November - 18\",\"amount\":\"100\"}', '2018-11-27 02:57:23', '2018-11-27 02:57:23'),
(686, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '[]', '2018-11-27 02:57:23', '2018-11-27 02:57:23'),
(687, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '[]', '2018-11-27 02:58:44', '2018-11-27 02:58:44'),
(688, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '[]', '2018-11-27 03:00:18', '2018-11-27 03:00:18'),
(689, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '[]', '2018-11-27 03:02:07', '2018-11-27 03:02:07'),
(690, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '[]', '2018-11-27 03:02:20', '2018-11-27 03:02:20'),
(691, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '[]', '2018-11-27 03:02:54', '2018-11-27 03:02:54'),
(692, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '[]', '2018-11-27 03:03:50', '2018-11-27 03:03:50'),
(693, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '[]', '2018-11-27 03:04:22', '2018-11-27 03:04:22'),
(694, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '[]', '2018-11-27 03:04:34', '2018-11-27 03:04:34'),
(695, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 03:05:43', '2018-11-27 03:05:43'),
(696, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 03:05:44', '2018-11-27 03:05:44'),
(697, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 03:05:54', '2018-11-27 03:05:54'),
(698, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 03:05:55', '2018-11-27 03:05:55'),
(699, 1, 'admin/training_contents', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 03:05:55', '2018-11-27 03:05:55'),
(700, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 03:05:57', '2018-11-27 03:05:57'),
(701, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 03:06:01', '2018-11-27 03:06:01'),
(702, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 03:06:03', '2018-11-27 03:06:03'),
(703, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 03:06:04', '2018-11-27 03:06:04'),
(704, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 03:06:05', '2018-11-27 03:06:05'),
(705, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '[]', '2018-11-27 03:07:07', '2018-11-27 03:07:07'),
(706, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '[]', '2018-11-27 03:08:06', '2018-11-27 03:08:06'),
(707, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '[]', '2018-11-27 03:08:12', '2018-11-27 03:08:12'),
(708, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '[]', '2018-11-27 03:08:48', '2018-11-27 03:08:48'),
(709, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 03:09:50', '2018-11-27 03:09:50'),
(710, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 03:12:34', '2018-11-27 03:12:34'),
(711, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 03:14:57', '2018-11-27 03:14:57'),
(712, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 03:14:58', '2018-11-27 03:14:58'),
(713, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '[]', '2018-11-27 03:15:53', '2018-11-27 03:15:53'),
(714, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '[]', '2018-11-27 03:15:58', '2018-11-27 03:15:58'),
(715, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 03:16:14', '2018-11-27 03:16:14'),
(716, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 03:16:15', '2018-11-27 03:16:15'),
(717, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '[]', '2018-11-27 03:17:38', '2018-11-27 03:17:38'),
(718, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 03:17:41', '2018-11-27 03:17:41'),
(719, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-11-27 03:17:43', '2018-11-27 03:17:43'),
(720, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 03:17:47', '2018-11-27 03:17:47'),
(721, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 03:17:48', '2018-11-27 03:17:48'),
(722, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 03:17:50', '2018-11-27 03:17:50'),
(723, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-27 03:20:25', '2018-11-27 03:20:25'),
(724, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-27 03:20:29', '2018-11-27 03:20:29'),
(725, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-27 03:21:45', '2018-11-27 03:21:45'),
(726, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 03:21:59', '2018-11-27 03:21:59'),
(727, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 03:22:02', '2018-11-27 03:22:02'),
(728, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 03:22:28', '2018-11-27 03:22:28'),
(729, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 03:22:42', '2018-11-27 03:22:42'),
(730, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 03:22:43', '2018-11-27 03:22:43'),
(731, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 03:22:43', '2018-11-27 03:22:43'),
(732, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 03:22:44', '2018-11-27 03:22:44'),
(733, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 03:22:45', '2018-11-27 03:22:45'),
(734, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 03:22:46', '2018-11-27 03:22:46'),
(735, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 03:22:48', '2018-11-27 03:22:48'),
(736, 1, 'admin/training_contents', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 03:22:56', '2018-11-27 03:22:56'),
(737, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 03:23:07', '2018-11-27 03:23:07'),
(738, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 03:23:08', '2018-11-27 03:23:08'),
(739, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 03:23:09', '2018-11-27 03:23:09'),
(740, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 03:23:10', '2018-11-27 03:23:10'),
(741, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 03:23:11', '2018-11-27 03:23:11'),
(742, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 03:23:11', '2018-11-27 03:23:11'),
(743, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 03:23:12', '2018-11-27 03:23:12'),
(744, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 03:23:16', '2018-11-27 03:23:16'),
(745, 1, 'admin/training_contents', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 03:23:16', '2018-11-27 03:23:16'),
(746, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 03:23:24', '2018-11-27 03:23:24'),
(747, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 03:23:25', '2018-11-27 03:23:25'),
(748, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 03:23:36', '2018-11-27 03:23:36');
INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(749, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 03:23:38', '2018-11-27 03:23:38'),
(750, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 03:23:39', '2018-11-27 03:23:39'),
(751, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 03:23:40', '2018-11-27 03:23:40'),
(752, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 03:23:40', '2018-11-27 03:23:40'),
(753, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 03:23:41', '2018-11-27 03:23:41'),
(754, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 03:23:42', '2018-11-27 03:23:42'),
(755, 1, 'admin/training_contents', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 03:23:43', '2018-11-27 03:23:43'),
(756, 1, 'admin/training_contents', 'GET', '127.0.0.1', '[]', '2018-11-27 03:28:59', '2018-11-27 03:28:59'),
(757, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 03:29:02', '2018-11-27 03:29:02'),
(758, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-27 03:29:25', '2018-11-27 03:29:25'),
(759, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 03:40:15', '2018-11-27 03:40:15'),
(760, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-27 03:40:18', '2018-11-27 03:40:18'),
(761, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-27 03:40:50', '2018-11-27 03:40:50'),
(762, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-27 03:40:53', '2018-11-27 03:40:53'),
(763, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 03:45:05', '2018-11-27 03:45:05'),
(764, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-27 03:45:06', '2018-11-27 03:45:06'),
(765, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-27 03:45:06', '2018-11-27 03:45:06'),
(766, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-27 03:45:06', '2018-11-27 03:45:06'),
(767, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-27 03:45:06', '2018-11-27 03:45:06'),
(768, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-27 03:45:06', '2018-11-27 03:45:06'),
(769, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-27 03:45:06', '2018-11-27 03:45:06'),
(770, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-27 03:45:06', '2018-11-27 03:45:06'),
(771, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-27 03:45:06', '2018-11-27 03:45:06'),
(772, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-27 03:45:07', '2018-11-27 03:45:07'),
(773, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-27 03:45:07', '2018-11-27 03:45:07'),
(774, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-27 03:45:07', '2018-11-27 03:45:07'),
(775, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-27 03:45:07', '2018-11-27 03:45:07'),
(776, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-27 03:45:07', '2018-11-27 03:45:07'),
(777, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-27 03:45:07', '2018-11-27 03:45:07'),
(778, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-27 03:45:07', '2018-11-27 03:45:07'),
(779, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-27 03:45:07', '2018-11-27 03:45:07'),
(780, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-27 03:45:07', '2018-11-27 03:45:07'),
(781, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-27 03:45:08', '2018-11-27 03:45:08'),
(782, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-27 03:45:08', '2018-11-27 03:45:08'),
(783, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-27 03:45:08', '2018-11-27 03:45:08'),
(784, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-27 03:45:08', '2018-11-27 03:45:08'),
(785, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-27 03:45:46', '2018-11-27 03:45:46'),
(786, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-27 03:46:11', '2018-11-27 03:46:11'),
(787, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-27 03:46:24', '2018-11-27 03:46:24'),
(788, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 03:46:36', '2018-11-27 03:46:36'),
(789, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 03:46:39', '2018-11-27 03:46:39'),
(790, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 03:46:40', '2018-11-27 03:46:40'),
(791, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 03:46:41', '2018-11-27 03:46:41'),
(792, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 03:46:42', '2018-11-27 03:46:42'),
(793, 1, 'admin/training_contents', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 03:46:43', '2018-11-27 03:46:43'),
(794, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 03:46:44', '2018-11-27 03:46:44'),
(795, 1, 'admin', 'GET', '85.17.24.66', '[]', '2018-11-27 03:55:43', '2018-11-27 03:55:43'),
(796, 1, 'admin/additional_returns', 'GET', '85.17.24.66', '[]', '2018-11-27 03:55:52', '2018-11-27 03:55:52'),
(797, 1, 'admin/additional_returns/janedoe', 'GET', '85.17.24.66', '[]', '2018-11-27 03:56:02', '2018-11-27 03:56:02'),
(798, 1, 'admin', 'GET', '202.4.108.142', '[]', '2018-11-27 03:56:54', '2018-11-27 03:56:54'),
(799, 1, 'admin/users', 'GET', '85.17.24.66', '[]', '2018-11-27 03:57:06', '2018-11-27 03:57:06'),
(800, 1, 'admin/auth/users', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 03:57:09', '2018-11-27 03:57:09'),
(801, 1, 'admin/auth/roles', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 03:57:12', '2018-11-27 03:57:12'),
(802, 1, 'admin/auth/permissions', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 03:57:15', '2018-11-27 03:57:15'),
(803, 1, 'admin/packages', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 03:57:20', '2018-11-27 03:57:20'),
(804, 1, 'admin', 'GET', '85.17.24.66', '[]', '2018-11-27 03:57:26', '2018-11-27 03:57:26'),
(805, 1, 'admin/users', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 03:57:32', '2018-11-27 03:57:32'),
(806, 1, 'admin/additional_returns', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 03:57:38', '2018-11-27 03:57:38'),
(807, 1, 'admin/training_contents', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 03:57:44', '2018-11-27 03:57:44'),
(808, 1, 'admin', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 03:57:59', '2018-11-27 03:57:59'),
(809, 1, 'admin/auth/setting', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 03:58:03', '2018-11-27 03:58:03'),
(810, 1, 'admin/users', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 03:58:10', '2018-11-27 03:58:10'),
(811, 1, 'admin/users/1/edit', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 03:58:15', '2018-11-27 03:58:15'),
(812, 1, 'admin/users/1/edit', 'GET', '202.4.108.142', '[]', '2018-11-27 03:58:35', '2018-11-27 03:58:35'),
(813, 1, 'admin/packages', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 03:58:40', '2018-11-27 03:58:40'),
(814, 1, 'admin/packages/3/edit', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 03:58:46', '2018-11-27 03:58:46'),
(815, 1, 'admin/auth/menu', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 03:58:58', '2018-11-27 03:58:58'),
(816, 1, 'admin/users', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 04:00:02', '2018-11-27 04:00:02'),
(817, 1, 'admin/users/1', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 04:00:15', '2018-11-27 04:00:15'),
(818, 1, 'admin', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 04:00:27', '2018-11-27 04:00:27'),
(819, 1, 'admin/auth/roles', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 04:01:05', '2018-11-27 04:01:05'),
(820, 1, 'admin/auth/roles/1/edit', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 04:01:13', '2018-11-27 04:01:13'),
(821, 1, 'admin/auth/menu', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 04:01:28', '2018-11-27 04:01:28'),
(822, 1, 'admin/auth/users', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 04:01:32', '2018-11-27 04:01:32'),
(823, 1, 'admin/packages', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 04:01:39', '2018-11-27 04:01:39'),
(824, 1, 'admin/users', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 04:01:49', '2018-11-27 04:01:49'),
(825, 1, 'admin/users/create', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 04:01:52', '2018-11-27 04:01:52'),
(826, 1, 'admin/users/create', 'GET', '202.4.108.142', '[]', '2018-11-27 04:02:13', '2018-11-27 04:02:13'),
(827, 1, 'admin', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 04:02:17', '2018-11-27 04:02:17'),
(828, 1, 'admin/auth/users', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 04:02:47', '2018-11-27 04:02:47'),
(829, 1, 'admin/auth/roles', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 04:02:48', '2018-11-27 04:02:48'),
(830, 1, 'admin/additional_returns', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 04:02:54', '2018-11-27 04:02:54'),
(831, 1, 'admin/training_contents', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 04:03:34', '2018-11-27 04:03:34'),
(832, 1, 'admin/packages', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 04:03:45', '2018-11-27 04:03:45'),
(833, 1, 'admin/auth/users', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 04:03:48', '2018-11-27 04:03:48'),
(834, 1, 'admin/packages', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 04:04:05', '2018-11-27 04:04:05'),
(835, 1, 'admin/packages', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 04:04:07', '2018-11-27 04:04:07'),
(836, 1, 'admin/users', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 04:04:20', '2018-11-27 04:04:20'),
(837, 1, 'admin/users/1', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 04:04:26', '2018-11-27 04:04:26'),
(838, 1, 'admin', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 04:04:35', '2018-11-27 04:04:35'),
(839, 1, 'admin/additional_returns', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 04:04:41', '2018-11-27 04:04:41'),
(840, 1, 'admin/users', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 04:04:42', '2018-11-27 04:04:42'),
(841, 1, 'admin', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 04:06:07', '2018-11-27 04:06:07'),
(842, 1, 'admin/auth/roles', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 04:06:10', '2018-11-27 04:06:10'),
(843, 1, 'admin', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 04:06:23', '2018-11-27 04:06:23'),
(844, 1, 'admin/training_contents', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 04:06:26', '2018-11-27 04:06:26'),
(845, 1, 'admin/training_contents/2', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 04:06:29', '2018-11-27 04:06:29'),
(846, 1, 'admin/training_contents', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 04:06:37', '2018-11-27 04:06:37'),
(847, 1, 'admin/training_contents/create', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 04:06:37', '2018-11-27 04:06:37'),
(848, 1, 'admin', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 04:06:53', '2018-11-27 04:06:53'),
(849, 1, 'admin/packages', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 04:06:55', '2018-11-27 04:06:55'),
(850, 1, 'admin/auth/users', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 04:07:03', '2018-11-27 04:07:03'),
(851, 1, 'admin/auth/users/create', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 04:07:06', '2018-11-27 04:07:06'),
(852, 1, 'admin/auth/users/create', 'GET', '202.4.108.142', '[]', '2018-11-27 04:07:52', '2018-11-27 04:07:52'),
(853, 1, 'admin', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 04:07:56', '2018-11-27 04:07:56'),
(854, 1, 'admin/users', 'GET', '202.4.108.142', '[]', '2018-11-27 04:31:31', '2018-11-27 04:31:31'),
(855, 1, 'admin/users/create', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 04:32:31', '2018-11-27 04:32:31'),
(856, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-11-27 05:04:36', '2018-11-27 05:04:36'),
(857, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:05:08', '2018-11-27 05:05:08'),
(858, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:05:12', '2018-11-27 05:05:12'),
(859, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:05:14', '2018-11-27 05:05:14'),
(860, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:05:16', '2018-11-27 05:05:16'),
(861, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:05:17', '2018-11-27 05:05:17'),
(862, 1, 'admin/training_contents', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:05:19', '2018-11-27 05:05:19'),
(863, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:05:19', '2018-11-27 05:05:19'),
(864, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:05:21', '2018-11-27 05:05:21'),
(865, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:05:23', '2018-11-27 05:05:23'),
(866, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:05:37', '2018-11-27 05:05:37'),
(867, 1, 'admin/users', 'GET', '202.4.108.142', '[]', '2018-11-27 05:09:18', '2018-11-27 05:09:18'),
(868, 1, 'admin/users', 'POST', '202.4.108.142', '{\"first_name\":\"Kamrul\",\"last_name\":\"Islam\",\"username\":\"rono\",\"phone\":\"01716791679\",\"email\":\"dreamfighterr@gmail.com\",\"password\":\"274675\",\"invest_amount\":{\"package_id\":\"3\",\"referral_code\":\"janedoe\",\"status\":\"paid\"},\"_token\":\"T4mxSZZDL7Z8PZyL3G9WTWaQ0fl3f6aKtcWnN0sM\",\"_previous_\":\"http:\\/\\/27.147.230.244\\/admin\\/users\"}', '2018-11-27 05:09:31', '2018-11-27 05:09:31'),
(869, 1, 'admin/users', 'GET', '202.4.108.142', '[]', '2018-11-27 05:09:32', '2018-11-27 05:09:32'),
(870, 1, 'admin/users/create', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:09:50', '2018-11-27 05:09:50'),
(871, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:10:31', '2018-11-27 05:10:31'),
(872, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:10:34', '2018-11-27 05:10:34'),
(873, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:10:35', '2018-11-27 05:10:35'),
(874, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:10:37', '2018-11-27 05:10:37'),
(875, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:10:38', '2018-11-27 05:10:38'),
(876, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:10:40', '2018-11-27 05:10:40'),
(877, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:10:41', '2018-11-27 05:10:41'),
(878, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:10:42', '2018-11-27 05:10:42'),
(879, 1, 'admin/users/1', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:10:45', '2018-11-27 05:10:45'),
(880, 1, 'admin/users', 'POST', '202.4.108.142', '{\"first_name\":\"Test\",\"last_name\":\"Islam\",\"username\":\"islam\",\"phone\":\"46446464___\",\"email\":\"dreamfighterr69@gmail.com\",\"password\":\"274675\",\"invest_amount\":{\"package_id\":\"2\",\"referral_code\":\"rono\",\"status\":\"paid\"},\"_token\":\"T4mxSZZDL7Z8PZyL3G9WTWaQ0fl3f6aKtcWnN0sM\",\"_previous_\":\"http:\\/\\/27.147.230.244\\/admin\\/users\"}', '2018-11-27 05:10:47', '2018-11-27 05:10:47'),
(881, 1, 'admin/users/create', 'GET', '202.4.108.142', '[]', '2018-11-27 05:10:48', '2018-11-27 05:10:48'),
(882, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:10:51', '2018-11-27 05:10:51'),
(883, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:10:55', '2018-11-27 05:10:55'),
(884, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:10:56', '2018-11-27 05:10:56'),
(885, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-11-27 05:10:57', '2018-11-27 05:10:57'),
(886, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:10:59', '2018-11-27 05:10:59'),
(887, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-11-27 05:10:59', '2018-11-27 05:10:59'),
(888, 1, 'admin/users', 'POST', '202.4.108.142', '{\"first_name\":\"Test\",\"last_name\":\"Islam\",\"username\":\"islam\",\"phone\":\"46446464___\",\"email\":\"dreamfighterr69@gmail.com\",\"password\":\"274675\",\"invest_amount\":{\"package_id\":\"2\",\"referral_code\":\"rono\",\"status\":\"paid\"},\"_token\":\"T4mxSZZDL7Z8PZyL3G9WTWaQ0fl3f6aKtcWnN0sM\"}', '2018-11-27 05:11:00', '2018-11-27 05:11:00'),
(889, 1, 'admin/users', 'GET', '202.4.108.142', '[]', '2018-11-27 05:11:00', '2018-11-27 05:11:00'),
(890, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:11:23', '2018-11-27 05:11:23'),
(891, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-11-27 05:11:24', '2018-11-27 05:11:24'),
(892, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:11:26', '2018-11-27 05:11:26'),
(893, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-11-27 05:11:27', '2018-11-27 05:11:27'),
(894, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:12:44', '2018-11-27 05:12:44'),
(895, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-11-27 05:12:44', '2018-11-27 05:12:44'),
(896, 1, 'admin/users/3', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:13:22', '2018-11-27 05:13:22'),
(897, 1, 'admin', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:13:29', '2018-11-27 05:13:29'),
(898, 1, 'admin/additional_returns', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:13:32', '2018-11-27 05:13:32'),
(899, 1, 'admin', 'GET', '202.4.108.142', '[]', '2018-11-27 05:13:32', '2018-11-27 05:13:32'),
(900, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:13:36', '2018-11-27 05:13:36'),
(901, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-11-27 05:13:36', '2018-11-27 05:13:36'),
(902, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-11-27 05:15:05', '2018-11-27 05:15:05'),
(903, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:15:08', '2018-11-27 05:15:08'),
(904, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-11-27 05:15:09', '2018-11-27 05:15:09'),
(905, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-11-27 05:17:17', '2018-11-27 05:17:17'),
(906, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:17:20', '2018-11-27 05:17:20'),
(907, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-11-27 05:17:21', '2018-11-27 05:17:21'),
(908, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:18:18', '2018-11-27 05:18:18'),
(909, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-11-27 05:18:19', '2018-11-27 05:18:19'),
(910, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-11-27 05:18:21', '2018-11-27 05:18:21'),
(911, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:18:25', '2018-11-27 05:18:25'),
(912, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-11-27 05:18:25', '2018-11-27 05:18:25'),
(913, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-11-27 05:18:50', '2018-11-27 05:18:50'),
(914, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:18:53', '2018-11-27 05:18:53'),
(915, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-11-27 05:18:53', '2018-11-27 05:18:53'),
(916, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-11-27 05:19:20', '2018-11-27 05:19:20'),
(917, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:19:23', '2018-11-27 05:19:23'),
(918, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-11-27 05:19:23', '2018-11-27 05:19:23'),
(919, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-11-27 05:23:55', '2018-11-27 05:23:55'),
(920, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:23:58', '2018-11-27 05:23:58'),
(921, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-27 05:24:03', '2018-11-27 05:24:03'),
(922, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:24:19', '2018-11-27 05:24:19'),
(923, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:24:23', '2018-11-27 05:24:23'),
(924, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:24:30', '2018-11-27 05:24:30'),
(925, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:24:31', '2018-11-27 05:24:31'),
(926, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:24:31', '2018-11-27 05:24:31'),
(927, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:24:33', '2018-11-27 05:24:33'),
(928, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:24:36', '2018-11-27 05:24:36'),
(929, 1, 'admin/training_contents', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:24:37', '2018-11-27 05:24:37'),
(930, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:24:40', '2018-11-27 05:24:40'),
(931, 1, 'admin/additional_returns/islam', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:24:42', '2018-11-27 05:24:42'),
(932, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:24:46', '2018-11-27 05:24:46'),
(933, 1, 'admin/additional_returns/rono', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:24:48', '2018-11-27 05:24:48'),
(934, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:24:58', '2018-11-27 05:24:58'),
(935, 1, 'admin/additional_returns/rono', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:25:01', '2018-11-27 05:25:01'),
(936, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:25:04', '2018-11-27 05:25:04'),
(937, 1, 'admin/additional_returns/rono', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:25:06', '2018-11-27 05:25:06'),
(938, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:25:08', '2018-11-27 05:25:08'),
(939, 1, 'admin/additional_returns/islam', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:25:11', '2018-11-27 05:25:11'),
(940, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:25:40', '2018-11-27 05:25:40'),
(941, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:25:44', '2018-11-27 05:25:44'),
(942, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:25:47', '2018-11-27 05:25:47'),
(943, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:25:50', '2018-11-27 05:25:50'),
(944, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:26:13', '2018-11-27 05:26:13'),
(945, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:26:34', '2018-11-27 05:26:34'),
(946, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:26:35', '2018-11-27 05:26:35'),
(947, 1, 'admin/training_contents', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:26:37', '2018-11-27 05:26:37'),
(948, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:26:38', '2018-11-27 05:26:38'),
(949, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:26:39', '2018-11-27 05:26:39'),
(950, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:26:40', '2018-11-27 05:26:40'),
(951, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:26:41', '2018-11-27 05:26:41'),
(952, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:26:42', '2018-11-27 05:26:42'),
(953, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:27:00', '2018-11-27 05:27:00'),
(954, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:27:56', '2018-11-27 05:27:56'),
(955, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:27:57', '2018-11-27 05:27:57'),
(956, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:27:58', '2018-11-27 05:27:58'),
(957, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:27:59', '2018-11-27 05:27:59'),
(958, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:28:13', '2018-11-27 05:28:13'),
(959, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:28:15', '2018-11-27 05:28:15'),
(960, 1, 'admin/training_contents', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:28:15', '2018-11-27 05:28:15'),
(961, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:28:16', '2018-11-27 05:28:16'),
(962, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:28:17', '2018-11-27 05:28:17'),
(963, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:28:20', '2018-11-27 05:28:20'),
(964, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:28:21', '2018-11-27 05:28:21'),
(965, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:28:21', '2018-11-27 05:28:21'),
(966, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:28:22', '2018-11-27 05:28:22'),
(967, 1, 'admin/training_contents', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:28:23', '2018-11-27 05:28:23'),
(968, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:28:25', '2018-11-27 05:28:25'),
(969, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:28:28', '2018-11-27 05:28:28'),
(970, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:28:57', '2018-11-27 05:28:57'),
(971, 1, 'admin/training_contents', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:28:58', '2018-11-27 05:28:58'),
(972, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:28:59', '2018-11-27 05:28:59'),
(973, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:29:01', '2018-11-27 05:29:01'),
(974, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:29:52', '2018-11-27 05:29:52'),
(975, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:29:53', '2018-11-27 05:29:53'),
(976, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:29:54', '2018-11-27 05:29:54'),
(977, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:29:54', '2018-11-27 05:29:54'),
(978, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:29:55', '2018-11-27 05:29:55'),
(979, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:29:57', '2018-11-27 05:29:57'),
(980, 1, 'admin/training_contents', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:29:57', '2018-11-27 05:29:57'),
(981, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:29:58', '2018-11-27 05:29:58'),
(982, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 05:29:59', '2018-11-27 05:29:59'),
(983, 1, 'admin/news', 'GET', '127.0.0.1', '[]', '2018-11-27 06:16:34', '2018-11-27 06:16:34'),
(984, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 06:16:39', '2018-11-27 06:16:39'),
(985, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 06:16:40', '2018-11-27 06:16:40'),
(986, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-11-27 06:36:24', '2018-11-27 06:36:24'),
(987, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 06:36:27', '2018-11-27 06:36:27'),
(988, 1, 'admin/additional_returns/islam', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 06:36:31', '2018-11-27 06:36:31'),
(989, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 06:37:07', '2018-11-27 06:37:07'),
(990, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 06:37:11', '2018-11-27 06:37:11'),
(991, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 06:37:12', '2018-11-27 06:37:12'),
(992, 1, 'admin/auth/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 06:37:16', '2018-11-27 06:37:16'),
(993, 1, 'admin/auth/users/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 06:37:18', '2018-11-27 06:37:18'),
(994, 1, 'admin/auth/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 06:37:24', '2018-11-27 06:37:24'),
(995, 1, 'admin/auth/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 06:37:29', '2018-11-27 06:37:29'),
(996, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 06:37:31', '2018-11-27 06:37:31'),
(997, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 06:37:32', '2018-11-27 06:37:32'),
(998, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 06:37:33', '2018-11-27 06:37:33'),
(999, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 06:37:34', '2018-11-27 06:37:34'),
(1000, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 06:37:36', '2018-11-27 06:37:36'),
(1001, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '[]', '2018-11-27 06:41:04', '2018-11-27 06:41:04'),
(1002, 1, 'admin', 'GET', '202.4.108.142', '[]', '2018-11-27 06:41:44', '2018-11-27 06:41:44'),
(1003, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 06:42:46', '2018-11-27 06:42:46'),
(1004, 1, 'admin/additional_returns', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 06:42:56', '2018-11-27 06:42:56'),
(1005, 1, 'admin/additional_returns/rono', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 06:43:20', '2018-11-27 06:43:20'),
(1006, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 06:43:22', '2018-11-27 06:43:22'),
(1007, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 06:43:49', '2018-11-27 06:43:49'),
(1008, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 06:43:57', '2018-11-27 06:43:57'),
(1009, 1, 'admin/users', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 06:44:03', '2018-11-27 06:44:03'),
(1010, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 06:46:58', '2018-11-27 06:46:58'),
(1011, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 06:47:01', '2018-11-27 06:47:01'),
(1012, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 06:47:05', '2018-11-27 06:47:05'),
(1013, 1, 'admin/training_contents', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 06:47:06', '2018-11-27 06:47:06'),
(1014, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 06:47:06', '2018-11-27 06:47:06'),
(1015, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 06:47:08', '2018-11-27 06:47:08'),
(1016, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 06:47:08', '2018-11-27 06:47:08'),
(1017, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 06:47:10', '2018-11-27 06:47:10'),
(1018, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 06:47:20', '2018-11-27 06:47:20'),
(1019, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 06:47:21', '2018-11-27 06:47:21'),
(1020, 1, 'admin/training_contents', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 06:47:22', '2018-11-27 06:47:22'),
(1021, 1, 'admin/additional_returns', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 07:22:01', '2018-11-27 07:22:01'),
(1022, 1, 'admin/users', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 07:22:05', '2018-11-27 07:22:05'),
(1023, 1, 'admin/packages', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 07:22:16', '2018-11-27 07:22:16'),
(1024, 1, 'admin/users', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 07:22:30', '2018-11-27 07:22:30'),
(1025, 1, 'admin/users/2/edit', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 07:22:34', '2018-11-27 07:22:34'),
(1026, 1, 'admin/users/2', 'PUT', '202.4.108.142', '{\"first_name\":\"Tauhidul\",\"last_name\":\"Islam\",\"username\":\"rono\",\"phone\":\"01716791679\",\"email\":\"dreamfighterr@gmail.com\",\"password\":\"274675\",\"invest_amount\":{\"package_id\":\"3\",\"referral_code\":\"janedoe\",\"status\":\"paid\"},\"_token\":\"T4mxSZZDL7Z8PZyL3G9WTWaQ0fl3f6aKtcWnN0sM\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/27.147.230.244\\/admin\\/users\"}', '2018-11-27 07:22:57', '2018-11-27 07:22:57'),
(1027, 1, 'admin/users/2/edit', 'GET', '202.4.108.142', '[]', '2018-11-27 07:22:57', '2018-11-27 07:22:57'),
(1028, 1, 'admin', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 07:23:37', '2018-11-27 07:23:37'),
(1029, 1, 'admin/packages', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 07:23:39', '2018-11-27 07:23:39'),
(1030, 1, 'admin/users', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 07:23:41', '2018-11-27 07:23:41'),
(1031, 1, 'admin/users/1', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 07:23:44', '2018-11-27 07:23:44'),
(1032, 1, 'admin/auth/users', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 07:23:50', '2018-11-27 07:23:50'),
(1033, 1, 'admin/auth/roles', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 07:23:58', '2018-11-27 07:23:58'),
(1034, 1, 'admin/auth/roles/1/edit', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 07:24:01', '2018-11-27 07:24:01'),
(1035, 1, 'admin/packages', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 07:24:23', '2018-11-27 07:24:23'),
(1036, 1, 'admin/additional_returns', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 07:24:24', '2018-11-27 07:24:24'),
(1037, 1, 'admin/additional_returns/rono', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 07:24:30', '2018-11-27 07:24:30'),
(1038, 1, 'admin/additional_returns/rono', 'POST', '202.4.108.142', '{\"_token\":\"T4mxSZZDL7Z8PZyL3G9WTWaQ0fl3f6aKtcWnN0sM\",\"month\":\"November - 18\",\"amount\":\"100\"}', '2018-11-27 07:25:13', '2018-11-27 07:25:13'),
(1039, 1, 'admin/additional_returns/rono', 'GET', '202.4.108.142', '[]', '2018-11-27 07:25:13', '2018-11-27 07:25:13'),
(1040, 1, 'admin', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 07:25:23', '2018-11-27 07:25:23'),
(1041, 1, 'admin/users', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 07:25:28', '2018-11-27 07:25:28'),
(1042, 1, 'admin/users/2', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 07:25:38', '2018-11-27 07:25:38'),
(1043, 1, 'admin/packages', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 07:25:44', '2018-11-27 07:25:44'),
(1044, 1, 'admin/additional_returns', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 07:26:15', '2018-11-27 07:26:15'),
(1045, 1, 'admin/additional_returns', 'GET', '202.4.108.142', '[]', '2018-11-27 08:47:28', '2018-11-27 08:47:28'),
(1046, 1, 'admin', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 08:49:03', '2018-11-27 08:49:03'),
(1047, 1, 'admin/auth/users', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 08:49:17', '2018-11-27 08:49:17'),
(1048, 1, 'admin/auth/roles', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 08:49:32', '2018-11-27 08:49:32'),
(1049, 1, 'admin/auth/roles/1/edit', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 08:49:34', '2018-11-27 08:49:34'),
(1050, 1, 'admin/packages', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 08:49:59', '2018-11-27 08:49:59'),
(1051, 1, 'admin/packages/1/edit', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 08:50:17', '2018-11-27 08:50:17'),
(1052, 1, 'admin/packages', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 08:50:52', '2018-11-27 08:50:52'),
(1053, 1, 'admin/packages/1/edit', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 08:50:56', '2018-11-27 08:50:56'),
(1054, 1, 'admin/packages', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 08:51:37', '2018-11-27 08:51:37'),
(1055, 1, 'admin/users', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 08:51:40', '2018-11-27 08:51:40'),
(1056, 1, 'admin/users/create', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 08:52:06', '2018-11-27 08:52:06'),
(1057, 1, 'admin/users', 'POST', '202.4.108.142', '{\"first_name\":\"ariful\",\"last_name\":\"Haque\",\"username\":\"arif\",\"phone\":\"664444_____\",\"email\":\"arif@gmail.com\",\"password\":\"123456\",\"invest_amount\":{\"package_id\":\"1\",\"referral_code\":\"rono\",\"status\":\"paid\"},\"_token\":\"T4mxSZZDL7Z8PZyL3G9WTWaQ0fl3f6aKtcWnN0sM\",\"_previous_\":\"http:\\/\\/27.147.230.244\\/admin\\/users\"}', '2018-11-27 08:53:11', '2018-11-27 08:53:11'),
(1058, 1, 'admin/users', 'GET', '202.4.108.142', '[]', '2018-11-27 08:53:11', '2018-11-27 08:53:11'),
(1059, 1, 'admin/users', 'GET', '202.4.108.142', '[]', '2018-11-27 08:53:15', '2018-11-27 08:53:15'),
(1060, 1, 'admin/users', 'GET', '202.4.108.142', '[]', '2018-11-27 09:02:47', '2018-11-27 09:02:47'),
(1061, 1, 'admin/additional_returns', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 09:03:08', '2018-11-27 09:03:08'),
(1062, 1, 'admin/additional_returns/islam', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 09:03:28', '2018-11-27 09:03:28'),
(1063, 1, 'admin/additional_returns/islam', 'POST', '202.4.108.142', '{\"_token\":\"T4mxSZZDL7Z8PZyL3G9WTWaQ0fl3f6aKtcWnN0sM\",\"month\":\"December - 18\",\"amount\":\"100\"}', '2018-11-27 09:03:36', '2018-11-27 09:03:36'),
(1064, 1, 'admin/additional_returns/islam', 'GET', '202.4.108.142', '[]', '2018-11-27 09:03:36', '2018-11-27 09:03:36'),
(1065, 1, 'admin/additional_returns', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 09:03:43', '2018-11-27 09:03:43'),
(1066, 1, 'admin/additional_returns', 'GET', '202.4.108.142', '[]', '2018-11-27 09:03:46', '2018-11-27 09:03:46'),
(1067, 1, 'admin/additional_returns/islam', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 09:05:00', '2018-11-27 09:05:00'),
(1068, 1, 'admin/additional_returns', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 09:05:24', '2018-11-27 09:05:24'),
(1069, 1, 'admin/users', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 09:05:38', '2018-11-27 09:05:38'),
(1070, 1, 'admin/users/1', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 09:07:35', '2018-11-27 09:07:35'),
(1071, 1, 'admin/users', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 09:07:39', '2018-11-27 09:07:39'),
(1072, 1, 'admin', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 09:08:22', '2018-11-27 09:08:22'),
(1073, 1, 'admin/training_contents', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 09:08:27', '2018-11-27 09:08:27'),
(1074, 1, 'admin/training_contents/1/edit', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 09:08:43', '2018-11-27 09:08:43'),
(1075, 1, 'admin/news', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 09:08:53', '2018-11-27 09:08:53'),
(1076, 1, 'admin/additional_returns', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 09:08:54', '2018-11-27 09:08:54'),
(1077, 1, 'admin/users', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 09:08:58', '2018-11-27 09:08:58'),
(1078, 1, 'admin/packages', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 09:09:00', '2018-11-27 09:09:00'),
(1079, 1, 'admin', 'GET', '202.4.108.142', '{\"_pjax\":\"#pjax-container\"}', '2018-11-27 09:09:23', '2018-11-27 09:09:23'),
(1080, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-12-05 00:26:51', '2018-12-05 00:26:51'),
(1081, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 00:27:05', '2018-12-05 00:27:05'),
(1082, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 00:27:28', '2018-12-05 00:27:28'),
(1083, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 00:27:29', '2018-12-05 00:27:29'),
(1084, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 00:27:36', '2018-12-05 00:27:36'),
(1085, 1, 'admin/auth/menu/9/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 00:27:42', '2018-12-05 00:27:42'),
(1086, 1, 'admin/auth/menu/9', 'PUT', '127.0.0.1', '{\"parent_id\":\"0\",\"title\":\"Members\",\"icon\":\"fa-gbp\",\"uri\":\"users\",\"roles\":[\"1\",null],\"permission\":\"*\",\"_token\":\"HDIjPbwOgoPZkoNzGFjkVkT3fMCnc1N6oFzSb0x1\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/auth\\/menu\"}', '2018-12-05 00:27:49', '2018-12-05 00:27:49'),
(1087, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-12-05 00:27:49', '2018-12-05 00:27:49'),
(1088, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-12-05 00:27:56', '2018-12-05 00:27:56'),
(1089, 1, 'admin/auth/menu/10/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 00:28:05', '2018-12-05 00:28:05'),
(1090, 1, 'admin/auth/menu/10', 'PUT', '127.0.0.1', '{\"parent_id\":\"0\",\"title\":\"Return with bonus\",\"icon\":\"fa-percent\",\"uri\":\"additional_returns\",\"roles\":[\"1\",null],\"permission\":\"*\",\"_token\":\"HDIjPbwOgoPZkoNzGFjkVkT3fMCnc1N6oFzSb0x1\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/auth\\/menu\"}', '2018-12-05 00:28:24', '2018-12-05 00:28:24'),
(1091, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-12-05 00:28:25', '2018-12-05 00:28:25'),
(1092, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-12-05 00:28:27', '2018-12-05 00:28:27'),
(1093, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 00:28:36', '2018-12-05 00:28:36'),
(1094, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 00:29:24', '2018-12-05 00:29:24'),
(1095, 1, 'admin/additional_returns/janedoe', 'GET', '127.0.0.1', '[]', '2018-12-05 00:32:46', '2018-12-05 00:32:46'),
(1096, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 00:33:12', '2018-12-05 00:33:12'),
(1097, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 00:33:14', '2018-12-05 00:33:14'),
(1098, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 00:33:16', '2018-12-05 00:33:16'),
(1099, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 00:33:17', '2018-12-05 00:33:17'),
(1100, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 00:33:18', '2018-12-05 00:33:18'),
(1101, 1, 'admin/training_contents', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 00:33:19', '2018-12-05 00:33:19'),
(1102, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 00:38:58', '2018-12-05 00:38:58'),
(1103, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-12-05 00:39:07', '2018-12-05 00:39:07'),
(1104, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 00:39:20', '2018-12-05 00:39:20'),
(1105, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-12-05 00:40:18', '2018-12-05 00:40:18'),
(1106, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 00:40:32', '2018-12-05 00:40:32'),
(1107, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-12-05 00:51:11', '2018-12-05 00:51:11'),
(1108, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-12-05 00:51:39', '2018-12-05 00:51:39'),
(1109, 1, 'admin/users/1', 'PUT', '127.0.0.1', '{\"name\":\"profile_verified\",\"value\":\"dsfa\",\"pk\":\"1\",\"_token\":\"HDIjPbwOgoPZkoNzGFjkVkT3fMCnc1N6oFzSb0x1\",\"_editable\":\"1\",\"_method\":\"PUT\"}', '2018-12-05 00:51:56', '2018-12-05 00:51:56'),
(1110, 1, 'admin/users/1', 'PUT', '127.0.0.1', '{\"name\":\"profile_verified\",\"value\":null,\"pk\":\"1\",\"_token\":\"HDIjPbwOgoPZkoNzGFjkVkT3fMCnc1N6oFzSb0x1\",\"_editable\":\"1\",\"_method\":\"PUT\"}', '2018-12-05 00:52:05', '2018-12-05 00:52:05'),
(1111, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-12-05 00:56:38', '2018-12-05 00:56:38'),
(1112, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 00:56:42', '2018-12-05 00:56:42'),
(1113, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-12-05 00:56:47', '2018-12-05 00:56:47'),
(1114, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-12-05 01:01:49', '2018-12-05 01:01:49'),
(1115, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-12-05 01:02:30', '2018-12-05 01:02:30'),
(1116, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-12-05 01:10:46', '2018-12-05 01:10:46'),
(1117, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 01:10:50', '2018-12-05 01:10:50'),
(1118, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 01:10:51', '2018-12-05 01:10:51'),
(1119, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 01:11:09', '2018-12-05 01:11:09'),
(1120, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 01:11:10', '2018-12-05 01:11:10'),
(1121, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 01:11:11', '2018-12-05 01:11:11'),
(1122, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 01:11:12', '2018-12-05 01:11:12'),
(1123, 1, 'admin/training_contents', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 01:11:13', '2018-12-05 01:11:13'),
(1124, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 01:11:15', '2018-12-05 01:11:15'),
(1125, 1, 'admin/training_contents', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 01:11:16', '2018-12-05 01:11:16'),
(1126, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 01:11:18', '2018-12-05 01:11:18'),
(1127, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-12-05 01:15:55', '2018-12-05 01:15:55'),
(1128, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 01:16:00', '2018-12-05 01:16:00'),
(1129, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-12-05 01:18:01', '2018-12-05 01:18:01');
INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(1130, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-12-05 01:18:26', '2018-12-05 01:18:26'),
(1131, 1, 'admin/users/1', 'PUT', '127.0.0.1', '{\"name\":\"invest_amount.status\",\"value\":\"pending\",\"pk\":\"1\",\"_token\":\"HDIjPbwOgoPZkoNzGFjkVkT3fMCnc1N6oFzSb0x1\",\"_editable\":\"1\",\"_method\":\"PUT\"}', '2018-12-05 01:18:36', '2018-12-05 01:18:36'),
(1132, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-12-05 01:18:41', '2018-12-05 01:18:41'),
(1133, 1, 'admin/users/1', 'PUT', '127.0.0.1', '{\"name\":\"invest_amount.status\",\"value\":\"paid\",\"pk\":\"1\",\"_token\":\"HDIjPbwOgoPZkoNzGFjkVkT3fMCnc1N6oFzSb0x1\",\"_editable\":\"1\",\"_method\":\"PUT\"}', '2018-12-05 01:18:46', '2018-12-05 01:18:46'),
(1134, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 01:47:17', '2018-12-05 01:47:17'),
(1135, 1, 'admin/auth/menu', 'POST', '127.0.0.1', '{\"parent_id\":\"0\",\"title\":\"Transactions\",\"icon\":\"fa-angle-double-right\",\"uri\":\"transactions\",\"roles\":[\"1\",null],\"permission\":\"*\",\"_token\":\"HDIjPbwOgoPZkoNzGFjkVkT3fMCnc1N6oFzSb0x1\"}', '2018-12-05 01:47:57', '2018-12-05 01:47:57'),
(1136, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-12-05 01:47:58', '2018-12-05 01:47:58'),
(1137, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-12-05 01:48:00', '2018-12-05 01:48:00'),
(1138, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 01:48:04', '2018-12-05 01:48:04'),
(1139, 1, 'admin/transactions/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 01:48:07', '2018-12-05 01:48:07'),
(1140, 1, 'admin/transactions/create', 'GET', '127.0.0.1', '[]', '2018-12-05 01:49:17', '2018-12-05 01:49:17'),
(1141, 1, 'admin/transactions/create', 'GET', '127.0.0.1', '[]', '2018-12-05 01:49:46', '2018-12-05 01:49:46'),
(1142, 1, 'admin/transactions/create', 'GET', '127.0.0.1', '[]', '2018-12-05 01:49:48', '2018-12-05 01:49:48'),
(1143, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 01:49:52', '2018-12-05 01:49:52'),
(1144, 1, 'admin/transactions/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 01:49:53', '2018-12-05 01:49:53'),
(1145, 1, 'admin/transactions/create', 'GET', '127.0.0.1', '[]', '2018-12-05 01:51:45', '2018-12-05 01:51:45'),
(1146, 1, 'admin/transactions/create', 'GET', '127.0.0.1', '[]', '2018-12-05 01:51:53', '2018-12-05 01:51:53'),
(1147, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 01:51:57', '2018-12-05 01:51:57'),
(1148, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 01:52:00', '2018-12-05 01:52:00'),
(1149, 1, 'admin/transactions/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 01:52:01', '2018-12-05 01:52:01'),
(1150, 1, 'admin/transactions/create', 'GET', '127.0.0.1', '[]', '2018-12-05 01:52:37', '2018-12-05 01:52:37'),
(1151, 1, 'admin/transactions/create', 'GET', '127.0.0.1', '[]', '2018-12-05 01:52:52', '2018-12-05 01:52:52'),
(1152, 1, 'admin/transactions/create', 'GET', '127.0.0.1', '[]', '2018-12-05 01:53:36', '2018-12-05 01:53:36'),
(1153, 1, 'admin/transactions/create', 'GET', '127.0.0.1', '[]', '2018-12-05 01:54:03', '2018-12-05 01:54:03'),
(1154, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 01:54:06', '2018-12-05 01:54:06'),
(1155, 1, 'admin/transactions', 'GET', '127.0.0.1', '[]', '2018-12-05 01:54:09', '2018-12-05 01:54:09'),
(1156, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 01:54:12', '2018-12-05 01:54:12'),
(1157, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 01:54:42', '2018-12-05 01:54:42'),
(1158, 1, 'admin/training_contents', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 01:54:43', '2018-12-05 01:54:43'),
(1159, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 01:54:44', '2018-12-05 01:54:44'),
(1160, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 01:54:45', '2018-12-05 01:54:45'),
(1161, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 01:54:45', '2018-12-05 01:54:45'),
(1162, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 01:54:47', '2018-12-05 01:54:47'),
(1163, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 01:54:48', '2018-12-05 01:54:48'),
(1164, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 02:15:52', '2018-12-05 02:15:52'),
(1165, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 02:15:54', '2018-12-05 02:15:54'),
(1166, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 02:15:57', '2018-12-05 02:15:57'),
(1167, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 02:27:35', '2018-12-05 02:27:35'),
(1168, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 02:27:37', '2018-12-05 02:27:37'),
(1169, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 02:27:38', '2018-12-05 02:27:38'),
(1170, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 02:27:40', '2018-12-05 02:27:40'),
(1171, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 02:27:41', '2018-12-05 02:27:41'),
(1172, 1, 'admin/users/1,2,3,4', 'DELETE', '127.0.0.1', '{\"_method\":\"delete\",\"_token\":\"HDIjPbwOgoPZkoNzGFjkVkT3fMCnc1N6oFzSb0x1\"}', '2018-12-05 02:27:51', '2018-12-05 02:27:51'),
(1173, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 02:27:51', '2018-12-05 02:27:51'),
(1174, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 02:27:54', '2018-12-05 02:27:54'),
(1175, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 02:27:55', '2018-12-05 02:27:55'),
(1176, 1, 'admin/users/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 02:27:59', '2018-12-05 02:27:59'),
(1177, 1, 'admin/users/create', 'GET', '127.0.0.1', '[]', '2018-12-05 02:30:54', '2018-12-05 02:30:54'),
(1178, 1, 'admin/users/create', 'GET', '127.0.0.1', '[]', '2018-12-05 02:30:58', '2018-12-05 02:30:58'),
(1179, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 02:31:02', '2018-12-05 02:31:02'),
(1180, 1, 'admin/users/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 02:31:03', '2018-12-05 02:31:03'),
(1181, 1, 'admin/users', 'POST', '127.0.0.1', '{\"first_name\":\"Anwar\",\"last_name\":\"Hussen\",\"username\":\"anwar0101\",\"phone\":\"01722565465\",\"email\":\"anwar.hussen4@gmail.com\",\"password\":\"123456\",\"invest_amount\":{\"package_id\":\"1\",\"referral_code\":null,\"status\":\"paid\"},\"_token\":\"HDIjPbwOgoPZkoNzGFjkVkT3fMCnc1N6oFzSb0x1\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/users\"}', '2018-12-05 02:31:42', '2018-12-05 02:31:42'),
(1182, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-12-05 02:31:42', '2018-12-05 02:31:42'),
(1183, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 02:31:57', '2018-12-05 02:31:57'),
(1184, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 02:32:03', '2018-12-05 02:32:03'),
(1185, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 02:32:21', '2018-12-05 02:32:21'),
(1186, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 02:32:22', '2018-12-05 02:32:22'),
(1187, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 02:32:25', '2018-12-05 02:32:25'),
(1188, 1, 'admin/training_contents', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 02:32:26', '2018-12-05 02:32:26'),
(1189, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 02:32:26', '2018-12-05 02:32:26'),
(1190, 1, 'admin/transactions', 'GET', '127.0.0.1', '[]', '2018-12-05 02:32:32', '2018-12-05 02:32:32'),
(1191, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 02:32:50', '2018-12-05 02:32:50'),
(1192, 1, 'admin/users/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 02:32:52', '2018-12-05 02:32:52'),
(1193, 1, 'admin/users', 'POST', '127.0.0.1', '{\"first_name\":\"Anwar\",\"last_name\":\"Junior\",\"username\":\"junior1010\",\"phone\":\"01722564224\",\"email\":\"test@test.com\",\"password\":\"123456\",\"invest_amount\":{\"package_id\":\"1\",\"referral_code\":\"anwar0101\",\"status\":\"paid\"},\"_token\":\"HDIjPbwOgoPZkoNzGFjkVkT3fMCnc1N6oFzSb0x1\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/users\"}', '2018-12-05 02:34:06', '2018-12-05 02:34:06'),
(1194, 1, 'admin/users/create', 'GET', '127.0.0.1', '[]', '2018-12-05 02:34:07', '2018-12-05 02:34:07'),
(1195, 1, 'admin/transactions', 'GET', '127.0.0.1', '[]', '2018-12-05 02:34:28', '2018-12-05 02:34:28'),
(1196, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-12-05 02:35:26', '2018-12-05 02:35:26'),
(1197, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-12-05 02:35:46', '2018-12-05 02:35:46'),
(1198, 1, 'admin/users', 'POST', '127.0.0.1', '{\"first_name\":\"Anwar\",\"last_name\":\"Junior\",\"username\":\"junior1010\",\"phone\":\"01722564224\",\"email\":\"test@test.com\",\"password\":\"123456\",\"invest_amount\":{\"package_id\":\"1\",\"referral_code\":\"anwar0101\",\"status\":\"paid\"},\"_token\":\"HDIjPbwOgoPZkoNzGFjkVkT3fMCnc1N6oFzSb0x1\"}', '2018-12-05 02:35:55', '2018-12-05 02:35:55'),
(1199, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-12-05 02:35:55', '2018-12-05 02:35:55'),
(1200, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 02:36:07', '2018-12-05 02:36:07'),
(1201, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 02:36:19', '2018-12-05 02:36:19'),
(1202, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 02:36:23', '2018-12-05 02:36:23'),
(1203, 1, 'admin/users/7', 'DELETE', '127.0.0.1', '{\"_method\":\"delete\",\"_token\":\"HDIjPbwOgoPZkoNzGFjkVkT3fMCnc1N6oFzSb0x1\"}', '2018-12-05 02:36:29', '2018-12-05 02:36:29'),
(1204, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 02:36:29', '2018-12-05 02:36:29'),
(1205, 1, 'admin/users/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 02:37:39', '2018-12-05 02:37:39'),
(1206, 1, 'admin/users', 'POST', '127.0.0.1', '{\"first_name\":\"Anwar\",\"last_name\":\"dfsa\",\"username\":\"janedoe\",\"phone\":\"01722635124\",\"email\":\"sdaf@daf.com\",\"password\":\"123456\",\"invest_amount\":{\"package_id\":\"1\",\"referral_code\":\"anwar0101\",\"status\":\"paid\"},\"_token\":\"HDIjPbwOgoPZkoNzGFjkVkT3fMCnc1N6oFzSb0x1\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/users\"}', '2018-12-05 02:38:14', '2018-12-05 02:38:14'),
(1207, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-12-05 02:38:14', '2018-12-05 02:38:14'),
(1208, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 02:38:17', '2018-12-05 02:38:17'),
(1209, 1, 'admin/transactions/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 02:38:19', '2018-12-05 02:38:19'),
(1210, 1, 'admin/transactions', 'POST', '127.0.0.1', '{\"type\":\"in\",\"amount_type\":\"token\",\"amount\":\"100\",\"status\":\"pending\",\"from_id\":\"8\",\"to_id\":\"5\",\"_token\":\"HDIjPbwOgoPZkoNzGFjkVkT3fMCnc1N6oFzSb0x1\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/transactions\"}', '2018-12-05 02:38:50', '2018-12-05 02:38:50'),
(1211, 1, 'admin/transactions/create', 'GET', '127.0.0.1', '[]', '2018-12-05 02:38:50', '2018-12-05 02:38:50'),
(1212, 1, 'admin/transactions', 'POST', '127.0.0.1', '{\"type\":\"in\",\"amount_type\":\"token\",\"amount\":\"100\",\"status\":\"pending\",\"from_id\":\"8\",\"to_id\":\"5\",\"_token\":\"HDIjPbwOgoPZkoNzGFjkVkT3fMCnc1N6oFzSb0x1\"}', '2018-12-05 02:39:39', '2018-12-05 02:39:39'),
(1213, 1, 'admin/transactions/create', 'GET', '127.0.0.1', '[]', '2018-12-05 02:39:39', '2018-12-05 02:39:39'),
(1214, 1, 'admin/transactions/create', 'GET', '127.0.0.1', '[]', '2018-12-05 02:39:42', '2018-12-05 02:39:42'),
(1215, 1, 'admin/transactions', 'POST', '127.0.0.1', '{\"type\":\"withdraw\",\"amount_type\":\"token\",\"amount\":\"100\",\"status\":\"pending\",\"from_id\":\"8\",\"to_id\":\"5\",\"note\":\"Testing\",\"_token\":\"HDIjPbwOgoPZkoNzGFjkVkT3fMCnc1N6oFzSb0x1\"}', '2018-12-05 02:40:02', '2018-12-05 02:40:02'),
(1216, 1, 'admin/transactions', 'GET', '127.0.0.1', '[]', '2018-12-05 02:40:02', '2018-12-05 02:40:02'),
(1217, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 02:40:13', '2018-12-05 02:40:13'),
(1218, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 02:40:15', '2018-12-05 02:40:15'),
(1219, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 02:41:25', '2018-12-05 02:41:25'),
(1220, 1, 'admin/users/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 02:41:26', '2018-12-05 02:41:26'),
(1221, 1, 'admin/users', 'POST', '127.0.0.1', '{\"first_name\":\"dsaf\",\"last_name\":\"dfas\",\"username\":\"fdsa\",\"phone\":\"05465465416\",\"email\":\"dsaf@afds.com\",\"password\":\"123456\",\"invest_amount\":{\"package_id\":\"1\",\"referral_code\":\"anwar0101\",\"status\":\"paid\"},\"_token\":\"HDIjPbwOgoPZkoNzGFjkVkT3fMCnc1N6oFzSb0x1\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/users\"}', '2018-12-05 02:41:56', '2018-12-05 02:41:56'),
(1222, 1, 'admin/users/create', 'GET', '127.0.0.1', '[]', '2018-12-05 02:41:57', '2018-12-05 02:41:57'),
(1223, 1, 'admin/users', 'POST', '127.0.0.1', '{\"first_name\":\"dsaf\",\"last_name\":\"dfas\",\"username\":\"fdsa\",\"phone\":\"05465465416\",\"email\":\"dsaf@afds.com\",\"password\":\"123456\",\"invest_amount\":{\"package_id\":\"1\",\"referral_code\":\"anwar0101\",\"status\":\"paid\"},\"_token\":\"HDIjPbwOgoPZkoNzGFjkVkT3fMCnc1N6oFzSb0x1\"}', '2018-12-05 02:45:31', '2018-12-05 02:45:31'),
(1224, 1, 'admin/users/create', 'GET', '127.0.0.1', '[]', '2018-12-05 02:45:31', '2018-12-05 02:45:31'),
(1225, 1, 'admin/users', 'POST', '127.0.0.1', '{\"first_name\":\"dsaf\",\"last_name\":\"dfas\",\"username\":\"fdsa\",\"phone\":\"05465465416\",\"email\":\"dsaf@afds.com\",\"password\":\"123456\",\"invest_amount\":{\"package_id\":\"1\",\"referral_code\":\"anwar0101\",\"status\":\"paid\"},\"_token\":\"HDIjPbwOgoPZkoNzGFjkVkT3fMCnc1N6oFzSb0x1\"}', '2018-12-05 02:45:47', '2018-12-05 02:45:47'),
(1226, 1, 'admin/users/create', 'GET', '127.0.0.1', '[]', '2018-12-05 02:45:48', '2018-12-05 02:45:48'),
(1227, 1, 'admin/users/create', 'GET', '127.0.0.1', '[]', '2018-12-05 02:46:35', '2018-12-05 02:46:35'),
(1228, 1, 'admin/users', 'POST', '127.0.0.1', '{\"first_name\":\"dsfa\",\"last_name\":\"dsfa\",\"username\":\"afds\",\"phone\":\"05465465465\",\"email\":\"afds@adasf.com\",\"password\":\"123456\",\"invest_amount\":{\"package_id\":\"1\",\"referral_code\":null,\"status\":\"paid\"},\"_token\":\"HDIjPbwOgoPZkoNzGFjkVkT3fMCnc1N6oFzSb0x1\"}', '2018-12-05 02:47:09', '2018-12-05 02:47:09'),
(1229, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-12-05 02:47:09', '2018-12-05 02:47:09'),
(1230, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 02:47:12', '2018-12-05 02:47:12'),
(1231, 1, 'admin/transactions/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 02:48:09', '2018-12-05 02:48:09'),
(1232, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 02:48:17', '2018-12-05 02:48:17'),
(1233, 1, 'admin/users/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 02:48:19', '2018-12-05 02:48:19'),
(1234, 1, 'admin/users', 'POST', '127.0.0.1', '{\"first_name\":\"dsf\",\"last_name\":\"sdfa\",\"username\":\"dfsasdfa\",\"phone\":\"05450465465\",\"email\":\"sdaffd@gmail.com\",\"password\":\"123456\",\"invest_amount\":{\"package_id\":\"1\",\"referral_code\":\"anwar0101\",\"status\":\"paid\"},\"_token\":\"HDIjPbwOgoPZkoNzGFjkVkT3fMCnc1N6oFzSb0x1\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/users\"}', '2018-12-05 02:48:44', '2018-12-05 02:48:44'),
(1235, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-12-05 02:49:11', '2018-12-05 02:49:11'),
(1236, 1, 'admin/users/12', 'DELETE', '127.0.0.1', '{\"_method\":\"delete\",\"_token\":\"HDIjPbwOgoPZkoNzGFjkVkT3fMCnc1N6oFzSb0x1\"}', '2018-12-05 02:49:19', '2018-12-05 02:49:19'),
(1237, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 02:49:19', '2018-12-05 02:49:19'),
(1238, 1, 'admin/users/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 02:49:22', '2018-12-05 02:49:22'),
(1239, 1, 'admin/users', 'POST', '127.0.0.1', '{\"first_name\":\"dsaf\",\"last_name\":\"dsfa\",\"username\":\"dsfaaaaaa\",\"phone\":\"01544564165\",\"email\":\"sdaffda@adfljf.com\",\"password\":\"123456\",\"invest_amount\":{\"package_id\":\"1\",\"referral_code\":\"anwar0101\",\"status\":\"paid\"},\"_token\":\"HDIjPbwOgoPZkoNzGFjkVkT3fMCnc1N6oFzSb0x1\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/users\"}', '2018-12-05 02:49:47', '2018-12-05 02:49:47'),
(1240, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 02:50:46', '2018-12-05 02:50:46'),
(1241, 1, 'admin/users/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-05 02:50:49', '2018-12-05 02:50:49'),
(1242, 1, 'admin/users', 'POST', '127.0.0.1', '{\"first_name\":\"sdaf\",\"last_name\":\"dsaf\",\"username\":\"dsfaaaaaa\",\"phone\":\"06540654654\",\"email\":\"dsaffffff@gmail.com\",\"password\":\"123456\",\"invest_amount\":{\"package_id\":\"1\",\"referral_code\":\"anwar0101\",\"status\":\"paid\"},\"_token\":\"HDIjPbwOgoPZkoNzGFjkVkT3fMCnc1N6oFzSb0x1\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/users\"}', '2018-12-05 02:51:23', '2018-12-05 02:51:23'),
(1243, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-12-06 00:33:06', '2018-12-06 00:33:06'),
(1244, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-06 00:33:11', '2018-12-06 00:33:11'),
(1245, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-12-06 00:33:42', '2018-12-06 00:33:42'),
(1246, 1, 'admin/users/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-06 00:33:45', '2018-12-06 00:33:45'),
(1247, 1, 'admin/users', 'POST', '127.0.0.1', '{\"first_name\":\"Anwar\",\"last_name\":\"Hussen\",\"username\":\"anwar\",\"phone\":\"01722635124\",\"email\":\"anwar.dev9@gmail.com\",\"password\":\"123456\",\"invest_amount\":{\"package_id\":\"1\",\"referral_code\":null,\"status\":\"paid\"},\"_token\":\"pVpzaaxFPOoYAGS5Z6iVF9bnFVCqaMxR15yAupsz\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/users\"}', '2018-12-06 00:34:37', '2018-12-06 00:34:37'),
(1248, 1, 'admin/users/create', 'GET', '127.0.0.1', '[]', '2018-12-06 00:34:37', '2018-12-06 00:34:37'),
(1249, 1, 'admin/users', 'POST', '127.0.0.1', '{\"first_name\":\"Anwar\",\"last_name\":\"Hussen\",\"username\":\"anwar\",\"phone\":\"01722635124\",\"email\":\"anwar.dev9@gmail.com\",\"password\":\"123456\",\"invest_amount\":{\"package_id\":\"1\",\"referral_code\":null,\"status\":\"paid\"},\"_token\":\"pVpzaaxFPOoYAGS5Z6iVF9bnFVCqaMxR15yAupsz\"}', '2018-12-06 00:34:59', '2018-12-06 00:34:59'),
(1250, 1, 'admin/users/create', 'GET', '127.0.0.1', '[]', '2018-12-06 00:34:59', '2018-12-06 00:34:59'),
(1251, 1, 'admin/users', 'POST', '127.0.0.1', '{\"first_name\":\"Anwar\",\"last_name\":\"Hussen\",\"username\":\"anwar\",\"phone\":\"01722635124\",\"email\":\"anwar.dev9@gmail.com\",\"password\":\"123456\",\"invest_amount\":{\"package_id\":\"1\",\"referral_code\":null,\"status\":\"paid\"},\"_token\":\"pVpzaaxFPOoYAGS5Z6iVF9bnFVCqaMxR15yAupsz\"}', '2018-12-06 00:35:30', '2018-12-06 00:35:30'),
(1252, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-12-06 00:35:30', '2018-12-06 00:35:30'),
(1253, 1, 'admin/users/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-06 00:35:46', '2018-12-06 00:35:46'),
(1254, 1, 'admin/users', 'POST', '127.0.0.1', '{\"first_name\":\"Kamrul Islam\",\"last_name\":\"Rono\",\"username\":\"rono\",\"phone\":\"01722635444\",\"email\":\"kamrul@gmail.com\",\"password\":\"123456\",\"invest_amount\":{\"package_id\":\"2\",\"referral_code\":\"anwar\",\"status\":\"paid\"},\"_token\":\"pVpzaaxFPOoYAGS5Z6iVF9bnFVCqaMxR15yAupsz\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/users\"}', '2018-12-06 00:36:58', '2018-12-06 00:36:58'),
(1255, 1, 'admin/users/create', 'GET', '127.0.0.1', '[]', '2018-12-06 00:36:58', '2018-12-06 00:36:58'),
(1256, 1, 'admin/users', 'POST', '127.0.0.1', '{\"first_name\":\"Kamrul Islam\",\"last_name\":\"Rono\",\"username\":\"rono\",\"phone\":\"01722635444\",\"email\":\"kamrul@gmail.com\",\"password\":\"123456\",\"invest_amount\":{\"package_id\":\"2\",\"referral_code\":\"anwar\",\"status\":\"paid\"},\"_token\":\"pVpzaaxFPOoYAGS5Z6iVF9bnFVCqaMxR15yAupsz\"}', '2018-12-06 00:37:31', '2018-12-06 00:37:31'),
(1257, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-12-06 00:37:32', '2018-12-06 00:37:32'),
(1258, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-06 00:37:42', '2018-12-06 00:37:42'),
(1259, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-06 00:37:52', '2018-12-06 00:37:52'),
(1260, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-06 00:37:54', '2018-12-06 00:37:54'),
(1261, 1, 'admin/users/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-06 00:38:08', '2018-12-06 00:38:08'),
(1262, 1, 'admin/users', 'POST', '127.0.0.1', '{\"first_name\":\"Layek\",\"last_name\":\"Islam\",\"username\":\"layek\",\"phone\":\"01455465465\",\"email\":\"layek@gmail.com\",\"password\":\"123456\",\"invest_amount\":{\"package_id\":\"3\",\"referral_code\":\"anwar\",\"status\":\"paid\"},\"_token\":\"pVpzaaxFPOoYAGS5Z6iVF9bnFVCqaMxR15yAupsz\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/users\"}', '2018-12-06 00:39:19', '2018-12-06 00:39:19'),
(1263, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-12-06 00:39:20', '2018-12-06 00:39:20'),
(1264, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-12-06 00:41:32', '2018-12-06 00:41:32'),
(1265, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-12-06 00:45:52', '2018-12-06 00:45:52'),
(1266, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-06 00:45:56', '2018-12-06 00:45:56'),
(1267, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-06 00:45:57', '2018-12-06 00:45:57'),
(1268, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-06 00:45:59', '2018-12-06 00:45:59'),
(1269, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-06 00:46:07', '2018-12-06 00:46:07'),
(1270, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-12-06 00:46:41', '2018-12-06 00:46:41'),
(1271, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-12-06 00:46:46', '2018-12-06 00:46:46'),
(1272, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-12-06 00:46:55', '2018-12-06 00:46:55'),
(1273, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-12-06 00:47:14', '2018-12-06 00:47:14'),
(1274, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-06 00:47:19', '2018-12-06 00:47:19'),
(1275, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-06 00:47:20', '2018-12-06 00:47:20'),
(1276, 1, 'admin/users/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-06 00:47:21', '2018-12-06 00:47:21'),
(1277, 1, 'admin/users', 'POST', '127.0.0.1', '{\"first_name\":\"Anwar\",\"last_name\":\"Hussen\",\"username\":\"anwar\",\"phone\":\"01722635124\",\"email\":\"anwar.dev9@gmail.com\",\"password\":\"123456\",\"invest_amount\":{\"package_id\":\"1\",\"referral_code\":null,\"status\":\"paid\"},\"_token\":\"BqKhUBtfdLVND8owbUaVwpumX7ZUkvKugG5AI8RH\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/users\"}', '2018-12-06 00:48:10', '2018-12-06 00:48:10'),
(1278, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-12-06 00:48:11', '2018-12-06 00:48:11'),
(1279, 1, 'admin/users/create', 'GET', '127.0.0.1', '[]', '2018-12-06 00:49:37', '2018-12-06 00:49:37'),
(1280, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-12-06 01:17:34', '2018-12-06 01:17:34'),
(1281, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-06 01:17:37', '2018-12-06 01:17:37'),
(1282, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-12-08 22:55:19', '2018-12-08 22:55:19'),
(1283, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-12-08 22:59:36', '2018-12-08 22:59:36'),
(1284, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-12-08 23:00:04', '2018-12-08 23:00:04'),
(1285, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-08 23:08:21', '2018-12-08 23:08:21'),
(1286, 1, 'admin/users/16/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-08 23:08:28', '2018-12-08 23:08:28'),
(1287, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-08 23:08:52', '2018-12-08 23:08:52'),
(1288, 1, 'admin/users/16/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-08 23:08:58', '2018-12-08 23:08:58'),
(1289, 1, 'admin/users/16/edit', 'GET', '127.0.0.1', '[]', '2018-12-08 23:09:23', '2018-12-08 23:09:23'),
(1290, 1, 'admin/users/16', 'PUT', '127.0.0.1', '{\"first_name\":\"dsaf\",\"last_name\":\"dsfa\",\"username\":\"sdfaadsfa\",\"phone\":null,\"email\":\"dsaffd@gmail.copm\",\"password\":null,\"invest_amount\":{\"package_id\":\"1\",\"referral_code\":null,\"status\":\"pending\"},\"_token\":\"I97lopxqJEc2lonSZQ6LJCJyvOu9uKQir1NwDM4V\",\"_method\":\"PUT\"}', '2018-12-08 23:09:51', '2018-12-08 23:09:51'),
(1291, 1, 'admin/users/16/edit', 'GET', '127.0.0.1', '[]', '2018-12-08 23:09:51', '2018-12-08 23:09:51'),
(1292, 1, 'admin/users/16', 'PUT', '127.0.0.1', '{\"first_name\":\"dsaf\",\"last_name\":\"dsfa\",\"username\":\"sdfaadsfa\",\"phone\":null,\"email\":\"dsaffd@gmail.copm\",\"password\":null,\"invest_amount\":{\"package_id\":\"1\",\"referral_code\":null,\"status\":\"pending\"},\"_token\":\"I97lopxqJEc2lonSZQ6LJCJyvOu9uKQir1NwDM4V\",\"_method\":\"PUT\"}', '2018-12-08 23:10:04', '2018-12-08 23:10:04'),
(1293, 1, 'admin/users/16/edit', 'GET', '127.0.0.1', '[]', '2018-12-08 23:10:04', '2018-12-08 23:10:04'),
(1294, 1, 'admin/users/16/edit', 'GET', '127.0.0.1', '[]', '2018-12-08 23:11:03', '2018-12-08 23:11:03'),
(1295, 1, 'admin/users/16', 'PUT', '127.0.0.1', '{\"first_name\":\"dsaf\",\"last_name\":\"dsfa\",\"username\":\"sdfaadsfa\",\"phone\":null,\"email\":\"dsaffd@gmail.copm\",\"password\":null,\"invest_amount\":{\"package_id\":\"1\",\"referral_code\":null,\"status\":\"pending\"},\"_token\":\"I97lopxqJEc2lonSZQ6LJCJyvOu9uKQir1NwDM4V\",\"_method\":\"PUT\"}', '2018-12-08 23:11:24', '2018-12-08 23:11:24'),
(1296, 1, 'admin/users/16/edit', 'GET', '127.0.0.1', '[]', '2018-12-08 23:11:24', '2018-12-08 23:11:24'),
(1297, 1, 'admin/users/16/edit', 'GET', '127.0.0.1', '[]', '2018-12-08 23:12:01', '2018-12-08 23:12:01'),
(1298, 1, 'admin/users/16', 'PUT', '127.0.0.1', '{\"first_name\":\"dsaf\",\"last_name\":\"dsfa\",\"username\":\"sdfaadsfa\",\"phone\":null,\"email\":\"dsaffd@gmail.copm\",\"password\":null,\"invest_amount\":{\"package_id\":\"1\",\"referral_code\":null,\"status\":\"pending\"},\"_token\":\"I97lopxqJEc2lonSZQ6LJCJyvOu9uKQir1NwDM4V\",\"_method\":\"PUT\"}', '2018-12-08 23:12:04', '2018-12-08 23:12:04'),
(1299, 1, 'admin/users/16/edit', 'GET', '127.0.0.1', '[]', '2018-12-08 23:12:04', '2018-12-08 23:12:04'),
(1300, 1, 'admin/users/16', 'PUT', '127.0.0.1', '{\"first_name\":\"dsaf\",\"last_name\":\"dsfa\",\"username\":\"sdfaadsfa\",\"phone\":null,\"email\":\"dsaffd@gmail.copm\",\"password\":null,\"invest_amount\":{\"package_id\":\"1\",\"referral_code\":null,\"status\":\"pending\"},\"_token\":\"I97lopxqJEc2lonSZQ6LJCJyvOu9uKQir1NwDM4V\",\"_method\":\"PUT\"}', '2018-12-08 23:12:07', '2018-12-08 23:12:07'),
(1301, 1, 'admin/users/16/edit', 'GET', '127.0.0.1', '[]', '2018-12-08 23:12:07', '2018-12-08 23:12:07'),
(1302, 1, 'admin/users/16/edit', 'GET', '127.0.0.1', '[]', '2018-12-08 23:12:20', '2018-12-08 23:12:20'),
(1303, 1, 'admin/users/16', 'PUT', '127.0.0.1', '{\"first_name\":\"dsaf\",\"last_name\":\"dsfa\",\"username\":\"sdfaadsfa\",\"phone\":null,\"email\":\"dsaffd@gmail.copm\",\"password\":null,\"invest_amount\":{\"package_id\":\"1\",\"referral_code\":null,\"status\":\"pending\"},\"_token\":\"I97lopxqJEc2lonSZQ6LJCJyvOu9uKQir1NwDM4V\",\"_method\":\"PUT\"}', '2018-12-08 23:12:23', '2018-12-08 23:12:23'),
(1304, 1, 'admin/users/16/edit', 'GET', '127.0.0.1', '[]', '2018-12-08 23:12:23', '2018-12-08 23:12:23'),
(1305, 1, 'admin/users/16/edit', 'GET', '127.0.0.1', '[]', '2018-12-08 23:13:51', '2018-12-08 23:13:51'),
(1306, 1, 'admin/users/16', 'PUT', '127.0.0.1', '{\"first_name\":\"dsaf\",\"last_name\":\"dsfa\",\"username\":\"sdfaadsfa\",\"phone\":null,\"email\":\"dsaffd@gmail.copm\",\"password\":null,\"invest_amount\":{\"package_id\":\"1\",\"referral_code\":null,\"status\":\"pending\"},\"_token\":\"I97lopxqJEc2lonSZQ6LJCJyvOu9uKQir1NwDM4V\",\"_method\":\"PUT\"}', '2018-12-08 23:13:55', '2018-12-08 23:13:55'),
(1307, 1, 'admin/users/16/edit', 'GET', '127.0.0.1', '[]', '2018-12-08 23:13:56', '2018-12-08 23:13:56'),
(1308, 1, 'admin/users/16/edit', 'GET', '127.0.0.1', '[]', '2018-12-08 23:14:32', '2018-12-08 23:14:32'),
(1309, 1, 'admin/users/16', 'PUT', '127.0.0.1', '{\"first_name\":\"dsaf\",\"last_name\":\"dsfa\",\"username\":\"sdfaadsfa\",\"phone\":null,\"email\":\"dsaffd@gmail.copm\",\"password\":null,\"invest_amount\":{\"package_id\":\"1\",\"referral_code\":null,\"status\":\"pending\"},\"_token\":\"I97lopxqJEc2lonSZQ6LJCJyvOu9uKQir1NwDM4V\",\"_method\":\"PUT\"}', '2018-12-08 23:14:35', '2018-12-08 23:14:35'),
(1310, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-12-08 23:14:35', '2018-12-08 23:14:35'),
(1311, 1, 'admin/users/16/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-08 23:14:44', '2018-12-08 23:14:44'),
(1312, 1, 'admin/users/16', 'PUT', '127.0.0.1', '{\"first_name\":\"dsaf\",\"last_name\":\"dsfa\",\"username\":\"sdfaadsfa\",\"phone\":null,\"email\":\"dsaffd@gmail.com\",\"password\":null,\"invest_amount\":{\"package_id\":\"1\",\"referral_code\":null,\"status\":\"pending\"},\"_token\":\"I97lopxqJEc2lonSZQ6LJCJyvOu9uKQir1NwDM4V\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/users\"}', '2018-12-08 23:14:51', '2018-12-08 23:14:51'),
(1313, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-12-08 23:14:51', '2018-12-08 23:14:51'),
(1314, 1, 'admin/users/16/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-08 23:14:55', '2018-12-08 23:14:55'),
(1315, 1, 'admin/users/16', 'PUT', '127.0.0.1', '{\"first_name\":\"dsaf\",\"last_name\":\"dsfa\",\"username\":\"sdfaadsfa\",\"phone\":null,\"email\":\"dsaffd@gmail.com\",\"password\":null,\"invest_amount\":{\"package_id\":\"1\",\"referral_code\":null,\"status\":\"paid\"},\"_token\":\"I97lopxqJEc2lonSZQ6LJCJyvOu9uKQir1NwDM4V\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/users\"}', '2018-12-08 23:15:03', '2018-12-08 23:15:03'),
(1316, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-12-08 23:15:03', '2018-12-08 23:15:03'),
(1317, 1, 'admin/users/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-08 23:15:17', '2018-12-08 23:15:17'),
(1318, 1, 'admin/users', 'POST', '127.0.0.1', '{\"first_name\":\"dsaf\",\"last_name\":\"dsfa\",\"username\":\"adsfsadf\",\"phone\":null,\"email\":\"dsaffd@gmail.com\",\"password\":\"123456\",\"invest_amount\":{\"package_id\":\"1\",\"referral_code\":null,\"status\":\"pending\"},\"_token\":\"I97lopxqJEc2lonSZQ6LJCJyvOu9uKQir1NwDM4V\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/users\"}', '2018-12-08 23:15:38', '2018-12-08 23:15:38'),
(1319, 1, 'admin/users/create', 'GET', '127.0.0.1', '[]', '2018-12-08 23:15:38', '2018-12-08 23:15:38'),
(1320, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-08 23:17:57', '2018-12-08 23:17:57'),
(1321, 1, 'admin/users/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-08 23:17:59', '2018-12-08 23:17:59'),
(1322, 1, 'admin/users', 'POST', '127.0.0.1', '{\"first_name\":\"sdfa\",\"last_name\":\"dsaf\",\"username\":\"dsfaasdf\",\"phone\":null,\"email\":\"dsaffd@gmail.com\",\"password\":\"1123456\",\"invest_amount\":{\"package_id\":\"3\",\"referral_code\":null,\"status\":\"pending\"},\"_token\":\"I97lopxqJEc2lonSZQ6LJCJyvOu9uKQir1NwDM4V\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/users\"}', '2018-12-08 23:18:28', '2018-12-08 23:18:28'),
(1323, 1, 'admin/users/create', 'GET', '127.0.0.1', '[]', '2018-12-08 23:18:28', '2018-12-08 23:18:28'),
(1324, 1, 'admin/users/create', 'GET', '127.0.0.1', '[]', '2018-12-08 23:19:47', '2018-12-08 23:19:47'),
(1325, 1, 'admin/users/create', 'GET', '127.0.0.1', '[]', '2018-12-08 23:20:11', '2018-12-08 23:20:11'),
(1326, 1, 'admin/users/create', 'GET', '127.0.0.1', '[]', '2018-12-08 23:21:45', '2018-12-08 23:21:45'),
(1327, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-08 23:21:51', '2018-12-08 23:21:51'),
(1328, 1, 'admin/users/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-08 23:21:56', '2018-12-08 23:21:56'),
(1329, 1, 'admin/users', 'POST', '127.0.0.1', '{\"first_name\":\"afdsdfsa\",\"last_name\":\"dfsdfssadf\",\"username\":\"dfasdsf\",\"phone\":null,\"email\":\"dsaffd@gmail.com\",\"password\":\"123456\",\"invest_amount\":{\"package_id\":\"1\",\"referral_code\":null,\"status\":\"pending\"},\"_token\":\"I97lopxqJEc2lonSZQ6LJCJyvOu9uKQir1NwDM4V\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/users\"}', '2018-12-08 23:22:15', '2018-12-08 23:22:15'),
(1330, 1, 'admin/users/create', 'GET', '127.0.0.1', '[]', '2018-12-08 23:22:15', '2018-12-08 23:22:15'),
(1331, 1, 'admin/users', 'POST', '127.0.0.1', '{\"first_name\":\"afdsdfsa\",\"last_name\":\"dfsdfssadf\",\"username\":\"dfasdsf\",\"phone\":null,\"email\":\"dsaffdd@gmail.com\",\"password\":\"123456\",\"invest_amount\":{\"package_id\":\"1\",\"referral_code\":null,\"status\":\"pending\"},\"_token\":\"I97lopxqJEc2lonSZQ6LJCJyvOu9uKQir1NwDM4V\"}', '2018-12-08 23:22:25', '2018-12-08 23:22:25'),
(1332, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-12-08 23:22:25', '2018-12-08 23:22:25'),
(1333, 1, 'admin/users/18/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-08 23:22:29', '2018-12-08 23:22:29'),
(1334, 1, 'admin/users/18', 'PUT', '127.0.0.1', '{\"first_name\":\"Test\",\"last_name\":\"dfsdfssadf\",\"username\":\"dfasdsf\",\"phone\":null,\"email\":\"dsaffdd@gmail.com\",\"password\":null,\"invest_amount\":{\"package_id\":\"1\",\"referral_code\":null,\"status\":\"pending\"},\"_token\":\"I97lopxqJEc2lonSZQ6LJCJyvOu9uKQir1NwDM4V\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/users\"}', '2018-12-08 23:22:36', '2018-12-08 23:22:36'),
(1335, 1, 'admin/users/18/edit', 'GET', '127.0.0.1', '[]', '2018-12-08 23:22:36', '2018-12-08 23:22:36'),
(1336, 1, 'admin/users/18', 'PUT', '127.0.0.1', '{\"first_name\":\"Test\",\"last_name\":\"dfsdfssadf\",\"username\":\"dfasdsf\",\"phone\":null,\"email\":\"dsaffdd@gmail.com\",\"password\":null,\"invest_amount\":{\"package_id\":\"1\",\"referral_code\":null,\"status\":\"pending\"},\"_token\":\"I97lopxqJEc2lonSZQ6LJCJyvOu9uKQir1NwDM4V\",\"_method\":\"PUT\"}', '2018-12-08 23:22:40', '2018-12-08 23:22:40'),
(1337, 1, 'admin/users/18/edit', 'GET', '127.0.0.1', '[]', '2018-12-08 23:22:41', '2018-12-08 23:22:41'),
(1338, 1, 'admin/users/18', 'PUT', '127.0.0.1', '{\"first_name\":\"Test\",\"last_name\":\"dfsdfssadf\",\"username\":\"dfasdsf\",\"phone\":null,\"email\":\"dsaffdd@gmail.com\",\"password\":null,\"invest_amount\":{\"package_id\":\"1\",\"referral_code\":null,\"status\":\"pending\"},\"_token\":\"I97lopxqJEc2lonSZQ6LJCJyvOu9uKQir1NwDM4V\",\"_method\":\"PUT\"}', '2018-12-08 23:23:08', '2018-12-08 23:23:08'),
(1339, 1, 'admin/users/18/edit', 'GET', '127.0.0.1', '[]', '2018-12-08 23:23:08', '2018-12-08 23:23:08'),
(1340, 1, 'admin/users/18/edit', 'GET', '127.0.0.1', '[]', '2018-12-08 23:23:11', '2018-12-08 23:23:11'),
(1341, 1, 'admin/users/18', 'PUT', '127.0.0.1', '{\"first_name\":\"afdsdfsa\",\"last_name\":\"dfsdfssadf\",\"username\":\"dfasdsf\",\"phone\":null,\"email\":\"dsaffdd@gmail.com\",\"password\":null,\"invest_amount\":{\"package_id\":\"1\",\"referral_code\":null,\"status\":\"pending\"},\"_token\":\"I97lopxqJEc2lonSZQ6LJCJyvOu9uKQir1NwDM4V\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/users\\/create\"}', '2018-12-08 23:23:14', '2018-12-08 23:23:14'),
(1342, 1, 'admin/users/18/edit', 'GET', '127.0.0.1', '[]', '2018-12-08 23:23:14', '2018-12-08 23:23:14'),
(1343, 1, 'admin/users/18/edit', 'GET', '127.0.0.1', '[]', '2018-12-08 23:23:52', '2018-12-08 23:23:52'),
(1344, 1, 'admin/users/18', 'PUT', '127.0.0.1', '{\"first_name\":\"afdsdfsa\",\"last_name\":\"dfsdfssadf\",\"username\":\"dfasdsf\",\"phone\":null,\"email\":\"dsaffdd@gmail.com\",\"password\":null,\"invest_amount\":{\"package_id\":\"1\",\"referral_code\":null,\"status\":\"pending\"},\"_token\":\"I97lopxqJEc2lonSZQ6LJCJyvOu9uKQir1NwDM4V\",\"_method\":\"PUT\"}', '2018-12-08 23:24:00', '2018-12-08 23:24:00'),
(1345, 1, 'admin/users/18/edit', 'GET', '127.0.0.1', '[]', '2018-12-08 23:24:00', '2018-12-08 23:24:00'),
(1346, 1, 'admin/users/18/edit', 'GET', '127.0.0.1', '[]', '2018-12-08 23:24:30', '2018-12-08 23:24:30'),
(1347, 1, 'admin/users/18', 'PUT', '127.0.0.1', '{\"first_name\":\"afdsdfsa\",\"last_name\":\"dfsdfssadf\",\"username\":\"dfasdsf\",\"phone\":null,\"email\":\"dsaffdd@gmail.com\",\"password\":null,\"invest_amount\":{\"package_id\":\"1\",\"referral_code\":null,\"status\":\"pending\"},\"_token\":\"I97lopxqJEc2lonSZQ6LJCJyvOu9uKQir1NwDM4V\",\"_method\":\"PUT\"}', '2018-12-08 23:24:32', '2018-12-08 23:24:32'),
(1348, 1, 'admin/users/18/edit', 'GET', '127.0.0.1', '[]', '2018-12-08 23:24:32', '2018-12-08 23:24:32'),
(1349, 1, 'admin/users/18', 'PUT', '127.0.0.1', '{\"first_name\":\"afdsdfsa\",\"last_name\":\"dfsdfssadf\",\"username\":\"dfasdsf\",\"phone\":null,\"email\":\"dsaffdd@gmail.com\",\"password\":null,\"invest_amount\":{\"package_id\":\"1\",\"referral_code\":null,\"status\":\"pending\"},\"_token\":\"I97lopxqJEc2lonSZQ6LJCJyvOu9uKQir1NwDM4V\",\"_method\":\"PUT\"}', '2018-12-08 23:29:03', '2018-12-08 23:29:03'),
(1350, 1, 'admin/users/18/edit', 'GET', '127.0.0.1', '[]', '2018-12-08 23:29:04', '2018-12-08 23:29:04'),
(1351, 1, 'admin/users/18/edit', 'GET', '127.0.0.1', '[]', '2018-12-08 23:29:06', '2018-12-08 23:29:06'),
(1352, 1, 'admin/users/18', 'PUT', '127.0.0.1', '{\"first_name\":\"Test\",\"last_name\":\"dfsdfssadf\",\"username\":\"dfasdsf\",\"phone\":null,\"email\":\"dsaffdd@gmail.com\",\"password\":null,\"invest_amount\":{\"package_id\":\"1\",\"referral_code\":null,\"status\":\"pending\"},\"_token\":\"I97lopxqJEc2lonSZQ6LJCJyvOu9uKQir1NwDM4V\",\"_method\":\"PUT\"}', '2018-12-08 23:29:14', '2018-12-08 23:29:14'),
(1353, 1, 'admin/users/18/edit', 'GET', '127.0.0.1', '[]', '2018-12-08 23:29:14', '2018-12-08 23:29:14'),
(1354, 1, 'admin/users/18', 'PUT', '127.0.0.1', '{\"first_name\":\"Test\",\"last_name\":\"dfsdfssadf\",\"username\":\"dfasdsf\",\"phone\":null,\"email\":\"dsaffdd@gmail.com\",\"password\":null,\"invest_amount\":{\"package_id\":\"1\",\"referral_code\":null,\"status\":\"pending\"},\"_token\":\"I97lopxqJEc2lonSZQ6LJCJyvOu9uKQir1NwDM4V\",\"_method\":\"PUT\"}', '2018-12-08 23:29:18', '2018-12-08 23:29:18'),
(1355, 1, 'admin/users/18/edit', 'GET', '127.0.0.1', '[]', '2018-12-08 23:29:18', '2018-12-08 23:29:18'),
(1356, 1, 'admin/users/18/edit', 'GET', '127.0.0.1', '[]', '2018-12-08 23:29:21', '2018-12-08 23:29:21'),
(1357, 1, 'admin/users/18/edit', 'GET', '127.0.0.1', '[]', '2018-12-08 23:29:33', '2018-12-08 23:29:33'),
(1358, 1, 'admin/users/18', 'PUT', '127.0.0.1', '{\"first_name\":\"afdsdfsa\",\"last_name\":\"dfsdfssadf\",\"username\":\"dfasdsf\",\"phone\":null,\"email\":\"dsaffdd@gmail.com\",\"password\":null,\"invest_amount\":{\"package_id\":\"1\",\"referral_code\":null,\"status\":\"pending\"},\"_token\":\"I97lopxqJEc2lonSZQ6LJCJyvOu9uKQir1NwDM4V\",\"_method\":\"PUT\"}', '2018-12-08 23:29:37', '2018-12-08 23:29:37'),
(1359, 1, 'admin/users/18/edit', 'GET', '127.0.0.1', '[]', '2018-12-08 23:29:37', '2018-12-08 23:29:37'),
(1360, 1, 'admin/users/18', 'PUT', '127.0.0.1', '{\"first_name\":\"afdsdfsa\",\"last_name\":\"dfsdfssadf\",\"username\":\"dfasdsf\",\"phone\":null,\"email\":\"dsaffdd@gmail.com\",\"password\":null,\"invest_amount\":{\"package_id\":\"1\",\"referral_code\":null,\"status\":\"pending\"},\"_token\":\"I97lopxqJEc2lonSZQ6LJCJyvOu9uKQir1NwDM4V\",\"_method\":\"PUT\"}', '2018-12-08 23:29:41', '2018-12-08 23:29:41'),
(1361, 1, 'admin/users/18/edit', 'GET', '127.0.0.1', '[]', '2018-12-08 23:29:41', '2018-12-08 23:29:41'),
(1362, 1, 'admin/users/18/edit', 'GET', '127.0.0.1', '[]', '2018-12-08 23:30:45', '2018-12-08 23:30:45'),
(1363, 1, 'admin/users/18', 'PUT', '127.0.0.1', '{\"first_name\":\"afdsdfsa\",\"last_name\":\"dfsdfssadf\",\"username\":\"dfasdsf\",\"phone\":null,\"email\":\"dsaffdd@gmail.com\",\"password\":null,\"invest_amount\":{\"package_id\":\"1\",\"referral_code\":null,\"status\":\"pending\"},\"_token\":\"I97lopxqJEc2lonSZQ6LJCJyvOu9uKQir1NwDM4V\",\"_method\":\"PUT\"}', '2018-12-08 23:30:49', '2018-12-08 23:30:49'),
(1364, 1, 'admin/users/18/edit', 'GET', '127.0.0.1', '[]', '2018-12-08 23:30:49', '2018-12-08 23:30:49'),
(1365, 1, 'admin/users/18/edit', 'GET', '127.0.0.1', '[]', '2018-12-08 23:30:53', '2018-12-08 23:30:53'),
(1366, 1, 'admin/users/18', 'PUT', '127.0.0.1', '{\"first_name\":\"afdsdfsa\",\"last_name\":\"dfsdfssadf\",\"username\":\"dfasdsf\",\"phone\":null,\"email\":\"dsaffdd@gmail.com\",\"password\":null,\"invest_amount\":{\"package_id\":\"1\",\"referral_code\":null,\"status\":\"pending\"},\"_token\":\"I97lopxqJEc2lonSZQ6LJCJyvOu9uKQir1NwDM4V\",\"_method\":\"PUT\"}', '2018-12-08 23:30:59', '2018-12-08 23:30:59'),
(1367, 1, 'admin/users/18/edit', 'GET', '127.0.0.1', '[]', '2018-12-08 23:30:59', '2018-12-08 23:30:59'),
(1368, 1, 'admin/users/18/edit', 'GET', '127.0.0.1', '[]', '2018-12-08 23:31:11', '2018-12-08 23:31:11'),
(1369, 1, 'admin/users/18', 'PUT', '127.0.0.1', '{\"first_name\":\"afdsdfsa\",\"last_name\":\"dfsdfssadf\",\"username\":\"dfasdsf\",\"phone\":null,\"password\":null,\"invest_amount\":{\"package_id\":\"1\",\"referral_code\":null,\"status\":\"pending\"},\"_token\":\"I97lopxqJEc2lonSZQ6LJCJyvOu9uKQir1NwDM4V\",\"_method\":\"PUT\"}', '2018-12-08 23:31:15', '2018-12-08 23:31:15'),
(1370, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-12-08 23:31:16', '2018-12-08 23:31:16'),
(1371, 1, 'admin/users/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-08 23:31:23', '2018-12-08 23:31:23'),
(1372, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-08 23:31:26', '2018-12-08 23:31:26'),
(1373, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-12-08 23:32:39', '2018-12-08 23:32:39'),
(1374, 1, 'admin/users/18/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-08 23:32:44', '2018-12-08 23:32:44'),
(1375, 1, 'admin/users/18', 'PUT', '127.0.0.1', '{\"first_name\":\"afdsdfsa\",\"last_name\":\"dfsdfssadf\",\"phone\":null,\"password\":null,\"invest_amount\":{\"package_id\":\"1\",\"referral_code\":null,\"status\":\"paid\"},\"_token\":\"I97lopxqJEc2lonSZQ6LJCJyvOu9uKQir1NwDM4V\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/users\"}', '2018-12-08 23:32:51', '2018-12-08 23:32:51'),
(1376, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-12-08 23:32:51', '2018-12-08 23:32:51'),
(1377, 1, 'admin/users/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-08 23:33:09', '2018-12-08 23:33:09'),
(1378, 1, 'admin/users', 'POST', '127.0.0.1', '{\"first_name\":\"dsaf\",\"last_name\":\"dafs\",\"username\":\"anwar\",\"phone\":null,\"email\":\"anwar.hussen4@gmail.com\",\"password\":\"123456\",\"invest_amount\":{\"package_id\":\"1\",\"referral_code\":null,\"status\":\"pending\"},\"_token\":\"I97lopxqJEc2lonSZQ6LJCJyvOu9uKQir1NwDM4V\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/users\"}', '2018-12-08 23:33:29', '2018-12-08 23:33:29'),
(1379, 1, 'admin/users/create', 'GET', '127.0.0.1', '[]', '2018-12-08 23:33:29', '2018-12-08 23:33:29'),
(1380, 1, 'admin/users/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-08 23:33:39', '2018-12-08 23:33:39'),
(1381, 1, 'admin/users', 'POST', '127.0.0.1', '{\"first_name\":\"adasf\",\"last_name\":\"sdafdsaf\",\"username\":\"dfsadsaf\",\"phone\":null,\"email\":\"fdadfas@gmail.com\",\"password\":null,\"invest_amount\":{\"package_id\":\"1\",\"referral_code\":null,\"status\":null},\"_token\":\"I97lopxqJEc2lonSZQ6LJCJyvOu9uKQir1NwDM4V\"}', '2018-12-08 23:34:52', '2018-12-08 23:34:52'),
(1382, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-12-08 23:35:33', '2018-12-08 23:35:33'),
(1383, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-12-08 23:35:51', '2018-12-08 23:35:51'),
(1384, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-12-08 23:36:09', '2018-12-08 23:36:09'),
(1385, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-08 23:36:21', '2018-12-08 23:36:21'),
(1386, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-08 23:36:22', '2018-12-08 23:36:22'),
(1387, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-08 23:36:24', '2018-12-08 23:36:24'),
(1388, 1, 'admin/training_contents', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-08 23:36:25', '2018-12-08 23:36:25'),
(1389, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-08 23:36:25', '2018-12-08 23:36:25'),
(1390, 1, 'admin/transactions', 'GET', '127.0.0.1', '[]', '2018-12-08 23:38:07', '2018-12-08 23:38:07'),
(1391, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-08 23:38:16', '2018-12-08 23:38:16'),
(1392, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-08 23:38:18', '2018-12-08 23:38:18'),
(1393, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-08 23:38:20', '2018-12-08 23:38:20'),
(1394, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-08 23:38:21', '2018-12-08 23:38:21'),
(1395, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-08 23:38:22', '2018-12-08 23:38:22'),
(1396, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-08 23:38:22', '2018-12-08 23:38:22'),
(1397, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-08 23:38:23', '2018-12-08 23:38:23'),
(1398, 1, 'admin/training_contents', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-08 23:38:24', '2018-12-08 23:38:24'),
(1399, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-08 23:38:24', '2018-12-08 23:38:24'),
(1400, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-08 23:38:26', '2018-12-08 23:38:26'),
(1401, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-12-08 23:38:50', '2018-12-08 23:38:50'),
(1402, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-08 23:38:54', '2018-12-08 23:38:54'),
(1403, 1, 'admin/training_contents', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-08 23:38:57', '2018-12-08 23:38:57'),
(1404, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-08 23:38:58', '2018-12-08 23:38:58'),
(1405, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-08 23:38:58', '2018-12-08 23:38:58'),
(1406, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-08 23:38:59', '2018-12-08 23:38:59'),
(1407, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-08 23:39:00', '2018-12-08 23:39:00'),
(1408, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-08 23:39:01', '2018-12-08 23:39:01'),
(1409, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-08 23:39:03', '2018-12-08 23:39:03'),
(1410, 1, 'admin/training_contents', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-08 23:39:03', '2018-12-08 23:39:03'),
(1411, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-08 23:39:04', '2018-12-08 23:39:04'),
(1412, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-08 23:39:05', '2018-12-08 23:39:05'),
(1413, 1, 'admin/training_contents', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-08 23:39:06', '2018-12-08 23:39:06'),
(1414, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-08 23:39:07', '2018-12-08 23:39:07'),
(1415, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-08 23:39:38', '2018-12-08 23:39:38'),
(1416, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-08 23:39:40', '2018-12-08 23:39:40'),
(1417, 1, 'admin/training_contents', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-08 23:39:41', '2018-12-08 23:39:41'),
(1418, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-08 23:39:42', '2018-12-08 23:39:42');
INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(1419, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-08 23:39:43', '2018-12-08 23:39:43'),
(1420, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-08 23:39:44', '2018-12-08 23:39:44'),
(1421, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-08 23:39:44', '2018-12-08 23:39:44'),
(1422, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-12-08 23:48:45', '2018-12-08 23:48:45'),
(1423, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-09 00:08:20', '2018-12-09 00:08:20'),
(1424, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-12-09 00:08:20', '2018-12-09 00:08:20'),
(1425, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-12-09 00:08:37', '2018-12-09 00:08:37'),
(1426, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-09 00:08:41', '2018-12-09 00:08:41'),
(1427, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-12-09 00:08:41', '2018-12-09 00:08:41'),
(1428, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-09 00:09:13', '2018-12-09 00:09:13'),
(1429, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-12-09 00:09:13', '2018-12-09 00:09:13'),
(1430, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-09 00:09:23', '2018-12-09 00:09:23'),
(1431, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-12-09 00:09:23', '2018-12-09 00:09:23'),
(1432, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-12-09 00:12:52', '2018-12-09 00:12:52'),
(1433, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-09 00:12:55', '2018-12-09 00:12:55'),
(1434, 1, 'admin/users/15/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-09 00:13:02', '2018-12-09 00:13:02'),
(1435, 1, 'admin/users/15', 'PUT', '127.0.0.1', '{\"first_name\":\"Abwar\",\"last_name\":\"Huss\",\"phone\":null,\"password\":null,\"invest_amount\":{\"package_id\":\"1\",\"referral_code\":null,\"status\":\"paid\"},\"_token\":\"I97lopxqJEc2lonSZQ6LJCJyvOu9uKQir1NwDM4V\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/users\"}', '2018-12-09 00:13:10', '2018-12-09 00:13:10'),
(1436, 1, 'admin/users/15/edit', 'GET', '127.0.0.1', '[]', '2018-12-09 00:13:10', '2018-12-09 00:13:10'),
(1437, 1, 'admin/users/15', 'PUT', '127.0.0.1', '{\"first_name\":\"Abwar\",\"last_name\":\"Huss\",\"phone\":null,\"password\":\"123456\",\"invest_amount\":{\"package_id\":\"1\",\"referral_code\":null,\"status\":\"paid\"},\"_token\":\"I97lopxqJEc2lonSZQ6LJCJyvOu9uKQir1NwDM4V\",\"_method\":\"PUT\"}', '2018-12-09 00:13:39', '2018-12-09 00:13:39'),
(1438, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-12-09 00:13:40', '2018-12-09 00:13:40'),
(1439, 1, 'admin/users/14/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-09 00:13:56', '2018-12-09 00:13:56'),
(1440, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-09 00:14:55', '2018-12-09 00:14:55'),
(1441, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-09 00:14:57', '2018-12-09 00:14:57'),
(1442, 1, 'admin/users/14/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-09 00:15:05', '2018-12-09 00:15:05'),
(1443, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-09 00:15:18', '2018-12-09 00:15:18'),
(1444, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-12-09 00:15:41', '2018-12-09 00:15:41'),
(1445, 1, 'admin/users/14,15,16,18', 'DELETE', '127.0.0.1', '{\"_method\":\"delete\",\"_token\":\"I97lopxqJEc2lonSZQ6LJCJyvOu9uKQir1NwDM4V\"}', '2018-12-09 00:16:39', '2018-12-09 00:16:39'),
(1446, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-09 00:16:39', '2018-12-09 00:16:39'),
(1447, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-09 00:16:43', '2018-12-09 00:16:43'),
(1448, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-09 00:16:45', '2018-12-09 00:16:45'),
(1449, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-09 00:16:48', '2018-12-09 00:16:48'),
(1450, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-09 00:16:49', '2018-12-09 00:16:49'),
(1451, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-09 00:16:50', '2018-12-09 00:16:50'),
(1452, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-09 00:16:50', '2018-12-09 00:16:50'),
(1453, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-09 00:16:51', '2018-12-09 00:16:51'),
(1454, 1, 'admin/training_contents', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-09 00:16:52', '2018-12-09 00:16:52'),
(1455, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-09 00:16:53', '2018-12-09 00:16:53'),
(1456, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-09 00:16:56', '2018-12-09 00:16:56'),
(1457, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-09 00:16:59', '2018-12-09 00:16:59'),
(1458, 1, 'admin/users/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-09 00:17:00', '2018-12-09 00:17:00'),
(1459, 1, 'admin/users', 'POST', '127.0.0.1', '{\"first_name\":\"Anwar\",\"last_name\":\"Hussen\",\"username\":\"anwar\",\"phone\":\"01722566545\",\"email\":\"anwar.hussen4@gmail.com\",\"password\":\"123456\",\"invest_amount\":{\"package_id\":\"1\",\"referral_code\":null,\"status\":\"paid\"},\"_token\":\"I97lopxqJEc2lonSZQ6LJCJyvOu9uKQir1NwDM4V\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/users\"}', '2018-12-09 00:17:44', '2018-12-09 00:17:44'),
(1460, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-12-09 00:17:45', '2018-12-09 00:17:45'),
(1461, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-09 00:17:55', '2018-12-09 00:17:55'),
(1462, 1, 'admin/users/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-09 00:19:28', '2018-12-09 00:19:28'),
(1463, 1, 'admin/users', 'POST', '127.0.0.1', '{\"first_name\":\"Anwar\",\"last_name\":\"2\",\"username\":\"anwar2\",\"phone\":null,\"email\":\"anwar2@gmail.com\",\"password\":\"123456\",\"invest_amount\":{\"package_id\":\"1\",\"referral_code\":\"anwar\",\"status\":\"paid\"},\"_token\":\"9r52U2zwZKkPyhI72cQdQ1sVQkmxmk6XsHMdMTwS\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/users\"}', '2018-12-09 00:20:07', '2018-12-09 00:20:07'),
(1464, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-12-09 00:20:08', '2018-12-09 00:20:08'),
(1465, 1, 'admin/users/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-09 00:20:22', '2018-12-09 00:20:22'),
(1466, 1, 'admin/users', 'POST', '127.0.0.1', '{\"first_name\":\"Anwar\",\"last_name\":\"3\",\"username\":\"anwar3\",\"phone\":null,\"email\":\"anwar3@gmail.com\",\"password\":\"123456\",\"invest_amount\":{\"package_id\":\"1\",\"referral_code\":\"anwar\",\"status\":\"pending\"},\"_token\":\"9r52U2zwZKkPyhI72cQdQ1sVQkmxmk6XsHMdMTwS\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/users\"}', '2018-12-09 00:21:03', '2018-12-09 00:21:03'),
(1467, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-12-09 00:21:04', '2018-12-09 00:21:04'),
(1468, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-09 00:21:13', '2018-12-09 00:21:13'),
(1469, 1, 'admin/transactions/1,2,3,4', 'DELETE', '127.0.0.1', '{\"_method\":\"delete\",\"_token\":\"9r52U2zwZKkPyhI72cQdQ1sVQkmxmk6XsHMdMTwS\"}', '2018-12-09 00:21:45', '2018-12-09 00:21:45'),
(1470, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-09 00:21:45', '2018-12-09 00:21:45'),
(1471, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-09 00:21:55', '2018-12-09 00:21:55'),
(1472, 1, 'admin/users/21/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-09 00:22:02', '2018-12-09 00:22:02'),
(1473, 1, 'admin/users/21', 'PUT', '127.0.0.1', '{\"first_name\":\"Anwar\",\"last_name\":\"3\",\"phone\":null,\"password\":null,\"invest_amount\":{\"package_id\":\"1\",\"referral_code\":\"anwar\",\"status\":\"paid\"},\"_token\":\"9r52U2zwZKkPyhI72cQdQ1sVQkmxmk6XsHMdMTwS\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/users\"}', '2018-12-09 00:22:07', '2018-12-09 00:22:07'),
(1474, 1, 'admin/users/21/edit', 'GET', '127.0.0.1', '[]', '2018-12-09 00:22:07', '2018-12-09 00:22:07'),
(1475, 1, 'admin/users/21', 'PUT', '127.0.0.1', '{\"first_name\":\"Anwar\",\"last_name\":\"3\",\"phone\":null,\"password\":\"123456\",\"invest_amount\":{\"package_id\":\"1\",\"referral_code\":\"anwar\",\"status\":\"paid\"},\"_token\":\"9r52U2zwZKkPyhI72cQdQ1sVQkmxmk6XsHMdMTwS\",\"_method\":\"PUT\"}', '2018-12-09 00:22:32', '2018-12-09 00:22:32'),
(1476, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-12-09 00:22:32', '2018-12-09 00:22:32'),
(1477, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-09 00:22:43', '2018-12-09 00:22:43'),
(1478, 1, 'admin/transactions', 'GET', '127.0.0.1', '[]', '2018-12-09 00:35:14', '2018-12-09 00:35:14'),
(1479, 1, 'admin/transactions/9', 'PUT', '127.0.0.1', '{\"name\":\"status\",\"value\":\"accepted\",\"pk\":\"9\",\"_token\":\"9r52U2zwZKkPyhI72cQdQ1sVQkmxmk6XsHMdMTwS\",\"_editable\":\"1\",\"_method\":\"PUT\"}', '2018-12-09 00:35:33', '2018-12-09 00:35:33'),
(1480, 1, 'admin/transactions', 'GET', '127.0.0.1', '[]', '2018-12-09 00:35:46', '2018-12-09 00:35:46'),
(1481, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-09 00:36:19', '2018-12-09 00:36:19'),
(1482, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-09 01:07:49', '2018-12-09 01:07:49'),
(1483, 1, 'admin/training_contents', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-09 01:07:51', '2018-12-09 01:07:51'),
(1484, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-09 01:07:52', '2018-12-09 01:07:52'),
(1485, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-09 01:08:02', '2018-12-09 01:08:02'),
(1486, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-12-10 03:33:50', '2018-12-10 03:33:50'),
(1487, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-10 03:33:58', '2018-12-10 03:33:58'),
(1488, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-10 03:34:05', '2018-12-10 03:34:05'),
(1489, 1, 'admin/users/20/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-10 03:34:19', '2018-12-10 03:34:19'),
(1490, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-10 03:34:34', '2018-12-10 03:34:34'),
(1491, 1, 'admin/users/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-10 03:34:37', '2018-12-10 03:34:37'),
(1492, 1, 'admin/users', 'POST', '127.0.0.1', '{\"first_name\":\"Kamrul\",\"last_name\":\"Islam\",\"username\":\"rono\",\"phone\":\"01672246318\",\"email\":\"dreamfighterr@gmail.com\",\"password\":\"123456\",\"invest_amount\":{\"package_id\":\"1\",\"referral_code\":\"anwar2\",\"status\":\"paid\"},\"_token\":\"xKVZx453WmQIrrrDY0kAR3xRguquAyjo1TvsHkFK\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/users\"}', '2018-12-10 03:35:08', '2018-12-10 03:35:08'),
(1493, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-12-10 03:35:09', '2018-12-10 03:35:09'),
(1494, 1, 'admin/users/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-10 03:35:18', '2018-12-10 03:35:18'),
(1495, 1, 'admin/users', 'POST', '127.0.0.1', '{\"first_name\":\"Piya\",\"last_name\":\"Chowdhury\",\"username\":\"piya\",\"phone\":\"01672246318\",\"email\":\"piya@gmail.com\",\"password\":\"123456\",\"invest_amount\":{\"package_id\":\"2\",\"referral_code\":\"rono\",\"status\":\"pending\"},\"_token\":\"xKVZx453WmQIrrrDY0kAR3xRguquAyjo1TvsHkFK\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/users\"}', '2018-12-10 03:35:57', '2018-12-10 03:35:57'),
(1496, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-12-10 03:35:57', '2018-12-10 03:35:57'),
(1497, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-12-10 03:37:52', '2018-12-10 03:37:52'),
(1498, 1, 'admin/users/22/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-10 03:37:57', '2018-12-10 03:37:57'),
(1499, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-10 03:38:02', '2018-12-10 03:38:02'),
(1500, 1, 'admin/additional_returns/rono', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-10 03:38:05', '2018-12-10 03:38:05'),
(1501, 1, 'admin/additional_returns/rono', 'POST', '127.0.0.1', '{\"_token\":\"xKVZx453WmQIrrrDY0kAR3xRguquAyjo1TvsHkFK\",\"month\":\"December - 18\",\"amount\":\"120\"}', '2018-12-10 03:38:10', '2018-12-10 03:38:10'),
(1502, 1, 'admin/additional_returns/rono', 'GET', '127.0.0.1', '[]', '2018-12-10 03:38:11', '2018-12-10 03:38:11'),
(1503, 1, 'admin/auth/logout', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-10 03:38:51', '2018-12-10 03:38:51'),
(1504, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-12-10 03:39:16', '2018-12-10 03:39:16'),
(1505, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-10 03:39:44', '2018-12-10 03:39:44'),
(1506, 1, 'admin/users/23/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-10 03:39:53', '2018-12-10 03:39:53'),
(1507, 1, 'admin/users/23', 'PUT', '127.0.0.1', '{\"first_name\":\"Piya\",\"last_name\":\"Chowdhury\",\"phone\":\"01672246318\",\"password\":null,\"invest_amount\":{\"package_id\":\"2\",\"referral_code\":\"rono\",\"status\":\"paid\"},\"_token\":\"QjrOzwFNXKgNvxy8e2uhbhuYoBNxRli2DCtWx9DB\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/users\"}', '2018-12-10 03:40:00', '2018-12-10 03:40:00'),
(1508, 1, 'admin/users/23/edit', 'GET', '127.0.0.1', '[]', '2018-12-10 03:40:00', '2018-12-10 03:40:00'),
(1509, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-10 03:40:13', '2018-12-10 03:40:13'),
(1510, 1, 'admin/users/23/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-10 03:40:16', '2018-12-10 03:40:16'),
(1511, 1, 'admin/users/23', 'PUT', '127.0.0.1', '{\"first_name\":\"Piya\",\"last_name\":\"Chowdhury\",\"phone\":\"01672246318\",\"password\":\"123456\",\"invest_amount\":{\"package_id\":\"2\",\"referral_code\":\"rono\",\"status\":\"paid\"},\"_token\":\"QjrOzwFNXKgNvxy8e2uhbhuYoBNxRli2DCtWx9DB\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/users\"}', '2018-12-10 03:40:23', '2018-12-10 03:40:23'),
(1512, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-12-10 03:40:24', '2018-12-10 03:40:24'),
(1513, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-12-10 03:40:45', '2018-12-10 03:40:45'),
(1514, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-10 03:41:49', '2018-12-10 03:41:49'),
(1515, 1, 'admin/transactions', 'GET', '127.0.0.1', '[]', '2018-12-10 03:42:14', '2018-12-10 03:42:14'),
(1516, 1, 'admin/transactions', 'GET', '127.0.0.1', '[]', '2018-12-10 03:44:22', '2018-12-10 03:44:22'),
(1517, 1, 'admin/transactions/20', 'PUT', '127.0.0.1', '{\"name\":\"status\",\"value\":\"accepted\",\"pk\":\"20\",\"_token\":\"QjrOzwFNXKgNvxy8e2uhbhuYoBNxRli2DCtWx9DB\",\"_editable\":\"1\",\"_method\":\"PUT\"}', '2018-12-10 03:44:30', '2018-12-10 03:44:30'),
(1518, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-10 03:46:38', '2018-12-10 03:46:38'),
(1519, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-10 03:46:46', '2018-12-10 03:46:46'),
(1520, 1, 'admin/auth/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-10 03:46:49', '2018-12-10 03:46:49'),
(1521, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-10 03:46:58', '2018-12-10 03:46:58'),
(1522, 1, 'admin/transactions/20', 'PUT', '127.0.0.1', '{\"name\":\"status\",\"value\":\"pending\",\"pk\":\"20\",\"_token\":\"QjrOzwFNXKgNvxy8e2uhbhuYoBNxRli2DCtWx9DB\",\"_editable\":\"1\",\"_method\":\"PUT\"}', '2018-12-10 03:47:10', '2018-12-10 03:47:10'),
(1523, 1, 'admin/transactions', 'GET', '127.0.0.1', '[]', '2018-12-10 03:50:08', '2018-12-10 03:50:08'),
(1524, 1, 'admin/transactions/21', 'PUT', '127.0.0.1', '{\"name\":\"status\",\"value\":\"accepted\",\"pk\":\"21\",\"_token\":\"QjrOzwFNXKgNvxy8e2uhbhuYoBNxRli2DCtWx9DB\",\"_editable\":\"1\",\"_method\":\"PUT\"}', '2018-12-10 03:50:19', '2018-12-10 03:50:19'),
(1525, 1, 'admin/transactions/20', 'PUT', '127.0.0.1', '{\"name\":\"status\",\"value\":\"rejected\",\"pk\":\"20\",\"_token\":\"QjrOzwFNXKgNvxy8e2uhbhuYoBNxRli2DCtWx9DB\",\"_editable\":\"1\",\"_method\":\"PUT\"}', '2018-12-10 03:50:51', '2018-12-10 03:50:51'),
(1526, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_export_\":\"all\"}', '2018-12-10 04:33:29', '2018-12-10 04:33:29'),
(1527, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-10 05:42:43', '2018-12-10 05:42:43'),
(1528, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-10 05:42:46', '2018-12-10 05:42:46'),
(1529, 1, 'admin/training_contents', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-10 05:45:13', '2018-12-10 05:45:13'),
(1530, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-10 05:45:18', '2018-12-10 05:45:18'),
(1531, 1, 'admin/news/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-10 05:45:22', '2018-12-10 05:45:22'),
(1532, 1, 'admin/news', 'POST', '127.0.0.1', '{\"title\":\"New news from admin\",\"content\":\"<p>hello this is a test news.<\\/p>\",\"_token\":\"QjrOzwFNXKgNvxy8e2uhbhuYoBNxRli2DCtWx9DB\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/news\"}', '2018-12-10 05:45:40', '2018-12-10 05:45:40'),
(1533, 1, 'admin/news', 'GET', '127.0.0.1', '[]', '2018-12-10 05:45:40', '2018-12-10 05:45:40'),
(1534, 1, 'admin/training_contents', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-10 05:46:13', '2018-12-10 05:46:13'),
(1535, 1, 'admin/training_contents', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-10 05:46:22', '2018-12-10 05:46:22'),
(1536, 1, 'admin/training_contents/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-10 05:46:24', '2018-12-10 05:46:24'),
(1537, 1, 'admin/training_contents', 'POST', '127.0.0.1', '{\"caption\":\"New training content\",\"content\":\"<p>test content&nbsp;<\\/p>\",\"_token\":\"QjrOzwFNXKgNvxy8e2uhbhuYoBNxRli2DCtWx9DB\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/training_contents\"}', '2018-12-10 05:46:47', '2018-12-10 05:46:47'),
(1538, 1, 'admin/training_contents', 'GET', '127.0.0.1', '[]', '2018-12-10 05:46:47', '2018-12-10 05:46:47'),
(1539, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-10 06:14:30', '2018-12-10 06:14:30'),
(1540, 1, 'admin/transactions', 'GET', '127.0.0.1', '[]', '2018-12-10 06:14:35', '2018-12-10 06:14:35'),
(1541, 1, 'admin/transactions', 'GET', '127.0.0.1', '[]', '2018-12-10 06:15:02', '2018-12-10 06:15:02'),
(1542, 1, 'admin/transactions/22', 'PUT', '127.0.0.1', '{\"name\":\"status\",\"value\":\"accepted\",\"pk\":\"22\",\"_token\":\"QjrOzwFNXKgNvxy8e2uhbhuYoBNxRli2DCtWx9DB\",\"_editable\":\"1\",\"_method\":\"PUT\"}', '2018-12-10 06:15:12', '2018-12-10 06:15:12'),
(1543, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-10 06:20:09', '2018-12-10 06:20:09'),
(1544, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-10 06:20:20', '2018-12-10 06:20:20'),
(1545, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-10 06:20:25', '2018-12-10 06:20:25'),
(1546, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-10 06:21:19', '2018-12-10 06:21:19'),
(1547, 1, 'admin/additional_returns/anwar', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-10 06:21:31', '2018-12-10 06:21:31'),
(1548, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-10 06:21:54', '2018-12-10 06:21:54'),
(1549, 1, 'admin/additional_returns/rono', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-10 06:21:57', '2018-12-10 06:21:57'),
(1550, 1, 'admin/additional_returns/rono', 'POST', '127.0.0.1', '{\"_token\":\"QjrOzwFNXKgNvxy8e2uhbhuYoBNxRli2DCtWx9DB\",\"month\":\"December - 18\",\"amount\":\"220\"}', '2018-12-10 06:22:21', '2018-12-10 06:22:21'),
(1551, 1, 'admin/additional_returns/rono', 'GET', '127.0.0.1', '[]', '2018-12-10 06:22:22', '2018-12-10 06:22:22'),
(1552, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-10 06:30:06', '2018-12-10 06:30:06'),
(1553, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-10 06:30:24', '2018-12-10 06:30:24'),
(1554, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-10 06:33:09', '2018-12-10 06:33:09'),
(1555, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-10 06:34:33', '2018-12-10 06:34:33'),
(1556, 1, 'admin/users/24/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-10 06:34:42', '2018-12-10 06:34:42'),
(1557, 1, 'admin/users/24', 'PUT', '127.0.0.1', '{\"first_name\":\"kamrul\",\"last_name\":\"arif\",\"phone\":null,\"password\":\"admin\",\"invest_amount\":{\"package_id\":\"3\",\"referral_code\":\"rono\",\"status\":\"paid\"},\"_token\":\"QjrOzwFNXKgNvxy8e2uhbhuYoBNxRli2DCtWx9DB\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/users\"}', '2018-12-10 06:34:46', '2018-12-10 06:34:46'),
(1558, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-12-10 06:34:47', '2018-12-10 06:34:47'),
(1559, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-10 06:35:42', '2018-12-10 06:35:42'),
(1560, 1, 'admin/packages', 'GET', '127.0.0.1', '[]', '2018-12-10 08:07:43', '2018-12-10 08:07:43'),
(1561, 1, 'admin/packages', 'GET', '127.0.0.1', '[]', '2018-12-10 08:08:10', '2018-12-10 08:08:10'),
(1562, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-12-11 03:11:08', '2018-12-11 03:11:08'),
(1563, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-11 03:14:10', '2018-12-11 03:14:10'),
(1564, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-11 03:17:50', '2018-12-11 03:17:50'),
(1565, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-12-11 03:21:28', '2018-12-11 03:21:28'),
(1566, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-12-11 03:23:43', '2018-12-11 03:23:43'),
(1567, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-12-11 03:25:41', '2018-12-11 03:25:41'),
(1568, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-11 03:28:51', '2018-12-11 03:28:51'),
(1569, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-11 03:28:55', '2018-12-11 03:28:55'),
(1570, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-11 03:28:59', '2018-12-11 03:28:59'),
(1571, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-11 03:29:06', '2018-12-11 03:29:06'),
(1572, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-11 03:29:12', '2018-12-11 03:29:12'),
(1573, 1, 'admin/training_contents', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-11 03:29:14', '2018-12-11 03:29:14'),
(1574, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-11 03:29:15', '2018-12-11 03:29:15'),
(1575, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-11 03:29:22', '2018-12-11 03:29:22'),
(1576, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-11 03:37:14', '2018-12-11 03:37:14'),
(1577, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-11 03:37:35', '2018-12-11 03:37:35'),
(1578, 1, 'admin/training_contents', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-11 03:37:35', '2018-12-11 03:37:35'),
(1579, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-11 03:37:36', '2018-12-11 03:37:36'),
(1580, 1, 'admin/transactions', 'GET', '127.0.0.1', '[]', '2018-12-11 03:40:52', '2018-12-11 03:40:52'),
(1581, 1, 'admin/transactions', 'GET', '127.0.0.1', '[]', '2018-12-11 03:42:18', '2018-12-11 03:42:18'),
(1582, 1, 'admin/transactions', 'GET', '127.0.0.1', '[]', '2018-12-11 03:46:26', '2018-12-11 03:46:26'),
(1583, 1, 'admin/transactions', 'GET', '127.0.0.1', '[]', '2018-12-11 03:48:02', '2018-12-11 03:48:02'),
(1584, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_scope_\":\"new\",\"_pjax\":\"#pjax-container\"}', '2018-12-11 03:48:07', '2018-12-11 03:48:07'),
(1585, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-11 03:48:10', '2018-12-11 03:48:10'),
(1586, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"_scope_\":\"new\"}', '2018-12-11 03:48:13', '2018-12-11 03:48:13'),
(1587, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"_scope_\":\"new\"}', '2018-12-11 03:48:16', '2018-12-11 03:48:16'),
(1588, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"_scope_\":\"new\"}', '2018-12-11 03:48:19', '2018-12-11 03:48:19'),
(1589, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-11 03:48:25', '2018-12-11 03:48:25'),
(1590, 1, 'admin/transactions', 'GET', '127.0.0.1', '[]', '2018-12-11 03:49:38', '2018-12-11 03:49:38'),
(1591, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_scope_\":\"new\",\"_pjax\":\"#pjax-container\"}', '2018-12-11 03:49:42', '2018-12-11 03:49:42'),
(1592, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_scope_\":\"new\",\"_pjax\":\"#pjax-container\"}', '2018-12-11 03:49:44', '2018-12-11 03:49:44'),
(1593, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-11 03:49:48', '2018-12-11 03:49:48'),
(1594, 1, 'admin/transactions', 'GET', '127.0.0.1', '[]', '2018-12-11 03:52:00', '2018-12-11 03:52:00'),
(1595, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_scope_\":\"new\",\"_pjax\":\"#pjax-container\"}', '2018-12-11 03:52:06', '2018-12-11 03:52:06'),
(1596, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_scope_\":\"new\",\"_pjax\":\"#pjax-container\"}', '2018-12-11 03:52:09', '2018-12-11 03:52:09'),
(1597, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_scope_\":\"new\",\"_pjax\":\"#pjax-container\"}', '2018-12-11 03:52:12', '2018-12-11 03:52:12'),
(1598, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_scope_\":\"new\",\"_pjax\":\"#pjax-container\"}', '2018-12-11 03:52:14', '2018-12-11 03:52:14'),
(1599, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_scope_\":\"new\",\"_pjax\":\"#pjax-container\"}', '2018-12-11 03:52:15', '2018-12-11 03:52:15'),
(1600, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_scope_\":\"new\",\"_pjax\":\"#pjax-container\"}', '2018-12-11 03:52:17', '2018-12-11 03:52:17'),
(1601, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_scope_\":\"new\",\"_pjax\":\"#pjax-container\"}', '2018-12-11 03:52:18', '2018-12-11 03:52:18'),
(1602, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_scope_\":\"new\",\"_pjax\":\"#pjax-container\"}', '2018-12-11 03:52:22', '2018-12-11 03:52:22'),
(1603, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-11 03:52:24', '2018-12-11 03:52:24'),
(1604, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"_scope_\":\"new\"}', '2018-12-11 03:52:29', '2018-12-11 03:52:29'),
(1605, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"_scope_\":\"new\"}', '2018-12-11 03:52:31', '2018-12-11 03:52:31'),
(1606, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"_scope_\":\"new\"}', '2018-12-11 03:52:34', '2018-12-11 03:52:34'),
(1607, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-11 03:53:21', '2018-12-11 03:53:21'),
(1608, 1, 'admin/transactions', 'GET', '127.0.0.1', '[]', '2018-12-11 03:53:22', '2018-12-11 03:53:22'),
(1609, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_scope_\":\"new\",\"_pjax\":\"#pjax-container\"}', '2018-12-11 03:53:34', '2018-12-11 03:53:34'),
(1610, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_scope_\":\"new\",\"_pjax\":\"#pjax-container\"}', '2018-12-11 03:53:37', '2018-12-11 03:53:37'),
(1611, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_scope_\":\"new\",\"_pjax\":\"#pjax-container\"}', '2018-12-11 03:53:42', '2018-12-11 03:53:42'),
(1612, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-11 03:54:55', '2018-12-11 03:54:55'),
(1613, 1, 'admin/transactions', 'GET', '127.0.0.1', '[]', '2018-12-11 03:54:58', '2018-12-11 03:54:58'),
(1614, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_scope_\":\"new\",\"_pjax\":\"#pjax-container\"}', '2018-12-11 03:55:01', '2018-12-11 03:55:01'),
(1615, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-11 03:56:43', '2018-12-11 03:56:43'),
(1616, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"_scope_\":\"new\"}', '2018-12-11 03:56:46', '2018-12-11 03:56:46'),
(1617, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_scope_\":\"new\"}', '2018-12-11 03:58:46', '2018-12-11 03:58:46'),
(1618, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-11 03:58:51', '2018-12-11 03:58:51'),
(1619, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"_scope_\":\"new\"}', '2018-12-11 03:58:55', '2018-12-11 03:58:55'),
(1620, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"_scope_\":\"new\"}', '2018-12-11 03:58:57', '2018-12-11 03:58:57'),
(1621, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"_scope_\":\"new\"}', '2018-12-11 03:59:00', '2018-12-11 03:59:00'),
(1622, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"_scope_\":\"new\"}', '2018-12-11 03:59:01', '2018-12-11 03:59:01'),
(1623, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"_scope_\":\"new\"}', '2018-12-11 03:59:02', '2018-12-11 03:59:02'),
(1624, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"_scope_\":\"new\"}', '2018-12-11 03:59:04', '2018-12-11 03:59:04'),
(1625, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_scope_\":\"new\"}', '2018-12-11 03:59:52', '2018-12-11 03:59:52'),
(1626, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-11 03:59:55', '2018-12-11 03:59:55'),
(1627, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"_scope_\":\"year\"}', '2018-12-11 03:59:58', '2018-12-11 03:59:58'),
(1628, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"_scope_\":\"month\"}', '2018-12-11 04:00:00', '2018-12-11 04:00:00'),
(1629, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_scope_\":\"month\"}', '2018-12-11 04:00:37', '2018-12-11 04:00:37'),
(1630, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_scope_\":\"yesterday\",\"_pjax\":\"#pjax-container\"}', '2018-12-11 04:00:43', '2018-12-11 04:00:43'),
(1631, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_scope_\":\"yesterday\"}', '2018-12-11 04:02:50', '2018-12-11 04:02:50'),
(1632, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_scope_\":\"yesterday\"}', '2018-12-11 04:03:40', '2018-12-11 04:03:40'),
(1633, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_scope_\":\"yesterday\"}', '2018-12-11 04:04:10', '2018-12-11 04:04:10'),
(1634, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-11 04:04:46', '2018-12-11 04:04:46'),
(1635, 1, 'admin/transactions/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-11 04:04:50', '2018-12-11 04:04:50'),
(1636, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-11 04:05:14', '2018-12-11 04:05:14'),
(1637, 1, 'admin/training_contents', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-11 04:05:19', '2018-12-11 04:05:19'),
(1638, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-11 04:05:21', '2018-12-11 04:05:21'),
(1639, 1, 'admin/transactions', 'GET', '127.0.0.1', '[]', '2018-12-11 04:08:51', '2018-12-11 04:08:51'),
(1640, 1, 'admin/transactions', 'GET', '127.0.0.1', '[]', '2018-12-11 04:10:12', '2018-12-11 04:10:12'),
(1641, 1, 'admin/transactions', 'GET', '127.0.0.1', '[]', '2018-12-11 04:10:17', '2018-12-11 04:10:17'),
(1642, 1, 'admin/transactions', 'GET', '127.0.0.1', '[]', '2018-12-11 04:11:22', '2018-12-11 04:11:22'),
(1643, 1, 'admin/transactions', 'GET', '127.0.0.1', '[]', '2018-12-11 04:11:59', '2018-12-11 04:11:59'),
(1644, 1, 'admin/transactions', 'GET', '127.0.0.1', '[]', '2018-12-11 04:14:20', '2018-12-11 04:14:20'),
(1645, 1, 'admin/transactions', 'GET', '127.0.0.1', '[]', '2018-12-11 04:16:44', '2018-12-11 04:16:44'),
(1646, 1, 'admin/transactions', 'GET', '127.0.0.1', '[]', '2018-12-11 04:17:05', '2018-12-11 04:17:05'),
(1647, 1, 'admin/transactions', 'GET', '127.0.0.1', '[]', '2018-12-11 04:17:53', '2018-12-11 04:17:53'),
(1648, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"from_id\":\"19\",\"_pjax\":\"#pjax-container\"}', '2018-12-11 04:18:02', '2018-12-11 04:18:02'),
(1649, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"from_id\":\"19\"}', '2018-12-11 04:18:20', '2018-12-11 04:18:20'),
(1650, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"from_id\":\"19\",\"to_id\":\"20\",\"_pjax\":\"#pjax-container\"}', '2018-12-11 04:18:32', '2018-12-11 04:18:32'),
(1651, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"from_id\":\"19\",\"to_id\":\"20\",\"_pjax\":\"#pjax-container\"}', '2018-12-11 04:18:39', '2018-12-11 04:18:39'),
(1652, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-11 04:18:41', '2018-12-11 04:18:41'),
(1653, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"from_id\":\"20\",\"to_id\":null}', '2018-12-11 04:18:47', '2018-12-11 04:18:47'),
(1654, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-11 04:18:53', '2018-12-11 04:18:53'),
(1655, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"from_id\":null,\"to_id\":\"20\"}', '2018-12-11 04:18:58', '2018-12-11 04:18:58'),
(1656, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-11 04:19:03', '2018-12-11 04:19:03'),
(1657, 1, 'admin/transactions', 'GET', '127.0.0.1', '[]', '2018-12-11 04:20:32', '2018-12-11 04:20:32'),
(1658, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"from_id\":null,\"to_id\":null,\"status\":\"pending\",\"_pjax\":\"#pjax-container\"}', '2018-12-11 04:20:36', '2018-12-11 04:20:36'),
(1659, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"from_id\":null,\"to_id\":null,\"status\":\"accepted\"}', '2018-12-11 04:20:39', '2018-12-11 04:20:39'),
(1660, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"from_id\":null,\"to_id\":null,\"status\":\"rejected\"}', '2018-12-11 04:20:42', '2018-12-11 04:20:42'),
(1661, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-11 04:20:44', '2018-12-11 04:20:44'),
(1662, 1, 'admin/transactions', 'GET', '127.0.0.1', '[]', '2018-12-11 04:21:08', '2018-12-11 04:21:08'),
(1663, 1, 'admin/transactions', 'GET', '127.0.0.1', '[]', '2018-12-11 04:24:28', '2018-12-11 04:24:28'),
(1664, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"from_id\":null,\"to_id\":null,\"status\":\"accepted\",\"_pjax\":\"#pjax-container\"}', '2018-12-11 04:24:36', '2018-12-11 04:24:36'),
(1665, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"from_id\":null,\"to_id\":null,\"status\":\"rejected\"}', '2018-12-11 04:24:39', '2018-12-11 04:24:39'),
(1666, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"from_id\":null,\"to_id\":null,\"status\":\"pending\"}', '2018-12-11 04:24:42', '2018-12-11 04:24:42'),
(1667, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"from_id\":\"19\",\"to_id\":null,\"status\":\"pending\"}', '2018-12-11 04:24:50', '2018-12-11 04:24:50'),
(1668, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"from_id\":\"20\",\"to_id\":null,\"status\":\"pending\"}', '2018-12-11 04:24:53', '2018-12-11 04:24:53'),
(1669, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-11 04:24:56', '2018-12-11 04:24:56'),
(1670, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"from_id\":\"20\",\"to_id\":null,\"status\":null}', '2018-12-11 04:25:00', '2018-12-11 04:25:00'),
(1671, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"from_id\":\"20\",\"to_id\":\"19\",\"status\":null}', '2018-12-11 04:25:07', '2018-12-11 04:25:07'),
(1672, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-11 04:25:17', '2018-12-11 04:25:17'),
(1673, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-11 04:25:24', '2018-12-11 04:25:24'),
(1674, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-11 04:25:26', '2018-12-11 04:25:26'),
(1675, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-12-11 04:26:23', '2018-12-11 04:26:23'),
(1676, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-12-11 04:29:41', '2018-12-11 04:29:41'),
(1677, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-12-11 04:34:08', '2018-12-11 04:34:08'),
(1678, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-12-11 04:35:03', '2018-12-11 04:35:03'),
(1679, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-12-11 04:35:42', '2018-12-11 04:35:42'),
(1680, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-12-11 04:36:44', '2018-12-11 04:36:44'),
(1681, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-12-11 04:37:08', '2018-12-11 04:37:08'),
(1682, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-12-11 04:38:36', '2018-12-11 04:38:36'),
(1683, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-12-11 04:39:54', '2018-12-11 04:39:54'),
(1684, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-12-11 04:40:33', '2018-12-11 04:40:33'),
(1685, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-12-11 04:42:14', '2018-12-11 04:42:14'),
(1686, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-12-11 04:42:37', '2018-12-11 04:42:37'),
(1687, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-12-11 04:42:53', '2018-12-11 04:42:53'),
(1688, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-11 04:44:13', '2018-12-11 04:44:13'),
(1689, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-11 04:44:29', '2018-12-11 04:44:29'),
(1690, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-11 04:44:31', '2018-12-11 04:44:31'),
(1691, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-11 04:44:32', '2018-12-11 04:44:32'),
(1692, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-11 04:44:34', '2018-12-11 04:44:34'),
(1693, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-11 04:44:36', '2018-12-11 04:44:36'),
(1694, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-11 04:44:38', '2018-12-11 04:44:38'),
(1695, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-11 04:44:40', '2018-12-11 04:44:40'),
(1696, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-11 04:44:43', '2018-12-11 04:44:43'),
(1697, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-11 04:45:14', '2018-12-11 04:45:14'),
(1698, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-12-11 04:51:23', '2018-12-11 04:51:23'),
(1699, 1, 'admin/users', 'GET', '127.0.0.1', '{\"username\":\"19\",\"invest_amount\":{\"status\":null,\"package_id\":null},\"_pjax\":\"#pjax-container\"}', '2018-12-11 04:51:33', '2018-12-11 04:51:33'),
(1700, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-11 04:51:44', '2018-12-11 04:51:44'),
(1701, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"username\":\"20\",\"invest_amount\":{\"status\":null,\"package_id\":null}}', '2018-12-11 04:51:50', '2018-12-11 04:51:50'),
(1702, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-11 04:51:53', '2018-12-11 04:51:53'),
(1703, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-12-11 04:52:24', '2018-12-11 04:52:24'),
(1704, 1, 'admin/users', 'GET', '127.0.0.1', '{\"invest_amount\":{\"status\":\"paid\",\"package_id\":null},\"_pjax\":\"#pjax-container\"}', '2018-12-11 04:52:31', '2018-12-11 04:52:31'),
(1705, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"invest_amount\":{\"status\":\"pending\",\"package_id\":null}}', '2018-12-11 04:52:33', '2018-12-11 04:52:33'),
(1706, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"invest_amount\":{\"status\":\"paid\",\"package_id\":null}}', '2018-12-11 04:52:36', '2018-12-11 04:52:36'),
(1707, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-11 04:53:26', '2018-12-11 04:53:26'),
(1708, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"username\":\"anwar\",\"invest_amount\":{\"status\":null,\"package_id\":null}}', '2018-12-11 04:53:30', '2018-12-11 04:53:30'),
(1709, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-11 04:53:31', '2018-12-11 04:53:31'),
(1710, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"username\":null,\"invest_amount\":{\"status\":null,\"package_id\":\"1\"}}', '2018-12-11 04:53:36', '2018-12-11 04:53:36'),
(1711, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"username\":null,\"invest_amount\":{\"status\":null,\"package_id\":\"2\"}}', '2018-12-11 04:53:39', '2018-12-11 04:53:39'),
(1712, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"username\":null,\"invest_amount\":{\"status\":null,\"package_id\":\"3\"}}', '2018-12-11 04:53:41', '2018-12-11 04:53:41'),
(1713, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-11 04:53:45', '2018-12-11 04:53:45'),
(1714, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-11 04:53:48', '2018-12-11 04:53:48'),
(1715, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-12-11 05:14:34', '2018-12-11 05:14:34'),
(1716, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-11 05:14:38', '2018-12-11 05:14:38'),
(1717, 1, 'admin/packages/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-11 05:14:42', '2018-12-11 05:14:42'),
(1718, 1, 'admin/packages/1', 'PUT', '127.0.0.1', '{\"title\":\"Silver\",\"amount\":\"5000.00\",\"total_months\":\"36\",\"additional_returns\":\"$150 - $300\",\"referral_bonus\":{\"first_level\":\"200.00\",\"second_level\":\"50.00\",\"third_level\":\"20.00\"},\"_token\":\"2ISEPVbph6KR2GkdhL2FNVdPMySe0CgxsiF8uyq8\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/packages\"}', '2018-12-11 05:15:22', '2018-12-11 05:15:22'),
(1719, 1, 'admin/packages', 'GET', '127.0.0.1', '[]', '2018-12-11 05:15:22', '2018-12-11 05:15:22'),
(1720, 1, 'admin/packages', 'GET', '127.0.0.1', '[]', '2018-12-11 05:15:39', '2018-12-11 05:15:39'),
(1721, 1, 'admin/packages/2/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-11 05:15:41', '2018-12-11 05:15:41'),
(1722, 1, 'admin/packages/2', 'PUT', '127.0.0.1', '{\"title\":\"Gold\",\"amount\":\"10000.00\",\"total_months\":\"36\",\"additional_returns\":\"$300 - $600\",\"referral_bonus\":{\"first_level\":\"500.00\",\"second_level\":\"100.00\",\"third_level\":\"40.00\"},\"_token\":\"2ISEPVbph6KR2GkdhL2FNVdPMySe0CgxsiF8uyq8\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/packages\"}', '2018-12-11 05:15:52', '2018-12-11 05:15:52'),
(1723, 1, 'admin/packages', 'GET', '127.0.0.1', '[]', '2018-12-11 05:15:52', '2018-12-11 05:15:52'),
(1724, 1, 'admin/packages/3/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-11 05:15:54', '2018-12-11 05:15:54'),
(1725, 1, 'admin/packages/3', 'PUT', '127.0.0.1', '{\"title\":\"Diamond\",\"amount\":\"20000.00\",\"total_months\":\"36\",\"additional_returns\":\"$600 - $1200\",\"referral_bonus\":{\"first_level\":\"1100.00\",\"second_level\":\"250.00\",\"third_level\":\"100.00\"},\"_token\":\"2ISEPVbph6KR2GkdhL2FNVdPMySe0CgxsiF8uyq8\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/packages\"}', '2018-12-11 05:16:06', '2018-12-11 05:16:06'),
(1726, 1, 'admin/packages', 'GET', '127.0.0.1', '[]', '2018-12-11 05:16:06', '2018-12-11 05:16:06'),
(1727, 1, 'admin/packages', 'GET', '127.0.0.1', '[]', '2018-12-11 06:03:24', '2018-12-11 06:03:24'),
(1728, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-11 06:03:29', '2018-12-11 06:03:29'),
(1729, 1, 'admin/auth/menu', 'POST', '127.0.0.1', '{\"parent_id\":\"0\",\"title\":\"Supports\",\"icon\":\"fa-bars\",\"uri\":\"supports\",\"roles\":[\"1\",null],\"permission\":\"*\",\"_token\":\"2ISEPVbph6KR2GkdhL2FNVdPMySe0CgxsiF8uyq8\"}', '2018-12-11 06:03:47', '2018-12-11 06:03:47'),
(1730, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-12-11 06:03:47', '2018-12-11 06:03:47'),
(1731, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-12-11 06:03:50', '2018-12-11 06:03:50'),
(1732, 1, 'admin/supports', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-11 06:03:53', '2018-12-11 06:03:53'),
(1733, 1, 'admin/supports', 'GET', '127.0.0.1', '[]', '2018-12-11 06:04:29', '2018-12-11 06:04:29'),
(1734, 1, 'admin/supports', 'GET', '127.0.0.1', '[]', '2018-12-11 06:04:32', '2018-12-11 06:04:32'),
(1735, 1, 'admin/supports', 'GET', '127.0.0.1', '[]', '2018-12-11 06:04:36', '2018-12-11 06:04:36'),
(1736, 1, 'admin/supports', 'GET', '127.0.0.1', '[]', '2018-12-11 06:05:12', '2018-12-11 06:05:12'),
(1737, 1, 'admin/supports', 'GET', '127.0.0.1', '[]', '2018-12-11 06:05:40', '2018-12-11 06:05:40'),
(1738, 1, 'admin/supports', 'GET', '127.0.0.1', '[]', '2018-12-11 06:05:54', '2018-12-11 06:05:54'),
(1739, 1, 'admin/supports', 'GET', '127.0.0.1', '[]', '2018-12-11 06:06:12', '2018-12-11 06:06:12'),
(1740, 1, 'admin/supports', 'GET', '127.0.0.1', '[]', '2018-12-11 06:06:35', '2018-12-11 06:06:35'),
(1741, 1, 'admin/supports', 'GET', '127.0.0.1', '[]', '2018-12-11 06:07:15', '2018-12-11 06:07:15'),
(1742, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-12-12 06:48:20', '2018-12-12 06:48:20'),
(1743, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-12 06:48:26', '2018-12-12 06:48:26'),
(1744, 1, 'admin/users/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-12 06:48:29', '2018-12-12 06:48:29'),
(1745, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-12-12 08:09:55', '2018-12-12 08:09:55'),
(1746, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-12 08:09:58', '2018-12-12 08:09:58'),
(1747, 1, 'admin/packages/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-12 08:42:57', '2018-12-12 08:42:57'),
(1748, 1, 'admin/packages/1', 'PUT', '127.0.0.1', '{\"title\":\"Silver\",\"amount\":\"5000.00\",\"total_months\":\"36\",\"additional_returns\":\"$150 - $300\",\"referral_bonus\":{\"first_level\":\"200.00\",\"second_level\":\"50.00\",\"third_level\":\"20.00\"},\"_token\":\"k6fzQklfPsDnRvYx1mGmEef7ptXP75Kces6LC4W0\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/packages\"}', '2018-12-12 08:43:10', '2018-12-12 08:43:10'),
(1749, 1, 'admin/packages', 'GET', '127.0.0.1', '[]', '2018-12-12 08:43:11', '2018-12-12 08:43:11'),
(1750, 1, 'admin/packages/2/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-12 08:46:03', '2018-12-12 08:46:03'),
(1751, 1, 'admin/packages/2', 'PUT', '127.0.0.1', '{\"title\":\"Gold\",\"amount\":\"10000.00\",\"total_months\":\"36\",\"additional_returns\":\"$300 - $600\",\"referral_bonus\":{\"first_level\":\"500.00\",\"second_level\":\"100.00\",\"third_level\":\"40.00\"},\"_token\":\"k6fzQklfPsDnRvYx1mGmEef7ptXP75Kces6LC4W0\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/packages\"}', '2018-12-12 08:46:10', '2018-12-12 08:46:10'),
(1752, 1, 'admin/packages', 'GET', '127.0.0.1', '[]', '2018-12-12 08:46:11', '2018-12-12 08:46:11'),
(1753, 1, 'admin/packages/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-12 08:47:35', '2018-12-12 08:47:35'),
(1754, 1, 'admin/packages/1', 'PUT', '127.0.0.1', '{\"title\":\"Silver\",\"amount\":\"5000.00\",\"total_months\":\"36\",\"additional_returns\":\"$150 - $300\",\"referral_bonus\":{\"first_level\":\"200.00\",\"second_level\":\"50.00\",\"third_level\":\"20.00\"},\"_token\":\"k6fzQklfPsDnRvYx1mGmEef7ptXP75Kces6LC4W0\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/packages\"}', '2018-12-12 08:47:44', '2018-12-12 08:47:44'),
(1755, 1, 'admin/packages', 'GET', '127.0.0.1', '[]', '2018-12-12 08:47:44', '2018-12-12 08:47:44'),
(1756, 1, 'admin/packages/3/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-12 08:47:46', '2018-12-12 08:47:46');
INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(1757, 1, 'admin/packages/3', 'PUT', '127.0.0.1', '{\"title\":\"Diamond\",\"amount\":\"20000.00\",\"total_months\":\"36\",\"additional_returns\":\"$600 - $1200\",\"referral_bonus\":{\"first_level\":\"1100.00\",\"second_level\":\"250.00\",\"third_level\":\"100.00\"},\"_token\":\"k6fzQklfPsDnRvYx1mGmEef7ptXP75Kces6LC4W0\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/packages\"}', '2018-12-12 08:47:52', '2018-12-12 08:47:52'),
(1758, 1, 'admin/packages', 'GET', '127.0.0.1', '[]', '2018-12-12 08:47:52', '2018-12-12 08:47:52'),
(1759, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-12-13 03:10:25', '2018-12-13 03:10:25'),
(1760, 1, 'admin/auth/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-13 03:10:34', '2018-12-13 03:10:34'),
(1761, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-13 03:10:41', '2018-12-13 03:10:41'),
(1762, 1, 'admin/users/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-13 03:10:51', '2018-12-13 03:10:51'),
(1763, 1, 'admin/users', 'POST', '127.0.0.1', '{\"first_name\":\"Tahsin\",\"last_name\":\"Chowdhury\",\"username\":\"tahsin\",\"phone\":\"01672246318\",\"email\":\"tahsin@gmail.com\",\"password\":\"123456\",\"invest_amount\":{\"package_id\":\"1\",\"referral_code\":\"rono\",\"status\":\"pending\"},\"_token\":\"HIo0o2YXNVIgwSdEN8uW9OjjvPwWqUeKBa1Fvf1m\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/users\"}', '2018-12-13 03:11:38', '2018-12-13 03:11:38'),
(1764, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-12-13 03:11:39', '2018-12-13 03:11:39'),
(1765, 1, 'admin/users/25/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-13 03:11:48', '2018-12-13 03:11:48'),
(1766, 1, 'admin/users/25', 'PUT', '127.0.0.1', '{\"first_name\":\"Tahsin\",\"last_name\":\"Chowdhury\",\"phone\":\"01672246318\",\"password\":null,\"invest_amount\":{\"package_id\":\"1\",\"referral_code\":\"rono\",\"status\":\"paid\"},\"_token\":\"HIo0o2YXNVIgwSdEN8uW9OjjvPwWqUeKBa1Fvf1m\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/users\"}', '2018-12-13 03:11:52', '2018-12-13 03:11:52'),
(1767, 1, 'admin/users/25/edit', 'GET', '127.0.0.1', '[]', '2018-12-13 03:11:53', '2018-12-13 03:11:53'),
(1768, 1, 'admin/users/25', 'PUT', '127.0.0.1', '{\"first_name\":\"Tahsin\",\"last_name\":\"Chowdhury\",\"phone\":\"01672246318\",\"password\":\"123456\",\"invest_amount\":{\"package_id\":\"1\",\"referral_code\":\"rono\",\"status\":\"paid\"},\"_token\":\"HIo0o2YXNVIgwSdEN8uW9OjjvPwWqUeKBa1Fvf1m\",\"_method\":\"PUT\"}', '2018-12-13 03:12:00', '2018-12-13 03:12:00'),
(1769, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-12-13 03:12:02', '2018-12-13 03:12:02'),
(1770, 1, 'admin/users/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-13 03:22:39', '2018-12-13 03:22:39'),
(1771, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-13 03:26:46', '2018-12-13 03:26:46'),
(1772, 1, 'admin/users/26/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-13 03:26:52', '2018-12-13 03:26:52'),
(1773, 1, 'admin/users/26', 'PUT', '127.0.0.1', '{\"first_name\":\"Islam\",\"last_name\":\"tauhid\",\"phone\":null,\"password\":\"123456\",\"invest_amount\":{\"package_id\":\"2\",\"referral_code\":\"tahsin\",\"status\":\"paid\"},\"_token\":\"HIo0o2YXNVIgwSdEN8uW9OjjvPwWqUeKBa1Fvf1m\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/users\"}', '2018-12-13 03:27:01', '2018-12-13 03:27:01'),
(1774, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-12-13 03:27:02', '2018-12-13 03:27:02'),
(1775, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-13 03:28:04', '2018-12-13 03:28:04'),
(1776, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"page\":\"2\"}', '2018-12-13 03:28:15', '2018-12-13 03:28:15'),
(1777, 1, 'admin/transactions/41', 'PUT', '127.0.0.1', '{\"name\":\"status\",\"value\":\"accepted\",\"pk\":\"41\",\"_token\":\"HIo0o2YXNVIgwSdEN8uW9OjjvPwWqUeKBa1Fvf1m\",\"_editable\":\"1\",\"_method\":\"PUT\"}', '2018-12-13 03:28:25', '2018-12-13 03:28:25'),
(1778, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-13 03:30:53', '2018-12-13 03:30:53'),
(1779, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-13 03:30:59', '2018-12-13 03:30:59'),
(1780, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-12-13 11:21:00', '2018-12-13 11:21:00'),
(1781, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-13 11:21:16', '2018-12-13 11:21:16'),
(1782, 1, 'admin/users/19/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-13 11:21:24', '2018-12-13 11:21:24'),
(1783, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-13 11:21:43', '2018-12-13 11:21:43'),
(1784, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-13 11:21:49', '2018-12-13 11:21:49'),
(1785, 1, 'admin/users/19/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-13 11:21:56', '2018-12-13 11:21:56'),
(1786, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-13 11:24:34', '2018-12-13 11:24:34'),
(1787, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-13 11:24:36', '2018-12-13 11:24:36'),
(1788, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-13 11:26:16', '2018-12-13 11:26:16'),
(1789, 1, 'admin/users/19', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-13 11:26:23', '2018-12-13 11:26:23'),
(1790, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-13 11:26:27', '2018-12-13 11:26:27'),
(1791, 1, 'admin/users/19/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-13 11:26:29', '2018-12-13 11:26:29'),
(1792, 1, 'admin/supports', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-13 11:26:39', '2018-12-13 11:26:39'),
(1793, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-13 11:26:41', '2018-12-13 11:26:41'),
(1794, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-13 11:27:23', '2018-12-13 11:27:23'),
(1795, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-13 11:27:43', '2018-12-13 11:27:43'),
(1796, 1, 'admin/auth/logout', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-13 11:27:50', '2018-12-13 11:27:50'),
(1797, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-12-14 02:38:55', '2018-12-14 02:38:55'),
(1798, 1, 'admin/auth/logout', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-14 02:39:16', '2018-12-14 02:39:16'),
(1799, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-12-14 06:24:15', '2018-12-14 06:24:15'),
(1800, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-14 06:24:25', '2018-12-14 06:24:25'),
(1801, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-12-14 06:26:13', '2018-12-14 06:26:13'),
(1802, 1, 'admin/auth/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-14 06:26:34', '2018-12-14 06:26:34'),
(1803, 1, 'admin/auth/users/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-14 06:26:39', '2018-12-14 06:26:39'),
(1804, 1, 'admin/auth/roles', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-14 06:26:50', '2018-12-14 06:26:50'),
(1805, 1, 'admin/auth/roles/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-14 06:26:53', '2018-12-14 06:26:53'),
(1806, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-14 06:27:12', '2018-12-14 06:27:12'),
(1807, 1, 'admin/auth/menu/14/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-14 06:27:46', '2018-12-14 06:27:46'),
(1808, 1, 'admin/auth/menu/14', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-14 06:27:51', '2018-12-14 06:27:51'),
(1809, 1, 'admin/auth/menu/14/edit', 'GET', '127.0.0.1', '[]', '2018-12-14 06:27:51', '2018-12-14 06:27:51'),
(1810, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-14 06:28:15', '2018-12-14 06:28:15'),
(1811, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-14 06:28:25', '2018-12-14 06:28:25'),
(1812, 1, 'admin/auth/setting', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-14 06:28:39', '2018-12-14 06:28:39'),
(1813, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-14 06:28:50', '2018-12-14 06:28:50'),
(1814, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-14 06:28:53', '2018-12-14 06:28:53'),
(1815, 1, 'admin/users/27', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-14 06:29:06', '2018-12-14 06:29:06'),
(1816, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-14 06:29:10', '2018-12-14 06:29:10'),
(1817, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"_export_\":\"page:1\"}', '2018-12-14 06:29:17', '2018-12-14 06:29:17'),
(1818, 1, 'admin/users/27/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-14 06:29:39', '2018-12-14 06:29:39'),
(1819, 1, 'admin/users/27', 'PUT', '127.0.0.1', '{\"first_name\":\"Sumon\",\"last_name\":\"Sarker\",\"phone\":\"34534534532\",\"password\":null,\"invest_amount\":{\"package_id\":\"2\",\"referral_code\":\"rono\",\"status\":\"paid\"},\"_token\":\"7txEpBhZSc7zfrgsiDUwK2Ph4QMTfZ1MMsohPdEZ\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/users\"}', '2018-12-14 06:30:11', '2018-12-14 06:30:11'),
(1820, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-12-14 06:30:12', '2018-12-14 06:30:12'),
(1821, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-14 06:30:32', '2018-12-14 06:30:32'),
(1822, 1, 'admin/auth/setting', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-14 06:30:41', '2018-12-14 06:30:41'),
(1823, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-14 06:30:44', '2018-12-14 06:30:44'),
(1824, 1, 'admin/auth/setting', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-14 06:30:48', '2018-12-14 06:30:48'),
(1825, 1, 'admin/auth/permissions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-14 06:31:14', '2018-12-14 06:31:14'),
(1826, 1, 'admin/auth/permissions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-14 06:31:22', '2018-12-14 06:31:22'),
(1827, 1, 'admin/auth/permissions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"per_page\":\"30\"}', '2018-12-14 06:31:32', '2018-12-14 06:31:32'),
(1828, 1, 'admin/auth/permissions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"per_page\":\"10\"}', '2018-12-14 06:31:35', '2018-12-14 06:31:35'),
(1829, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-14 06:31:38', '2018-12-14 06:31:38'),
(1830, 1, 'admin/supports', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-14 06:31:40', '2018-12-14 06:31:40'),
(1831, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-14 06:34:53', '2018-12-14 06:34:53'),
(1832, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-12-14 06:35:00', '2018-12-14 06:35:00'),
(1833, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-12-14 06:36:51', '2018-12-14 06:36:51'),
(1834, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-12-14 06:39:34', '2018-12-14 06:39:34'),
(1835, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-14 06:40:59', '2018-12-14 06:40:59'),
(1836, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-12-14 06:44:05', '2018-12-14 06:44:05'),
(1837, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-12-14 06:44:55', '2018-12-14 06:44:55'),
(1838, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-14 06:45:01', '2018-12-14 06:45:01'),
(1839, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-14 06:45:04', '2018-12-14 06:45:04'),
(1840, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-14 06:45:08', '2018-12-14 06:45:08'),
(1841, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-14 06:45:10', '2018-12-14 06:45:10'),
(1842, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-14 06:45:13', '2018-12-14 06:45:13'),
(1843, 1, 'admin/auth/setting', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-14 06:46:49', '2018-12-14 06:46:49'),
(1844, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-14 06:46:58', '2018-12-14 06:46:58'),
(1845, 1, 'admin/users/19/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-14 06:47:01', '2018-12-14 06:47:01'),
(1846, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-14 06:49:39', '2018-12-14 06:49:39'),
(1847, 1, 'admin/users/19/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-14 06:50:19', '2018-12-14 06:50:19'),
(1848, 1, 'admin/users/19/edit', 'GET', '127.0.0.1', '[]', '2018-12-14 06:53:18', '2018-12-14 06:53:18'),
(1849, 1, 'admin/users/19', 'PUT', '127.0.0.1', '{\"first_name\":\"Anwar\",\"last_name\":\"Hussen\",\"phone\":\"01722566545\",\"password\":null,\"invest_amount\":{\"package_id\":\"1\",\"referral_code\":null,\"status\":\"paid\"},\"_token\":\"7txEpBhZSc7zfrgsiDUwK2Ph4QMTfZ1MMsohPdEZ\",\"_method\":\"PUT\"}', '2018-12-14 06:53:42', '2018-12-14 06:53:42'),
(1850, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-12-14 06:53:42', '2018-12-14 06:53:42'),
(1851, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-12-14 06:53:47', '2018-12-14 06:53:47'),
(1852, 1, 'admin/auth/setting', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-14 06:53:56', '2018-12-14 06:53:56'),
(1853, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-14 06:54:04', '2018-12-14 06:54:04'),
(1854, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-14 06:54:06', '2018-12-14 06:54:06'),
(1855, 1, 'admin/users/19/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-14 06:54:09', '2018-12-14 06:54:09'),
(1856, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-14 06:55:15', '2018-12-14 06:55:15'),
(1857, 1, 'admin/auth/setting', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-14 06:55:21', '2018-12-14 06:55:21'),
(1858, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-14 06:55:22', '2018-12-14 06:55:22'),
(1859, 1, 'admin/users/19', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-14 06:55:31', '2018-12-14 06:55:31'),
(1860, 1, 'admin/users/19', 'GET', '127.0.0.1', '[]', '2018-12-14 06:57:24', '2018-12-14 06:57:24'),
(1861, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-14 06:57:28', '2018-12-14 06:57:28'),
(1862, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-14 06:57:30', '2018-12-14 06:57:30'),
(1863, 1, 'admin/users/19', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-14 06:57:37', '2018-12-14 06:57:37'),
(1864, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-14 06:57:43', '2018-12-14 06:57:43'),
(1865, 1, 'admin/supports', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-14 06:57:48', '2018-12-14 06:57:48'),
(1866, 1, 'admin/supports/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-14 06:57:50', '2018-12-14 06:57:50'),
(1867, 1, 'admin/supports', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-14 06:58:18', '2018-12-14 06:58:18'),
(1868, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-14 06:58:22', '2018-12-14 06:58:22'),
(1869, 1, 'admin/auth/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-14 06:59:13', '2018-12-14 06:59:13'),
(1870, 1, 'admin/auth/roles', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-14 06:59:16', '2018-12-14 06:59:16'),
(1871, 1, 'admin/auth/permissions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-14 06:59:19', '2018-12-14 06:59:19'),
(1872, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-14 06:59:22', '2018-12-14 06:59:22'),
(1873, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-14 06:59:42', '2018-12-14 06:59:42'),
(1874, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-14 06:59:47', '2018-12-14 06:59:47'),
(1875, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-12-14 07:03:21', '2018-12-14 07:03:21'),
(1876, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-14 07:03:27', '2018-12-14 07:03:27'),
(1877, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-14 07:03:31', '2018-12-14 07:03:31'),
(1878, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-14 07:04:49', '2018-12-14 07:04:49'),
(1879, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"page\":\"2\"}', '2018-12-14 07:04:59', '2018-12-14 07:04:59'),
(1880, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"page\":\"3\"}', '2018-12-14 07:05:04', '2018-12-14 07:05:04'),
(1881, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"page\":\"1\"}', '2018-12-14 07:05:18', '2018-12-14 07:05:18'),
(1882, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"page\":\"1\",\"_scope_\":\"new\"}', '2018-12-14 07:05:30', '2018-12-14 07:05:30'),
(1883, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"page\":\"1\",\"_scope_\":\"yesterday\"}', '2018-12-14 07:05:33', '2018-12-14 07:05:33'),
(1884, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"page\":\"1\",\"_scope_\":\"week\"}', '2018-12-14 07:05:35', '2018-12-14 07:05:35'),
(1885, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"page\":\"1\",\"_scope_\":\"month\"}', '2018-12-14 07:05:38', '2018-12-14 07:05:38'),
(1886, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"page\":\"1\",\"_scope_\":\"year\"}', '2018-12-14 07:05:41', '2018-12-14 07:05:41'),
(1887, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"_scope_\":\"year\",\"from_id\":\"23\",\"to_id\":\"21\",\"status\":\"pending\"}', '2018-12-14 07:05:55', '2018-12-14 07:05:55'),
(1888, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"_scope_\":\"year\",\"from_id\":\"23\",\"to_id\":\"21\",\"status\":\"accepted\"}', '2018-12-14 07:06:06', '2018-12-14 07:06:06'),
(1889, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"_scope_\":\"year\",\"from_id\":\"20\",\"to_id\":\"19\",\"status\":\"pending\"}', '2018-12-14 07:06:13', '2018-12-14 07:06:13'),
(1890, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_scope_\":\"year\",\"from_id\":\"20\",\"to_id\":\"19\",\"status\":\"pending\",\"_pjax\":\"#pjax-container\"}', '2018-12-14 07:06:15', '2018-12-14 07:06:15'),
(1891, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_scope_\":\"year\",\"from_id\":\"20\",\"to_id\":\"19\",\"status\":\"pending\",\"_pjax\":\"#pjax-container\"}', '2018-12-14 07:06:21', '2018-12-14 07:06:21'),
(1892, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_scope_\":\"year\",\"from_id\":\"20\",\"to_id\":\"19\",\"status\":\"pending\",\"_pjax\":\"#pjax-container\"}', '2018-12-14 07:06:30', '2018-12-14 07:06:30'),
(1893, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_scope_\":\"year\",\"from_id\":\"20\",\"to_id\":\"19\",\"status\":\"pending\"}', '2018-12-14 07:06:42', '2018-12-14 07:06:42'),
(1894, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_scope_\":\"year\",\"from_id\":\"20\",\"to_id\":\"19\",\"status\":\"pending\",\"_pjax\":\"#pjax-container\"}', '2018-12-14 07:06:50', '2018-12-14 07:06:50'),
(1895, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_scope_\":\"year\",\"from_id\":\"20\",\"to_id\":\"19\",\"status\":\"pending\",\"_pjax\":\"#pjax-container\"}', '2018-12-14 07:07:11', '2018-12-14 07:07:11'),
(1896, 1, 'admin/transactions', 'GET', '127.0.0.1', '{\"_scope_\":\"year\",\"from_id\":\"20\",\"to_id\":\"19\",\"status\":\"pending\"}', '2018-12-14 07:07:35', '2018-12-14 07:07:35'),
(1897, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-14 07:07:48', '2018-12-14 07:07:48'),
(1898, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-14 07:07:59', '2018-12-14 07:07:59'),
(1899, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-14 07:10:24', '2018-12-14 07:10:24'),
(1900, 1, 'admin/auth/setting', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-14 07:10:32', '2018-12-14 07:10:32'),
(1901, 1, 'admin/auth/permissions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-14 07:10:36', '2018-12-14 07:10:36'),
(1902, 1, 'admin/auth/permissions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"per_page\":\"30\"}', '2018-12-14 07:10:41', '2018-12-14 07:10:41'),
(1903, 1, 'admin/auth/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-14 07:10:44', '2018-12-14 07:10:44'),
(1904, 1, 'admin/auth/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"per_page\":\"30\"}', '2018-12-14 07:10:47', '2018-12-14 07:10:47'),
(1905, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-14 07:10:49', '2018-12-14 07:10:49'),
(1906, 1, 'admin/news', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-14 07:10:51', '2018-12-14 07:10:51'),
(1907, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-14 07:10:55', '2018-12-14 07:10:55'),
(1908, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-14 07:11:09', '2018-12-14 07:11:09'),
(1909, 1, 'admin/packages', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-14 07:11:54', '2018-12-14 07:11:54'),
(1910, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-14 07:11:56', '2018-12-14 07:11:56'),
(1911, 1, 'admin/users/27/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-14 07:12:06', '2018-12-14 07:12:06'),
(1912, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-14 07:12:10', '2018-12-14 07:12:10'),
(1913, 1, 'admin/users/28/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-14 07:12:13', '2018-12-14 07:12:13'),
(1914, 1, 'admin/users/28', 'PUT', '127.0.0.1', '{\"first_name\":\"Sumon\",\"last_name\":\"Sarker\",\"phone\":\"77777777777\",\"password\":null,\"invest_amount\":{\"package_id\":\"1\",\"referral_code\":\"rono\",\"status\":\"pending\"},\"_token\":\"7txEpBhZSc7zfrgsiDUwK2Ph4QMTfZ1MMsohPdEZ\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/users\"}', '2018-12-14 07:12:41', '2018-12-14 07:12:41'),
(1915, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-12-14 07:12:42', '2018-12-14 07:12:42'),
(1916, 1, 'admin/users/27', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-14 07:12:59', '2018-12-14 07:12:59'),
(1917, 1, 'admin/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-14 07:13:16', '2018-12-14 07:13:16'),
(1918, 1, 'admin/additional_returns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-14 07:13:22', '2018-12-14 07:13:22'),
(1919, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-14 07:13:29', '2018-12-14 07:13:29'),
(1920, 1, 'admin/auth/roles', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-14 07:13:35', '2018-12-14 07:13:35');

-- --------------------------------------------------------

--
-- Table structure for table `admin_permissions`
--

CREATE TABLE `admin_permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `http_method` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `http_path` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_permissions`
--

INSERT INTO `admin_permissions` (`id`, `name`, `slug`, `http_method`, `http_path`, `created_at`, `updated_at`) VALUES
(1, 'All permission', '*', '', '*', NULL, NULL),
(2, 'Dashboard', 'dashboard', 'GET', '/', NULL, NULL),
(3, 'Login', 'auth.login', '', '/auth/login\r\n/auth/logout', NULL, NULL),
(4, 'User setting', 'auth.setting', 'GET,PUT', '/auth/setting', NULL, NULL),
(5, 'Auth management', 'auth.management', '', '/auth/roles\r\n/auth/permissions\r\n/auth/menu\r\n/auth/logs', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin_roles`
--

CREATE TABLE `admin_roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_roles`
--

INSERT INTO `admin_roles` (`id`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Administrator', 'administrator', '2018-11-16 09:54:39', '2018-11-16 09:54:39');

-- --------------------------------------------------------

--
-- Table structure for table `admin_role_menu`
--

CREATE TABLE `admin_role_menu` (
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_role_menu`
--

INSERT INTO `admin_role_menu` (`role_id`, `menu_id`, `created_at`, `updated_at`) VALUES
(1, 2, NULL, NULL),
(1, 8, NULL, NULL),
(1, 9, NULL, NULL),
(1, 10, NULL, NULL),
(1, 11, NULL, NULL),
(1, 12, NULL, NULL),
(1, 13, NULL, NULL),
(1, 14, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin_role_permissions`
--

CREATE TABLE `admin_role_permissions` (
  `role_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_role_permissions`
--

INSERT INTO `admin_role_permissions` (`role_id`, `permission_id`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin_role_users`
--

CREATE TABLE `admin_role_users` (
  `role_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_role_users`
--

INSERT INTO `admin_role_users` (`role_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin_users`
--

CREATE TABLE `admin_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_users`
--

INSERT INTO `admin_users` (`id`, `username`, `password`, `name`, `avatar`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', '$2y$10$AltpFjV4.kCgFp.3Qixn1OmKc0nRupHKYfmRqPy9w1gWSBa/KGsyq', 'Administrator', NULL, 'VmctRcEd7Abnsb7fSzktiDsyIy9LZp21g24hSGdjDzloFdk3u5srvAIukNmo', '2018-11-16 09:54:39', '2018-11-16 09:54:39');

-- --------------------------------------------------------

--
-- Table structure for table `admin_user_permissions`
--

CREATE TABLE `admin_user_permissions` (
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE `coupons` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `amount` decimal(8,2) NOT NULL DEFAULT '0.00',
  `start` date NOT NULL,
  `end` date NOT NULL,
  `note` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `coupons`
--

INSERT INTO `coupons` (`id`, `user_id`, `amount`, `start`, `end`, `note`, `created_at`, `updated_at`) VALUES
(2, 19, '100.00', '2018-12-09', '2019-01-08', 'Referral bonus from anwar3', '2018-12-09 00:22:32', '2018-12-09 00:22:32'),
(3, 20, '100.00', '2018-12-09', '2019-01-08', 'Received anwar', '2018-12-09 00:35:33', '2018-12-09 00:35:33'),
(4, 20, '100.00', '2018-12-10', '2019-01-09', 'Referral bonus from rono', '2018-12-10 03:35:09', '2018-12-10 03:35:09'),
(5, 19, '25.00', '2018-12-10', '2019-01-09', 'Referral bonus from rono', '2018-12-10 03:35:09', '2018-12-10 03:35:09'),
(7, 20, '50.00', '2018-12-10', '2019-01-09', 'Referral bonus from piya', '2018-12-10 03:40:23', '2018-12-10 03:40:23'),
(8, 19, '20.00', '2018-12-10', '2019-01-09', 'Referral bonus from piya', '2018-12-10 03:40:24', '2018-12-10 03:40:24'),
(11, 23, '250.00', '2018-12-10', '2019-01-09', 'Received rono', '2018-12-10 06:15:12', '2018-12-10 06:15:12'),
(12, 22, '550.00', '2018-12-10', '2019-01-09', 'Referral bonus from kharif007', '2018-12-10 06:34:47', '2018-12-10 06:34:47'),
(13, 20, '125.00', '2018-12-10', '2019-01-09', 'Referral bonus from kharif007', '2018-12-10 06:34:47', '2018-12-10 06:34:47'),
(14, 19, '50.00', '2018-12-10', '2019-01-09', 'Referral bonus from kharif007', '2018-12-10 06:34:47', '2018-12-10 06:34:47'),
(15, 22, '100.00', '2018-12-13', '2019-01-12', 'Referral bonus from tahsin', '2018-12-13 03:12:01', '2018-12-13 03:12:01'),
(16, 20, '25.00', '2018-12-13', '2019-01-12', 'Referral bonus from tahsin', '2018-12-13 03:12:02', '2018-12-13 03:12:02'),
(17, 19, '10.00', '2018-12-13', '2019-01-12', 'Referral bonus from tahsin', '2018-12-13 03:12:02', '2018-12-13 03:12:02'),
(19, 22, '50.00', '2018-12-13', '2019-01-12', 'Referral bonus from tauhid', '2018-12-13 03:27:01', '2018-12-13 03:27:01'),
(20, 20, '20.00', '2018-12-13', '2019-01-12', 'Referral bonus from tauhid', '2018-12-13 03:27:01', '2018-12-13 03:27:01'),
(21, 26, '250.00', '2018-12-13', '2019-01-12', 'Received tahsin', '2018-12-13 03:28:25', '2018-12-13 03:28:25'),
(22, 22, '250.00', '2018-12-14', '2019-01-13', 'Referral bonus from csesumonpro', '2018-12-14 06:30:12', '2018-12-14 06:30:12'),
(23, 20, '50.00', '2018-12-14', '2019-01-13', 'Referral bonus from csesumonpro', '2018-12-14 06:30:12', '2018-12-14 06:30:12'),
(24, 19, '20.00', '2018-12-14', '2019-01-13', 'Referral bonus from csesumonpro', '2018-12-14 06:30:12', '2018-12-14 06:30:12');

-- --------------------------------------------------------

--
-- Table structure for table `coupon_transactions`
--

CREATE TABLE `coupon_transactions` (
  `id` int(10) UNSIGNED NOT NULL,
  `amount` decimal(8,2) NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `approved_by` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `invest_amounts`
--

CREATE TABLE `invest_amounts` (
  `id` int(10) UNSIGNED NOT NULL,
  `package_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `referral_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `paid_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `invest_amounts`
--

INSERT INTO `invest_amounts` (`id`, `package_id`, `user_id`, `referral_code`, `status`, `paid_at`, `created_at`, `updated_at`) VALUES
(5, 1, 19, NULL, 'paid', NULL, '2018-12-09 00:17:45', '2018-12-09 00:17:45'),
(6, 1, 20, 'anwar', 'paid', NULL, '2018-12-09 00:20:07', '2018-12-09 00:20:07'),
(7, 1, 21, 'anwar', 'paid', NULL, '2018-12-09 00:21:04', '2018-12-09 00:22:32'),
(8, 1, 22, 'anwar2', 'paid', NULL, '2018-12-10 03:35:09', '2018-12-10 03:35:09'),
(9, 2, 23, 'rono', 'paid', NULL, '2018-12-10 03:35:57', '2018-12-10 03:40:23'),
(10, 3, 24, 'rono', 'paid', NULL, '2018-12-10 06:28:05', '2018-12-10 06:34:46'),
(11, 1, 25, 'rono', 'paid', NULL, '2018-12-13 03:11:39', '2018-12-13 03:12:00'),
(12, 2, 26, 'tahsin', 'paid', NULL, '2018-12-13 03:24:22', '2018-12-13 03:27:01'),
(13, 2, 27, 'rono', 'paid', NULL, '2018-12-13 09:57:49', '2018-12-14 06:30:11'),
(14, 1, 28, 'rono', 'pending', NULL, '2018-12-14 05:59:02', '2018-12-14 05:59:02');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(3, '2016_01_04_173148_create_admin_tables', 2),
(5, '2018_11_19_075408_create_packages_table', 3),
(6, '2014_10_12_000000_create_users_table', 4),
(7, '2014_10_12_100000_create_password_resets_table', 4),
(13, '2018_11_19_102932_create_coupons_table', 6),
(14, '2018_11_19_103934_create_tokens_table', 6),
(15, '2018_11_22_103606_create_token_transactions_table', 7),
(16, '2018_11_22_103632_create_coupon_transactions_table', 7),
(17, '2018_11_26_082128_create_news_table', 7),
(18, '2018_11_26_084529_create_training_contents_table', 7),
(19, '2018_11_27_083437_create_additional_returns_table', 8),
(20, '2018_12_05_064239_add_profile_verified_in_users_table', 9),
(25, '2018_12_05_072029_create_transactions_table', 10),
(26, '2018_12_05_100600_create_notifications_table', 10),
(27, '2018_11_19_085341_create_invest_amounts_table', 11),
(28, '2018_12_11_111015_add_badge_in_package_table', 12),
(29, '2018_12_11_115341_create_supports_table', 13);

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `title`, `content`, `created_at`, `updated_at`) VALUES
(1, 'What is Lorem Ipsum?', '<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', '2018-11-26 03:11:07', '2018-11-26 03:11:07'),
(2, 'Where does it come from?', '<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot; (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32.</p>\r\n\r\n<p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from &quot;de Finibus Bonorum et Malorum&quot; by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>', '2018-11-26 03:11:38', '2018-11-26 03:11:38'),
(3, 'New news from admin', '<p>hello this is a test news.</p>', '2018-12-10 05:45:40', '2018-12-10 05:45:40');

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_id` bigint(20) UNSIGNED NOT NULL,
  `data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `read_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `type`, `notifiable_type`, `notifiable_id`, `data`, `read_at`, `created_at`, `updated_at`) VALUES
('011855df-6769-4894-803c-4438936ad3a1', 'App\\Notifications\\NewTransaction', 'App\\Admin', 1, '{\"id\":39,\"type\":\"referral_in\",\"amount_type\":\"token\",\"amount\":\"20.00\",\"status\":\"accepted\",\"note\":\"Referral bonus from tauhid\",\"coupon_id\":null,\"from_id\":26,\"to_id\":20,\"created_at\":\"2018-12-13 09:27:01\",\"updated_at\":\"2018-12-13 09:27:01\"}', NULL, '2018-12-13 03:27:01', '2018-12-13 03:27:01'),
('025df95a-1fb5-448c-a986-10096cf38353', 'App\\Notifications\\NewTransaction', 'App\\Admin', 1, '{\"id\":29,\"type\":\"referral_in\",\"amount_type\":\"token\",\"amount\":\"100.00\",\"status\":\"accepted\",\"note\":\"Referral bonus from tahsin\",\"coupon_id\":null,\"from_id\":25,\"to_id\":22,\"created_at\":\"2018-12-13 09:12:01\",\"updated_at\":\"2018-12-13 09:12:01\"}', NULL, '2018-12-13 03:12:01', '2018-12-13 03:12:01'),
('02bfd11b-6416-40cc-a832-6876a2cc2b6c', 'App\\Notifications\\NewTransaction', 'App\\Admin', 1, '{\"id\":19,\"type\":\"referral_in\",\"amount_type\":\"coupon\",\"amount\":\"20.00\",\"status\":\"accepted\",\"note\":\"Referral bonus from piya\",\"coupon_id\":null,\"from_id\":23,\"to_id\":19,\"created_at\":\"2018-12-10 09:40:24\",\"updated_at\":\"2018-12-10 09:40:24\"}', NULL, '2018-12-10 03:40:24', '2018-12-10 03:40:24'),
('1e3aba79-a7f4-4aa2-931f-f1a77171a644', 'App\\Notifications\\NewTransaction', 'App\\Admin', 1, '{\"id\":44,\"type\":\"referral_in\",\"amount_type\":\"token\",\"amount\":\"50.00\",\"status\":\"accepted\",\"note\":\"Referral bonus from csesumonpro\",\"coupon_id\":null,\"from_id\":27,\"to_id\":20,\"created_at\":\"2018-12-14 12:30:12\",\"updated_at\":\"2018-12-14 12:30:12\"}', NULL, '2018-12-14 06:30:12', '2018-12-14 06:30:12'),
('1ffc9d61-1b6f-472c-9f39-b44680c892ac', 'App\\Notifications\\NewTransaction', 'App\\Admin', 1, '{\"id\":23,\"type\":\"referral_in\",\"amount_type\":\"token\",\"amount\":\"550.00\",\"status\":\"accepted\",\"note\":\"Referral bonus from kharif007\",\"coupon_id\":null,\"from_id\":24,\"to_id\":22,\"created_at\":\"2018-12-10 12:34:47\",\"updated_at\":\"2018-12-10 12:34:47\"}', NULL, '2018-12-10 06:34:47', '2018-12-10 06:34:47'),
('2067adfe-f697-4ed7-86cb-c17400b4bf93', 'App\\Notifications\\UpdatedTransaction', 'App\\User', 23, '{\"title\":\"Transaction status updated\",\"id\":20}', NULL, '2018-12-10 03:44:34', '2018-12-10 03:44:34'),
('23c10b60-ee39-4d38-a1c2-05e517201cf8', 'App\\Notifications\\NewTransaction', 'App\\Admin', 1, '{\"id\":31,\"type\":\"referral_in\",\"amount_type\":\"token\",\"amount\":\"25.00\",\"status\":\"accepted\",\"note\":\"Referral bonus from tahsin\",\"coupon_id\":null,\"from_id\":25,\"to_id\":20,\"created_at\":\"2018-12-13 09:12:02\",\"updated_at\":\"2018-12-13 09:12:02\"}', NULL, '2018-12-13 03:12:02', '2018-12-13 03:12:02'),
('2dfede0a-0cde-4e75-87a6-da600f5cda7f', 'App\\Notifications\\NewTransaction', 'App\\Admin', 1, '{\"id\":15,\"type\":\"referral_in\",\"amount_type\":\"coupon\",\"amount\":\"250.00\",\"status\":\"accepted\",\"note\":\"Referral bonus from piya\",\"coupon_id\":null,\"from_id\":23,\"to_id\":22,\"created_at\":\"2018-12-10 09:40:23\",\"updated_at\":\"2018-12-10 09:40:23\"}', NULL, '2018-12-10 03:40:23', '2018-12-10 03:40:23'),
('2fba5be9-4f88-42b1-b9db-f7d295a83271', 'App\\Notifications\\UpdatedTransaction', 'App\\User', 22, '{\"title\":\"Transaction status updated\",\"id\":20}', '2018-12-11 05:04:06', '2018-12-10 03:50:51', '2018-12-11 05:04:06'),
('40b00671-9caf-414b-b74a-8ef2f5aa3a2d', 'App\\Notifications\\NewTransaction', 'App\\Admin', 1, '{\"id\":46,\"type\":\"referral_in\",\"amount_type\":\"token\",\"amount\":\"20.00\",\"status\":\"accepted\",\"note\":\"Referral bonus from csesumonpro\",\"coupon_id\":null,\"from_id\":27,\"to_id\":19,\"created_at\":\"2018-12-14 12:30:12\",\"updated_at\":\"2018-12-14 12:30:12\"}', NULL, '2018-12-14 06:30:12', '2018-12-14 06:30:12'),
('41dd2661-b676-4e17-abbf-0187ea7d5008', 'App\\Notifications\\UpdatedTransaction', 'App\\User', 23, '{\"title\":\"Transaction status updated\",\"id\":21}', NULL, '2018-12-10 03:50:19', '2018-12-10 03:50:19'),
('47de9154-79e7-450c-8951-0499c8b7965e', 'App\\Notifications\\NewTransaction', 'App\\Admin', 1, '{\"id\":22,\"type\":\"gift\",\"amount_type\":\"coupon\",\"amount\":\"250.00\",\"status\":\"pending\",\"note\":\"Gift to piya\",\"coupon_id\":10,\"from_id\":22,\"to_id\":23,\"created_at\":\"2018-12-10 12:14:53\",\"updated_at\":\"2018-12-10 12:14:53\"}', NULL, '2018-12-10 06:14:54', '2018-12-10 06:14:54'),
('5b4556fb-4f57-4434-a67e-f29784985029', 'App\\Notifications\\NewTransaction', 'App\\Admin', 1, '{\"id\":43,\"type\":\"referral_in\",\"amount_type\":\"coupon\",\"amount\":\"250.00\",\"status\":\"accepted\",\"note\":\"Referral bonus from csesumonpro\",\"coupon_id\":null,\"from_id\":27,\"to_id\":22,\"created_at\":\"2018-12-14 12:30:12\",\"updated_at\":\"2018-12-14 12:30:12\"}', NULL, '2018-12-14 06:30:12', '2018-12-14 06:30:12'),
('5b7504bf-7dbc-494b-8dc1-3418c3711e09', 'App\\Notifications\\NewTransaction', 'App\\Admin', 1, '{\"id\":38,\"type\":\"referral_in\",\"amount_type\":\"coupon\",\"amount\":\"50.00\",\"status\":\"accepted\",\"note\":\"Referral bonus from tauhid\",\"coupon_id\":null,\"from_id\":26,\"to_id\":22,\"created_at\":\"2018-12-13 09:27:01\",\"updated_at\":\"2018-12-13 09:27:01\"}', NULL, '2018-12-13 03:27:01', '2018-12-13 03:27:01'),
('60765e3f-8c0c-4816-a484-9b6d5e7ca8a6', 'App\\Notifications\\UpdatedTransaction', 'App\\User', 22, '{\"title\":\"Transaction status updated\",\"id\":20}', '2018-12-11 05:04:06', '2018-12-10 03:44:30', '2018-12-11 05:04:06'),
('616e48e5-4f7b-4e2d-87b7-d116db1bc7ac', 'App\\Notifications\\NewTransaction', 'App\\Admin', 1, '{\"id\":35,\"type\":\"referral_in\",\"amount_type\":\"token\",\"amount\":\"250.00\",\"status\":\"accepted\",\"note\":\"Referral bonus from tauhid\",\"coupon_id\":null,\"from_id\":26,\"to_id\":25,\"created_at\":\"2018-12-13 09:27:01\",\"updated_at\":\"2018-12-13 09:27:01\"}', NULL, '2018-12-13 03:27:01', '2018-12-13 03:27:01'),
('736b8769-bb4e-40e9-a4d4-b138a8937bec', 'App\\Notifications\\UpdatedTransaction', 'App\\User', 26, '{\"title\":\"Transaction status updated\",\"id\":41}', NULL, '2018-12-13 03:28:28', '2018-12-13 03:28:28'),
('741e530a-3078-4a76-a77a-2bcec0b7a506', 'App\\Notifications\\UpdatedTransaction', 'App\\User', 25, '{\"title\":\"Transaction status updated\",\"id\":41}', NULL, '2018-12-13 03:28:25', '2018-12-13 03:28:25'),
('7bdb4527-2e03-4c05-b3b3-3d024b4f4327', 'App\\Notifications\\NewTransaction', 'App\\Admin', 1, '{\"id\":14,\"type\":\"referral_in\",\"amount_type\":\"token\",\"amount\":\"250.00\",\"status\":\"accepted\",\"note\":\"Referral bonus from piya\",\"coupon_id\":null,\"from_id\":23,\"to_id\":22,\"created_at\":\"2018-12-10 09:40:23\",\"updated_at\":\"2018-12-10 09:40:23\"}', NULL, '2018-12-10 03:40:23', '2018-12-10 03:40:23'),
('7eca860f-6053-4f34-86eb-39970ad6d932', 'App\\Notifications\\NewTransaction', 'App\\Admin', 1, '{\"id\":47,\"type\":\"referral_in\",\"amount_type\":\"coupon\",\"amount\":\"20.00\",\"status\":\"accepted\",\"note\":\"Referral bonus from csesumonpro\",\"coupon_id\":null,\"from_id\":27,\"to_id\":19,\"created_at\":\"2018-12-14 12:30:12\",\"updated_at\":\"2018-12-14 12:30:12\"}', NULL, '2018-12-14 06:30:12', '2018-12-14 06:30:12'),
('837226c9-aecf-4cd9-9d98-68708f688556', 'App\\Notifications\\NewTransaction', 'App\\Admin', 1, '{\"id\":30,\"type\":\"referral_in\",\"amount_type\":\"coupon\",\"amount\":\"100.00\",\"status\":\"accepted\",\"note\":\"Referral bonus from tahsin\",\"coupon_id\":null,\"from_id\":25,\"to_id\":22,\"created_at\":\"2018-12-13 09:12:01\",\"updated_at\":\"2018-12-13 09:12:01\"}', NULL, '2018-12-13 03:12:01', '2018-12-13 03:12:01'),
('843f34e9-8fa2-4408-b7a8-685ed96f6f50', 'App\\Notifications\\NewTransaction', 'App\\Admin', 1, '{\"id\":16,\"type\":\"referral_in\",\"amount_type\":\"token\",\"amount\":\"50.00\",\"status\":\"accepted\",\"note\":\"Referral bonus from piya\",\"coupon_id\":null,\"from_id\":23,\"to_id\":20,\"created_at\":\"2018-12-10 09:40:23\",\"updated_at\":\"2018-12-10 09:40:23\"}', NULL, '2018-12-10 03:40:23', '2018-12-10 03:40:23'),
('875104d0-143b-4699-8f00-55bfc3c77270', 'App\\Notifications\\NewTransaction', 'App\\Admin', 1, '{\"id\":37,\"type\":\"referral_in\",\"amount_type\":\"token\",\"amount\":\"50.00\",\"status\":\"accepted\",\"note\":\"Referral bonus from tauhid\",\"coupon_id\":null,\"from_id\":26,\"to_id\":22,\"created_at\":\"2018-12-13 09:27:01\",\"updated_at\":\"2018-12-13 09:27:01\"}', NULL, '2018-12-13 03:27:01', '2018-12-13 03:27:01'),
('87f6660a-98af-4b60-8abc-baa1e7dc7243', 'App\\Notifications\\NewTransaction', 'App\\Admin', 1, '{\"id\":13,\"type\":\"referral_in\",\"amount_type\":\"coupon\",\"amount\":\"25.00\",\"status\":\"accepted\",\"note\":\"Referral bonus from rono\",\"coupon_id\":null,\"from_id\":22,\"to_id\":19,\"created_at\":\"2018-12-10 09:35:09\",\"updated_at\":\"2018-12-10 09:35:09\"}', NULL, '2018-12-10 03:35:09', '2018-12-10 03:35:09'),
('8dce791d-7f95-4afc-bda6-07af9387a692', 'App\\Notifications\\NewTransaction', 'App\\Admin', 1, '{\"id\":12,\"type\":\"referral_in\",\"amount_type\":\"token\",\"amount\":\"25.00\",\"status\":\"accepted\",\"note\":\"Referral bonus from rono\",\"coupon_id\":null,\"from_id\":22,\"to_id\":19,\"created_at\":\"2018-12-10 09:35:09\",\"updated_at\":\"2018-12-10 09:35:09\"}', NULL, '2018-12-10 03:35:09', '2018-12-10 03:35:09'),
('8f84e651-fca6-47c7-ba55-efe47b30ab72', 'App\\Notifications\\NewTransaction', 'App\\Admin', 1, '{\"id\":34,\"type\":\"referral_in\",\"amount_type\":\"coupon\",\"amount\":\"10.00\",\"status\":\"accepted\",\"note\":\"Referral bonus from tahsin\",\"coupon_id\":null,\"from_id\":25,\"to_id\":19,\"created_at\":\"2018-12-13 09:12:02\",\"updated_at\":\"2018-12-13 09:12:02\"}', NULL, '2018-12-13 03:12:02', '2018-12-13 03:12:02'),
('90228261-c6a2-4c11-a89d-161021274521', 'App\\Notifications\\NewTransaction', 'App\\Admin', 1, '{\"id\":21,\"type\":\"gift\",\"amount_type\":\"coupon\",\"amount\":\"250.00\",\"status\":\"pending\",\"note\":\"Gift to rono\",\"coupon_id\":9,\"from_id\":23,\"to_id\":22,\"created_at\":\"2018-12-10 09:49:56\",\"updated_at\":\"2018-12-10 09:49:56\"}', NULL, '2018-12-10 03:49:56', '2018-12-10 03:49:56'),
('96f818a7-7fc2-4d2d-8d07-a5ec81983709', 'App\\Notifications\\NewTransaction', 'App\\Admin', 1, '{\"id\":36,\"type\":\"referral_in\",\"amount_type\":\"coupon\",\"amount\":\"250.00\",\"status\":\"accepted\",\"note\":\"Referral bonus from tauhid\",\"coupon_id\":null,\"from_id\":26,\"to_id\":25,\"created_at\":\"2018-12-13 09:27:01\",\"updated_at\":\"2018-12-13 09:27:01\"}', NULL, '2018-12-13 03:27:01', '2018-12-13 03:27:01'),
('a8e8ce86-3c87-4ba6-905a-1156db2a3efe', 'App\\Notifications\\NewTransaction', 'App\\Admin', 1, '{\"id\":24,\"type\":\"referral_in\",\"amount_type\":\"coupon\",\"amount\":\"550.00\",\"status\":\"accepted\",\"note\":\"Referral bonus from kharif007\",\"coupon_id\":null,\"from_id\":24,\"to_id\":22,\"created_at\":\"2018-12-10 12:34:47\",\"updated_at\":\"2018-12-10 12:34:47\"}', NULL, '2018-12-10 06:34:47', '2018-12-10 06:34:47'),
('acd93f0b-3912-4351-8298-cfe7819fbeea', 'App\\Notifications\\UpdatedTransaction', 'App\\User', 22, '{\"title\":\"Transaction status updated\",\"id\":22}', '2018-12-11 05:04:06', '2018-12-10 06:15:12', '2018-12-11 05:04:06'),
('af22357c-1f4f-42bb-b0a5-767a9beb38d7', 'App\\Notifications\\UpdatedTransaction', 'App\\User', 23, '{\"title\":\"Transaction status updated\",\"id\":20}', NULL, '2018-12-10 03:50:55', '2018-12-10 03:50:55'),
('b1b12ee5-dbb3-4e72-8238-41cf3223bb2e', 'App\\Notifications\\NewTransaction', 'App\\Admin', 1, '{\"id\":27,\"type\":\"referral_in\",\"amount_type\":\"token\",\"amount\":\"50.00\",\"status\":\"accepted\",\"note\":\"Referral bonus from kharif007\",\"coupon_id\":null,\"from_id\":24,\"to_id\":19,\"created_at\":\"2018-12-10 12:34:47\",\"updated_at\":\"2018-12-10 12:34:47\"}', NULL, '2018-12-10 06:34:47', '2018-12-10 06:34:47'),
('bbe2d26d-7763-4dfc-b191-9b6fa194e411', 'App\\Notifications\\NewTransaction', 'App\\Admin', 1, '{\"id\":11,\"type\":\"referral_in\",\"amount_type\":\"coupon\",\"amount\":\"100.00\",\"status\":\"accepted\",\"note\":\"Referral bonus from rono\",\"coupon_id\":null,\"from_id\":22,\"to_id\":20,\"created_at\":\"2018-12-10 09:35:09\",\"updated_at\":\"2018-12-10 09:35:09\"}', NULL, '2018-12-10 03:35:09', '2018-12-10 03:35:09'),
('c28cc3ba-449b-4231-a623-a82de3db581b', 'App\\Notifications\\NewTransaction', 'App\\Admin', 1, '{\"id\":18,\"type\":\"referral_in\",\"amount_type\":\"token\",\"amount\":\"20.00\",\"status\":\"accepted\",\"note\":\"Referral bonus from piya\",\"coupon_id\":null,\"from_id\":23,\"to_id\":19,\"created_at\":\"2018-12-10 09:40:24\",\"updated_at\":\"2018-12-10 09:40:24\"}', NULL, '2018-12-10 03:40:24', '2018-12-10 03:40:24'),
('c3a99413-7404-470e-bebb-974fa1aea964', 'App\\Notifications\\NewTransaction', 'App\\Admin', 1, '{\"id\":32,\"type\":\"referral_in\",\"amount_type\":\"coupon\",\"amount\":\"25.00\",\"status\":\"accepted\",\"note\":\"Referral bonus from tahsin\",\"coupon_id\":null,\"from_id\":25,\"to_id\":20,\"created_at\":\"2018-12-13 09:12:02\",\"updated_at\":\"2018-12-13 09:12:02\"}', NULL, '2018-12-13 03:12:02', '2018-12-13 03:12:02'),
('c64421e8-0100-430d-ab68-ddc09c03a71b', 'App\\Notifications\\NewTransaction', 'App\\Admin', 1, '{\"id\":45,\"type\":\"referral_in\",\"amount_type\":\"coupon\",\"amount\":\"50.00\",\"status\":\"accepted\",\"note\":\"Referral bonus from csesumonpro\",\"coupon_id\":null,\"from_id\":27,\"to_id\":20,\"created_at\":\"2018-12-14 12:30:12\",\"updated_at\":\"2018-12-14 12:30:12\"}', NULL, '2018-12-14 06:30:12', '2018-12-14 06:30:12'),
('cd4d68dc-3d99-4cdd-b1b3-57bcbdc5ac18', 'App\\Notifications\\NewTransaction', 'App\\Admin', 1, '{\"id\":20,\"type\":\"gift\",\"amount_type\":\"coupon\",\"amount\":\"250.00\",\"status\":\"pending\",\"note\":\"Gift to piya\",\"coupon_id\":6,\"from_id\":22,\"to_id\":23,\"created_at\":\"2018-12-10 09:42:58\",\"updated_at\":\"2018-12-10 09:42:58\"}', NULL, '2018-12-10 03:42:58', '2018-12-10 03:42:58'),
('d5066063-eebf-41e6-a076-e9a6a212cab5', 'App\\Notifications\\NewTransaction', 'App\\Admin', 1, '{\"id\":33,\"type\":\"referral_in\",\"amount_type\":\"token\",\"amount\":\"10.00\",\"status\":\"accepted\",\"note\":\"Referral bonus from tahsin\",\"coupon_id\":null,\"from_id\":25,\"to_id\":19,\"created_at\":\"2018-12-13 09:12:02\",\"updated_at\":\"2018-12-13 09:12:02\"}', NULL, '2018-12-13 03:12:02', '2018-12-13 03:12:02'),
('d5770cf9-6d7d-4155-8f7c-e5502f1c0957', 'App\\Notifications\\NewTransaction', 'App\\Admin', 1, '{\"id\":41,\"type\":\"gift\",\"amount_type\":\"coupon\",\"amount\":\"250.00\",\"status\":\"pending\",\"note\":\"Gift to tauhid\",\"coupon_id\":18,\"from_id\":25,\"to_id\":26,\"created_at\":\"2018-12-13 09:27:52\",\"updated_at\":\"2018-12-13 09:27:52\"}', NULL, '2018-12-13 03:27:52', '2018-12-13 03:27:52'),
('e84065a3-dcf7-4d48-9aa2-2fec84e1445b', 'App\\Notifications\\NewTransaction', 'App\\Admin', 1, '{\"id\":40,\"type\":\"referral_in\",\"amount_type\":\"coupon\",\"amount\":\"20.00\",\"status\":\"accepted\",\"note\":\"Referral bonus from tauhid\",\"coupon_id\":null,\"from_id\":26,\"to_id\":20,\"created_at\":\"2018-12-13 09:27:01\",\"updated_at\":\"2018-12-13 09:27:01\"}', NULL, '2018-12-13 03:27:01', '2018-12-13 03:27:01'),
('eb40825f-e63e-4428-b74f-67b7ca3101fc', 'App\\Notifications\\NewTransaction', 'App\\Admin', 1, '{\"id\":17,\"type\":\"referral_in\",\"amount_type\":\"coupon\",\"amount\":\"50.00\",\"status\":\"accepted\",\"note\":\"Referral bonus from piya\",\"coupon_id\":null,\"from_id\":23,\"to_id\":20,\"created_at\":\"2018-12-10 09:40:24\",\"updated_at\":\"2018-12-10 09:40:24\"}', NULL, '2018-12-10 03:40:24', '2018-12-10 03:40:24'),
('ed8d4374-2eda-4ef7-bfc8-10fe3ab8cad6', 'App\\Notifications\\NewTransaction', 'App\\Admin', 1, '{\"id\":28,\"type\":\"referral_in\",\"amount_type\":\"coupon\",\"amount\":\"50.00\",\"status\":\"accepted\",\"note\":\"Referral bonus from kharif007\",\"coupon_id\":null,\"from_id\":24,\"to_id\":19,\"created_at\":\"2018-12-10 12:34:47\",\"updated_at\":\"2018-12-10 12:34:47\"}', NULL, '2018-12-10 06:34:47', '2018-12-10 06:34:47'),
('efb576bb-cb26-47e8-962a-1e2e807e6bf7', 'App\\Notifications\\NewTransaction', 'App\\Admin', 1, '{\"id\":10,\"type\":\"referral_in\",\"amount_type\":\"token\",\"amount\":\"100.00\",\"status\":\"accepted\",\"note\":\"Referral bonus from rono\",\"coupon_id\":null,\"from_id\":22,\"to_id\":20,\"created_at\":\"2018-12-10 09:35:09\",\"updated_at\":\"2018-12-10 09:35:09\"}', NULL, '2018-12-10 03:35:09', '2018-12-10 03:35:09'),
('f13e8035-b293-4681-a222-31cd49595251', 'App\\Notifications\\NewTransaction', 'App\\Admin', 1, '{\"id\":42,\"type\":\"referral_in\",\"amount_type\":\"token\",\"amount\":\"250.00\",\"status\":\"accepted\",\"note\":\"Referral bonus from csesumonpro\",\"coupon_id\":null,\"from_id\":27,\"to_id\":22,\"created_at\":\"2018-12-14 12:30:11\",\"updated_at\":\"2018-12-14 12:30:11\"}', NULL, '2018-12-14 06:30:12', '2018-12-14 06:30:12'),
('fc1d0ed9-7819-4ff7-aa87-b09b6bc43ea5', 'App\\Notifications\\NewTransaction', 'App\\Admin', 1, '{\"id\":25,\"type\":\"referral_in\",\"amount_type\":\"token\",\"amount\":\"125.00\",\"status\":\"accepted\",\"note\":\"Referral bonus from kharif007\",\"coupon_id\":null,\"from_id\":24,\"to_id\":20,\"created_at\":\"2018-12-10 12:34:47\",\"updated_at\":\"2018-12-10 12:34:47\"}', NULL, '2018-12-10 06:34:47', '2018-12-10 06:34:47'),
('fc628ff3-3482-49b0-b5de-e853f2092486', 'App\\Notifications\\UpdatedTransaction', 'App\\User', 23, '{\"title\":\"Transaction status updated\",\"id\":22}', NULL, '2018-12-10 06:15:15', '2018-12-10 06:15:15'),
('ff012544-55fb-46dd-b363-e9e7b9fdb6aa', 'App\\Notifications\\UpdatedTransaction', 'App\\User', 22, '{\"title\":\"Transaction status updated\",\"id\":21}', '2018-12-11 05:04:06', '2018-12-10 03:50:23', '2018-12-11 05:04:06'),
('ff8bf4de-1d9c-42dc-b6d4-ba076d36a795', 'App\\Notifications\\NewTransaction', 'App\\Admin', 1, '{\"id\":26,\"type\":\"referral_in\",\"amount_type\":\"coupon\",\"amount\":\"125.00\",\"status\":\"accepted\",\"note\":\"Referral bonus from kharif007\",\"coupon_id\":null,\"from_id\":24,\"to_id\":20,\"created_at\":\"2018-12-10 12:34:47\",\"updated_at\":\"2018-12-10 12:34:47\"}', NULL, '2018-12-10 06:34:47', '2018-12-10 06:34:47');

-- --------------------------------------------------------

--
-- Table structure for table `packages`
--

CREATE TABLE `packages` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` decimal(8,2) UNSIGNED NOT NULL,
  `referral_bonus` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `total_months` int(10) UNSIGNED NOT NULL,
  `additional_returns` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `badge` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `packages`
--

INSERT INTO `packages` (`id`, `title`, `amount`, `referral_bonus`, `total_months`, `additional_returns`, `created_at`, `updated_at`, `badge`) VALUES
(1, 'Silver', '5000.00', '{\"first_level\":200,\"second_level\":50,\"third_level\":20}', 36, '$150 - $300', '2018-11-19 02:21:11', '2018-12-12 08:47:44', 'images/businessman2.png'),
(2, 'Gold', '10000.00', '{\"first_level\":500,\"second_level\":100,\"third_level\":40}', 36, '$300 - $600', '2018-11-19 02:22:23', '2018-12-12 08:46:11', 'images/businessman.png'),
(3, 'Diamond', '20000.00', '{\"first_level\":1100,\"second_level\":250,\"third_level\":100}', 36, '$600 - $1200', '2018-11-19 02:23:33', '2018-12-12 08:47:52', 'images/businessman3.png');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `supports`
--

CREATE TABLE `supports` (
  `id` int(10) UNSIGNED NOT NULL,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `supports`
--

INSERT INTO `supports` (`id`, `subject`, `description`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'payment', 'Palfdjlajdf lkjdfso  dfsjokj dfsaj dfsaakjoidf', 22, '2018-12-11 06:03:15', '2018-12-11 06:03:15');

-- --------------------------------------------------------

--
-- Table structure for table `tokens`
--

CREATE TABLE `tokens` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `balance` decimal(8,2) NOT NULL DEFAULT '0.00',
  `referral_bonus_earned` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tokens`
--

INSERT INTO `tokens` (`id`, `user_id`, `balance`, `referral_bonus_earned`, `created_at`, `updated_at`) VALUES
(17, 19, '325.00', 0, '2018-12-09 00:17:45', '2018-12-14 06:30:12'),
(18, 20, '370.00', 0, '2018-12-09 00:20:07', '2018-12-14 06:30:12'),
(19, 21, '0.00', 0, '2018-12-09 00:21:04', '2018-12-09 00:21:04'),
(20, 22, '1200.00', 0, '2018-12-10 03:35:09', '2018-12-14 06:30:11'),
(21, 23, '0.00', 0, '2018-12-10 03:35:57', '2018-12-10 03:35:57'),
(22, 24, '0.00', 0, '2018-12-10 06:28:05', '2018-12-10 06:28:05'),
(23, 25, '250.00', 0, '2018-12-13 03:11:39', '2018-12-13 03:27:01'),
(24, 26, '0.00', 0, '2018-12-13 03:24:22', '2018-12-13 03:24:22'),
(25, 27, '0.00', 0, '2018-12-13 09:57:50', '2018-12-13 09:57:50'),
(26, 28, '0.00', 0, '2018-12-14 05:59:02', '2018-12-14 05:59:02');

-- --------------------------------------------------------

--
-- Table structure for table `token_transactions`
--

CREATE TABLE `token_transactions` (
  `id` int(10) UNSIGNED NOT NULL,
  `amount` decimal(8,2) NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `approved_by` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `training_contents`
--

CREATE TABLE `training_contents` (
  `id` int(10) UNSIGNED NOT NULL,
  `caption` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `training_contents`
--

INSERT INTO `training_contents` (`id`, `caption`, `content`, `created_at`, `updated_at`) VALUES
(1, 'Where can I get some?', '<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don&#39;t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn&#39;t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>', '2018-11-26 03:13:11', '2018-11-26 03:13:11'),
(2, 'Why do we use it?', '<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>', '2018-11-26 03:13:31', '2018-11-26 03:13:31'),
(3, 'New training content', '<p>test content&nbsp;</p>', '2018-12-10 05:46:47', '2018-12-10 05:46:47');

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` decimal(8,2) NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `note` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `coupon_id` int(10) UNSIGNED DEFAULT NULL,
  `from_id` int(10) UNSIGNED DEFAULT NULL,
  `to_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`id`, `type`, `amount_type`, `amount`, `status`, `note`, `coupon_id`, `from_id`, `to_id`, `created_at`, `updated_at`) VALUES
(5, 'referral_in', 'token', '100.00', 'accepted', 'Referral bonus from anwar2', NULL, 20, 19, '2018-12-09 00:20:07', '2018-12-09 00:20:07'),
(6, 'referral_in', 'coupon', '100.00', 'accepted', 'Referral bonus from anwar2', NULL, 20, 19, '2018-12-09 00:20:08', '2018-12-09 00:20:08'),
(7, 'referral_in', 'token', '100.00', 'accepted', 'Referral bonus from anwar3', NULL, 21, 19, '2018-12-09 00:22:32', '2018-12-09 00:22:32'),
(8, 'referral_in', 'coupon', '100.00', 'accepted', 'Referral bonus from anwar3', NULL, 21, 19, '2018-12-09 00:22:32', '2018-12-09 00:22:32'),
(9, 'gift', 'coupon', '100.00', 'accepted', 'Gift to anwar2', 1, 19, 20, '2018-12-09 00:34:29', '2018-12-09 00:35:33'),
(10, 'referral_in', 'token', '100.00', 'accepted', 'Referral bonus from rono', NULL, 22, 20, '2018-12-10 03:35:09', '2018-12-10 03:35:09'),
(11, 'referral_in', 'coupon', '100.00', 'accepted', 'Referral bonus from rono', NULL, 22, 20, '2018-12-10 03:35:09', '2018-12-10 03:35:09'),
(12, 'referral_in', 'token', '25.00', 'accepted', 'Referral bonus from rono', NULL, 22, 19, '2018-12-10 03:35:09', '2018-12-10 03:35:09'),
(13, 'referral_in', 'coupon', '25.00', 'accepted', 'Referral bonus from rono', NULL, 22, 19, '2018-12-10 03:35:09', '2018-12-10 03:35:09'),
(14, 'referral_in', 'token', '250.00', 'accepted', 'Referral bonus from piya', NULL, 23, 22, '2018-12-10 03:40:23', '2018-12-10 03:40:23'),
(15, 'referral_in', 'coupon', '250.00', 'accepted', 'Referral bonus from piya', NULL, 23, 22, '2018-12-10 03:40:23', '2018-12-10 03:40:23'),
(16, 'referral_in', 'token', '50.00', 'accepted', 'Referral bonus from piya', NULL, 23, 20, '2018-12-10 03:40:23', '2018-12-10 03:40:23'),
(17, 'referral_in', 'coupon', '50.00', 'accepted', 'Referral bonus from piya', NULL, 23, 20, '2018-12-10 03:40:24', '2018-12-10 03:40:24'),
(18, 'referral_in', 'token', '20.00', 'accepted', 'Referral bonus from piya', NULL, 23, 19, '2018-12-10 03:40:24', '2018-12-10 03:40:24'),
(19, 'referral_in', 'coupon', '20.00', 'accepted', 'Referral bonus from piya', NULL, 23, 19, '2018-12-10 03:40:24', '2018-12-10 03:40:24'),
(20, 'gift', 'coupon', '250.00', 'rejected', 'Gift to piya', 6, 22, 23, '2018-12-10 03:42:58', '2018-12-10 03:50:51'),
(21, 'gift', 'coupon', '250.00', 'accepted', 'Gift to rono', 9, 23, 22, '2018-12-10 03:49:56', '2018-12-10 03:50:19'),
(22, 'gift', 'coupon', '250.00', 'accepted', 'Gift to piya', 10, 22, 23, '2018-12-10 06:14:53', '2018-12-10 06:15:12'),
(23, 'referral_in', 'token', '550.00', 'accepted', 'Referral bonus from kharif007', NULL, 24, 22, '2018-12-10 06:34:47', '2018-12-10 06:34:47'),
(24, 'referral_in', 'coupon', '550.00', 'accepted', 'Referral bonus from kharif007', NULL, 24, 22, '2018-12-10 06:34:47', '2018-12-10 06:34:47'),
(25, 'referral_in', 'token', '125.00', 'accepted', 'Referral bonus from kharif007', NULL, 24, 20, '2018-12-10 06:34:47', '2018-12-10 06:34:47'),
(26, 'referral_in', 'coupon', '125.00', 'accepted', 'Referral bonus from kharif007', NULL, 24, 20, '2018-12-10 06:34:47', '2018-12-10 06:34:47'),
(27, 'referral_in', 'token', '50.00', 'accepted', 'Referral bonus from kharif007', NULL, 24, 19, '2018-12-10 06:34:47', '2018-12-10 06:34:47'),
(28, 'referral_in', 'coupon', '50.00', 'accepted', 'Referral bonus from kharif007', NULL, 24, 19, '2018-12-10 06:34:47', '2018-12-10 06:34:47'),
(29, 'referral_in', 'token', '100.00', 'accepted', 'Referral bonus from tahsin', NULL, 25, 22, '2018-12-13 03:12:01', '2018-12-13 03:12:01'),
(30, 'referral_in', 'coupon', '100.00', 'accepted', 'Referral bonus from tahsin', NULL, 25, 22, '2018-12-13 03:12:01', '2018-12-13 03:12:01'),
(31, 'referral_in', 'token', '25.00', 'accepted', 'Referral bonus from tahsin', NULL, 25, 20, '2018-12-13 03:12:02', '2018-12-13 03:12:02'),
(32, 'referral_in', 'coupon', '25.00', 'accepted', 'Referral bonus from tahsin', NULL, 25, 20, '2018-12-13 03:12:02', '2018-12-13 03:12:02'),
(33, 'referral_in', 'token', '10.00', 'accepted', 'Referral bonus from tahsin', NULL, 25, 19, '2018-12-13 03:12:02', '2018-12-13 03:12:02'),
(34, 'referral_in', 'coupon', '10.00', 'accepted', 'Referral bonus from tahsin', NULL, 25, 19, '2018-12-13 03:12:02', '2018-12-13 03:12:02'),
(35, 'referral_in', 'token', '250.00', 'accepted', 'Referral bonus from tauhid', NULL, 26, 25, '2018-12-13 03:27:01', '2018-12-13 03:27:01'),
(36, 'referral_in', 'coupon', '250.00', 'accepted', 'Referral bonus from tauhid', NULL, 26, 25, '2018-12-13 03:27:01', '2018-12-13 03:27:01'),
(37, 'referral_in', 'token', '50.00', 'accepted', 'Referral bonus from tauhid', NULL, 26, 22, '2018-12-13 03:27:01', '2018-12-13 03:27:01'),
(38, 'referral_in', 'coupon', '50.00', 'accepted', 'Referral bonus from tauhid', NULL, 26, 22, '2018-12-13 03:27:01', '2018-12-13 03:27:01'),
(39, 'referral_in', 'token', '20.00', 'accepted', 'Referral bonus from tauhid', NULL, 26, 20, '2018-12-13 03:27:01', '2018-12-13 03:27:01'),
(40, 'referral_in', 'coupon', '20.00', 'accepted', 'Referral bonus from tauhid', NULL, 26, 20, '2018-12-13 03:27:01', '2018-12-13 03:27:01'),
(41, 'gift', 'coupon', '250.00', 'accepted', 'Gift to tauhid', 18, 25, 26, '2018-12-13 03:27:52', '2018-12-13 03:28:25'),
(42, 'referral_in', 'token', '250.00', 'accepted', 'Referral bonus from csesumonpro', NULL, 27, 22, '2018-12-14 06:30:11', '2018-12-14 06:30:11'),
(43, 'referral_in', 'coupon', '250.00', 'accepted', 'Referral bonus from csesumonpro', NULL, 27, 22, '2018-12-14 06:30:12', '2018-12-14 06:30:12'),
(44, 'referral_in', 'token', '50.00', 'accepted', 'Referral bonus from csesumonpro', NULL, 27, 20, '2018-12-14 06:30:12', '2018-12-14 06:30:12'),
(45, 'referral_in', 'coupon', '50.00', 'accepted', 'Referral bonus from csesumonpro', NULL, 27, 20, '2018-12-14 06:30:12', '2018-12-14 06:30:12'),
(46, 'referral_in', 'token', '20.00', 'accepted', 'Referral bonus from csesumonpro', NULL, 27, 19, '2018-12-14 06:30:12', '2018-12-14 06:30:12'),
(47, 'referral_in', 'coupon', '20.00', 'accepted', 'Referral bonus from csesumonpro', NULL, 27, 19, '2018-12-14 06:30:12', '2018-12-14 06:30:12');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `verification_type` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `verification_status` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `verification_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `profile_verified` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `username`, `phone`, `photo`, `email`, `verification_type`, `verification_status`, `verification_image`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `profile_verified`) VALUES
(19, 'Anwar', 'Hussen', 'anwar', '01722566545', 'images/cad14097bcf7db51a949f195688f86b0.png', 'anwar.hussen4@gmail.com', NULL, 'pending', NULL, '2018-12-09 00:18:37', NULL, NULL, '2018-12-09 00:17:45', '2018-12-14 06:53:42', NULL),
(20, 'Anwar', '2', 'anwar2', NULL, 'images/b59082180d802b7754c870d8754e7162.png', 'anwar2@gmail.com', NULL, 'pending', NULL, '2018-12-26 18:00:00', '$2y$10$L7xgeevRLLBvsLc6fqWwj.c2P0y/jWWcyVQ8C1qYoJ.Xzud87Gb7i', NULL, '2018-12-09 00:20:07', '2018-12-09 00:20:07', NULL),
(21, 'Anwar', '3', 'anwar3', NULL, 'images/755acac1ed23de2c3576f63f23c59a5d.png', 'anwar3@gmail.com', NULL, 'pending', NULL, '2018-12-03 18:00:00', '$2y$10$c3uRn64SMTaKi19FR0tFGOlpPYT2Q1EMe9uUDeiSPHk.LB1.dbnzW', NULL, '2018-12-09 00:21:04', '2018-12-09 00:22:32', NULL),
(22, 'Kamrul', 'hasnas', 'rono', '01672246318', 'images/ere-pass.jpg', 'dreamfighterr@gmail.com', NULL, 'pending', NULL, '2018-12-10 18:00:00', '$2y$10$gNlCKB4aYzCc.ZCCoyXzp.shCk3V06wCt9G0vts0kPS/DzbYFyPaC', 'TRJn29eyzYAUDXgBkNR1EfXhzDDuTS1osoxJHRq7y2G6g4qod1vdlbg3daNE', '2018-12-10 03:35:09', '2018-12-13 09:51:48', NULL),
(23, 'Piya', 'Chowdhury', 'piya', '01672246318', 'images/85641d0de899a7746d51b91f96e5a80b.png', 'piya@gmail.com', NULL, 'pending', NULL, '2018-12-17 18:00:00', '$2y$10$SvWu.RTQ9Edh.QrkpJhYI.o.5pxKr0nfkcTHhl/O6NozC21wRKSpi', 'B5wtrsR8E666Hn8EYlf2qdXo9bfkwYbJpwbMUNtovkjSYYPNqWglhtCMh46O', '2018-12-10 03:35:57', '2018-12-10 03:40:23', NULL),
(24, 'kamrul', 'arif', 'kharif007', NULL, 'images/ere-pass.jpg', 'kam.callcentre@gmail.com', NULL, 'pending', NULL, '2018-12-11 18:00:00', '$2y$10$taUFCAjw31IW3qSUA2cONOAoKMTSa0q20qguTziT6XiQMjJtP50F.', NULL, '2018-12-10 06:28:05', '2018-12-10 06:34:46', NULL),
(25, 'Tahsin', 'Chowdhury', 'tahsin', '01672246318', 'images/preview.jpg', 'tahsin@gmail.com', NULL, 'pending', NULL, '2018-12-10 18:00:00', '$2y$10$mVO7hQnlfn7EXv0n06Uapeic04cgJT8/eWUc.wx3Ll2ibIIpKhLXC', NULL, '2018-12-13 03:11:39', '2018-12-13 03:12:00', NULL),
(26, 'Islam', 'tauhid', 'tauhid', NULL, NULL, 'tauhid@gmail.com', NULL, 'pending', NULL, '2018-12-12 18:00:00', '$2y$10$KcKdeh36k//pUK.o1sJpA.P22AJvl6/l3CE3.6Ed.i/hlfSZCoG1q', 's2aHGRF9Wv9x7mTJS57lSm5CuNjKs831V3MSbpAfNimFrllnFY3tbs6WTIRm', '2018-12-13 03:24:22', '2018-12-13 03:27:01', NULL),
(27, 'Sumon', 'Sarker', 'csesumonpro', '34534534532', 'images/1544718662_png', 'csesumonpro@gmail.com', 'national_passport', 'rejected', 'images/1544788553_png', '2018-12-12 18:00:00', NULL, 'pMzq4uPW43iE8eEVPNfiCBXuewFYXPwWiPmyCi662MrMEAzm3y1B0YaC73Xd', '2018-12-13 09:57:49', '2018-12-14 06:30:11', NULL),
(28, 'Sumon', 'Sarker', 'test', '77777777777', 'images/Capture.PNG', 'test@gmail.com', 'international_passport', 'pending', 'images/1544789416_PNG', '2018-12-13 18:00:00', NULL, 'Vbzn9yXXDzhTClxWwzbFnW1E6L3uz56sVi4GB2SayMqddTInbu68BEpC4Epl', '2018-12-14 05:59:02', '2018-12-14 07:12:41', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `additional_returns`
--
ALTER TABLE `additional_returns`
  ADD PRIMARY KEY (`id`),
  ADD KEY `additional_returns_user_id_foreign` (`user_id`);

--
-- Indexes for table `admin_menu`
--
ALTER TABLE `admin_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_operation_log`
--
ALTER TABLE `admin_operation_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `admin_operation_log_user_id_index` (`user_id`);

--
-- Indexes for table `admin_permissions`
--
ALTER TABLE `admin_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admin_permissions_name_unique` (`name`);

--
-- Indexes for table `admin_roles`
--
ALTER TABLE `admin_roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admin_roles_name_unique` (`name`);

--
-- Indexes for table `admin_role_menu`
--
ALTER TABLE `admin_role_menu`
  ADD KEY `admin_role_menu_role_id_menu_id_index` (`role_id`,`menu_id`);

--
-- Indexes for table `admin_role_permissions`
--
ALTER TABLE `admin_role_permissions`
  ADD KEY `admin_role_permissions_role_id_permission_id_index` (`role_id`,`permission_id`);

--
-- Indexes for table `admin_role_users`
--
ALTER TABLE `admin_role_users`
  ADD KEY `admin_role_users_role_id_user_id_index` (`role_id`,`user_id`);

--
-- Indexes for table `admin_users`
--
ALTER TABLE `admin_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admin_users_username_unique` (`username`);

--
-- Indexes for table `admin_user_permissions`
--
ALTER TABLE `admin_user_permissions`
  ADD KEY `admin_user_permissions_user_id_permission_id_index` (`user_id`,`permission_id`);

--
-- Indexes for table `coupons`
--
ALTER TABLE `coupons`
  ADD PRIMARY KEY (`id`),
  ADD KEY `coupons_user_id_foreign` (`user_id`);

--
-- Indexes for table `coupon_transactions`
--
ALTER TABLE `coupon_transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invest_amounts`
--
ALTER TABLE `invest_amounts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `invest_amounts_user_id_foreign` (`user_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notifications_notifiable_type_notifiable_id_index` (`notifiable_type`(191),`notifiable_id`);

--
-- Indexes for table `packages`
--
ALTER TABLE `packages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `packages_title_unique` (`title`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`(191));

--
-- Indexes for table `supports`
--
ALTER TABLE `supports`
  ADD PRIMARY KEY (`id`),
  ADD KEY `supports_user_id_foreign` (`user_id`);

--
-- Indexes for table `tokens`
--
ALTER TABLE `tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tokens_user_id_foreign` (`user_id`);

--
-- Indexes for table `token_transactions`
--
ALTER TABLE `token_transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `training_contents`
--
ALTER TABLE `training_contents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_username_unique` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `additional_returns`
--
ALTER TABLE `additional_returns`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `admin_menu`
--
ALTER TABLE `admin_menu`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `admin_operation_log`
--
ALTER TABLE `admin_operation_log`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1921;

--
-- AUTO_INCREMENT for table `admin_permissions`
--
ALTER TABLE `admin_permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `admin_roles`
--
ALTER TABLE `admin_roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `admin_users`
--
ALTER TABLE `admin_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `coupons`
--
ALTER TABLE `coupons`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `coupon_transactions`
--
ALTER TABLE `coupon_transactions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `invest_amounts`
--
ALTER TABLE `invest_amounts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `packages`
--
ALTER TABLE `packages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `supports`
--
ALTER TABLE `supports`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tokens`
--
ALTER TABLE `tokens`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `token_transactions`
--
ALTER TABLE `token_transactions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `training_contents`
--
ALTER TABLE `training_contents`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `additional_returns`
--
ALTER TABLE `additional_returns`
  ADD CONSTRAINT `additional_returns_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `coupons`
--
ALTER TABLE `coupons`
  ADD CONSTRAINT `coupons_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `invest_amounts`
--
ALTER TABLE `invest_amounts`
  ADD CONSTRAINT `invest_amounts_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `supports`
--
ALTER TABLE `supports`
  ADD CONSTRAINT `supports_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `tokens`
--
ALTER TABLE `tokens`
  ADD CONSTRAINT `tokens_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

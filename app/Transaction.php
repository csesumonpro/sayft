<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{

    protected $fillable = ['type', 'amount_type', 'amount', 'status', 'note', 'from_id', 'to_id'];

    public function to_user()
    {
        return $this->belongsTo(User::class, 'to_id');
    }

    public function from_user()
    {
        return $this->belongsTo(User::class, 'from_id');
    }

    public function coupon()
    {
        return $this->belongsTo(Coupon::class);
    }
}

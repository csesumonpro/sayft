<?php

namespace App\Admin\Controllers;

use App\Http\Controllers\Controller;
use App\Transaction;
use App\User;
use Carbon\Carbon;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class TransactionController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Transactions')
            ->description('all transactions')
            ->body($this->grid());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Transaction);

        $grid->id('Id');
        $grid->type('Type');
        $grid->amount_type('Amount type');
        $grid->amount('Amount');

        //TODO: status disable if accepted
        $grid->status('Status')->editable('select', ['pending' => 'Pending', 'accepted' => 'Accepted', 'rejected' => 'Rejected']);

        $grid->column('from_user.username', 'From');
        $grid->column('to_user.username', 'To');
        $grid->column('created_at', 'Created at')->display(function ($created_at) {
            return Carbon::parse($created_at)->format('d/m/y \a\t h:m a');
        });

        $grid->column('updated_at', 'Updated at')->display(function ($created_at) {
            return Carbon::parse($created_at)->format('d/m/y \a\t h:m a');
        });

        $grid->filter(function ($filter) {

            // Remove the default id filter
            $filter->disableIdFilter();

            // Add a column filter
            $filter->equal('from_id', 'From')->select(User::all()->pluck('username', 'id'));
            $filter->equal('to_id', 'To')->select(User::all()->pluck('username', 'id'));
            $filter->equal('status', 'Status')->select(['pending' => 'Pending', 'accepted' => 'Accepted', 'rejected' => 'Rejected']);

            // Multiple conditional query
            $filter->scope('new', 'Today')
                ->whereDate('created_at', Carbon::today())
                ->orWhereDate('updated_at', Carbon::today());

            // Multiple conditional query
            $filter->scope('yesterday', 'Yesterday')
                ->whereDate('created_at', Carbon::yesterday())
                ->orWhereDate('updated_at', Carbon::yesterday());

            // Multiple conditional query
            $filter->scope('week', 'This Week')
                ->whereBetween('created_at', [Carbon::today()->startOfWeek(), Carbon::today()->endOfWeek()])
                ->orWhereBetween('updated_at', [Carbon::today()->startOfWeek(), Carbon::today()->endOfWeek()]);

            // Multiple conditional query
            $filter->scope('month', 'This Month')
                ->whereBetween('created_at', [Carbon::today()->startOfMonth(), Carbon::today()->endOfMonth()])
                ->orWhereBetween('updated_at', [Carbon::today()->startOfMonth(), Carbon::today()->endOfMonth()]);

            // Multiple conditional query
            $filter->scope('year', 'This Year')
                ->whereBetween('created_at', [Carbon::today()->startOfYear(), Carbon::today()->endOfYear()])
                ->orWhereBetween('updated_at', [Carbon::today()->startOfYear(), Carbon::today()->endOfYear()]);

        });


        return $grid;
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Transaction::findOrFail($id));

        $show->id('Id');
        $show->type('Type');
        $show->amount('Amount');
        $show->status('Status');
        $show->from_id('From id');
        $show->to_id('To id');
        $show->created_at('Created at');
        $show->updated_at('Updated at');

        return $show;
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Transaction);

        $form->select('type', 'Type')->options(['withdraw' => 'Withdraw', 'gift' => 'Gift', 'in' => 'IN'])->required();
        $form->select('amount_type', 'Amount Type')->options(['token' => 'Token', 'coupon' => 'Coupon'])->required();
        $form->decimal('amount', 'Amount')->required();
        $form->select('status', 'Status')->options(['pending' => 'Pending', 'accepted' => 'Accepted', 'rejected' => 'Rejected', 'cancel' => 'Cancel'])->required();
        $form->select('from_id', 'From')->options(User::all()->pluck('username', 'id'))->required();
        $form->select('to_id', 'To')->options(User::all()->pluck('username', 'id'));
        $form->textarea('note', 'Note')->placeholder('Note')->required();
        return $form;
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }
}

<?php

namespace App\Admin\Controllers;

use App\Http\Controllers\Controller;
use App\Package;
use App\User;
use Carbon\Carbon;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class InvestorController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Investors')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new User);

        $grid->column('Name')->display(function () {
            return $this->first_name . ' ' . $this->last_name;
        });

        $grid->username('Username');
        $grid->email('Email');
        $grid->phone('Phone');
        $grid->profile_verified('Verified')->editable('select', ['' => 'No', 'verified' => 'Verified', 'fraud' => 'Fraud']);
        $grid->photo('Photo')->image(asset('storage'), 50, 50);
        $grid->column('invest_amount.package_id', 'Invest Amount')->display(function ($package) {
            $package = Package::find($package);
            return '$' . $package->amount . " <span class='label sm label-primary'>{$package->title}</span>";
        });

        $grid->column('invest_amount.status', 'Status')->display(function ($status) {
            $label = $status == 'pending' ? 'warning' : 'success';
            return "<span class=\"label sm label-{$label}\">{$status}</span>";
        });

        $grid->token()->balance('Tokens');
        $grid->column('Coupons')->display(function () {
            $amount = $this->coupons->sum('amount');
            return sprintf('%0.2f', $amount);
        });

        $grid->column('created_at', 'Joined at')->display(function ($created_at) {
            return Carbon::parse($created_at)->format('d/m/y \a\t h:m a');
        });

        $grid->column('updated_at', 'Updated at')->display(function ($created_at) {
            return Carbon::parse($created_at)->format('d/m/y \a\t h:m a');
        });

        $grid->filter(function ($filter) {

            // Remove the default id filter
            $filter->disableIdFilter();

            // Add a column filter
            $filter->equal('username', 'Username')->select(User::all()->pluck('username', 'username'));
            $filter->equal('invest_amount.status', 'Status')->select(['paid' => 'Paid', 'pending' => 'Pending']);
            $filter->equal('invest_amount.package_id', 'Package')->select(Package::all()->pluck('title', 'id'));

            // Multiple conditional query
            $filter->scope('new', 'Today')
                ->whereDate('created_at', Carbon::today())
                ->orWhereDate('updated_at', Carbon::today());

            // Multiple conditional query
            $filter->scope('yesterday', 'Yesterday')
                ->whereDate('created_at', Carbon::yesterday())
                ->orWhereDate('updated_at', Carbon::yesterday());

            // Multiple conditional query
            $filter->scope('week', 'This Week')
                ->whereBetween('created_at', [Carbon::today()->startOfWeek(), Carbon::today()->endOfWeek()])
                ->orWhereBetween('updated_at', [Carbon::today()->startOfWeek(), Carbon::today()->endOfWeek()]);

            // Multiple conditional query
            $filter->scope('month', 'This Month')
                ->whereBetween('created_at', [Carbon::today()->startOfMonth(), Carbon::today()->endOfMonth()])
                ->orWhereBetween('updated_at', [Carbon::today()->startOfMonth(), Carbon::today()->endOfMonth()]);

            // Multiple conditional query
            $filter->scope('year', 'This Year')
                ->whereBetween('created_at', [Carbon::today()->startOfYear(), Carbon::today()->endOfYear()])
                ->orWhereBetween('updated_at', [Carbon::today()->startOfYear(), Carbon::today()->endOfYear()]);

        });

        return $grid;
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(User::findOrFail($id));

        $show->id('Id');
        $show->first_name('First name');
        $show->last_name('Last name');
        $show->username('Username');
        $show->phone('Phone');
        $show->photo('Photo');
        $show->email('Email');
        $show->email_verified_at('Email verified at');
        $show->created_at('Created at');
        $show->updated_at('Updated at');

        return $show;
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        $user = User::find($id);
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form($user)->edit($id));
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form(User $user = null)
    {
        $form = new Form(new User);

        $form->tab('Investor Information', function (Form $form) use ($user) {

            $form->text('first_name', 'First Name')->placeholder('eg. Jone')->required();
            $form->text('last_name', 'Last Name')->placeholder('eg. Doe')->required();
            if ($user) {
                $form->text('username', 'Username')->placeholder('jonedoe1')->required()->disable();
            } else {
                $form->text('username', 'Username')->placeholder('jonedoe1')->required()->rules('required|unique:users');
            }
            $form->mobile('phone', 'Phone')->placeholder('Mobile');
            $form->image('photo', 'Photo');
            if ($user) {
                $form->email('email', 'Email')->placeholder('jond.deo@domain.com')->required()->disable();
            } else {
                $form->email('email', 'Email')->placeholder('jond.deo@domain.com')->rules('unique:users')->required();
            }
            $form->password('password', 'Password')->placeholder('password');

        }, true);

        $form->tab('Package Selection', function (Form $form) {
            $form->select('invest_amount.package_id', 'Select Package')->options(Package::all()->pluck('title', 'id'))->required();
            $form->text('invest_amount.referral_code', 'Referral Code')->placeholder('eg. jonedoe01');
            $form->select('invest_amount.status', 'Status')->options([
                'pending' => 'Pending',
                'paid' => 'Paid'
            ]);
        });

        $form->saving(function (Form $form) {
            if ($form->password != null) {
                $form->password = bcrypt($form->password);
            }
        });

        /*    $form->saved(function (Form $form) {
                $id = $form->model()->id;
                $user = User::find($id);

                $invest_amount = $user->invest_amount;

                $user->token()->save(new Token());

                if ($invest_amount->status == 'paid') {
                    $form->model()->paid_at = Carbon::today();
                }

                if ($invest_amount->status == 'paid' && $invest_amount->referral_code != null) {
                    //countable as a referrals bonus
                    $refer_user = User::whereUsername($invest_amount->referral_code)->get()->first();

                    $tokens = -1;
                    $coupons = -1;

                    switch ($refer_user->token->referral_bonus_earned) {
                        case 0:
                            $tokens = $refer_user->invest_amount->package->referral_bonus['first_level'] / 2.0;
                            $coupons = $refer_user->invest_amount->package->referral_bonus['first_level'] / 2.0;
                            break;

                        case 1:
                            $tokens = $refer_user->invest_amount->package->referral_bonus['second_level'] / 2.0;
                            $coupons = $refer_user->invest_amount->package->referral_bonus['second_level'] / 2.0;
                            break;

                        case 2:
                            $tokens = $refer_user->invest_amount->package->referral_bonus['third_level'] / 2.0;
                            $coupons = $refer_user->invest_amount->package->referral_bonus['third_level'] / 2.0;
                            break;

                        default:
                            $tokens = -1;
                            $coupons = -1;
                    }

                    if ($tokens != -1) {

                        $user_token = $refer_user->token;
                        $user_token->increment('balance', $tokens);
                        $user_token->increment('referral_bonus_earned');
                        $user_token->save();

                        $c = new Coupon();
                        $c->user_id = $refer_user->id;
                        $c->amount = $coupons;
                        $c->start = Carbon::today();
                        $c->end = Carbon::today()->addDay(10);
                        $c->note = "Referral bonus from {$user->username}";
                        $c->save();

                    }

                }

            });*/

        return $form;
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }
}

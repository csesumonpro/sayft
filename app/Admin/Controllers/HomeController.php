<?php

namespace App\Admin\Controllers;

use App\AdditionalReturn;
use App\Http\Controllers\Controller;
use App\InvestAmount;
use App\User;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Encore\Admin\Layout\Content;
use Encore\Admin\Widgets\InfoBox;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(Content $content)
    {

        $uesrs = InvestAmount::whereStatus('paid')->get();
        $sum = 0.00;
        foreach ($uesrs as $item) {
            $sum += $item->package->amount;
        }

        $members = new InfoBox('Total Members', 'users', 'aqua', '/admin/users', User::all()->count());
        $invest = new InfoBox('Total Invest', 'gbp', 'aqua', '/admin/users', $sum);
        $return = new InfoBox('Total Return', 'gbp', 'aqua', '/admin/additional_returns', AdditionalReturn::all()->sum('amount'));
        return $content
            ->header('Dashboard')
            ->description('dashboard info')
            ->row(function ($row) {
                $row->column(12, '<div></div>');
            })
            ->row(function ($row) use ($members, $invest, $return) {
                $row->column(4, $members->render());
                $row->column(4, $invest);
                $row->column(4, $return);
            });
    }

    public function returns(Content $content)
    {
        /*$investors = InvestAmount::all()->toArray();

        $table = new Table($headers, $investors);*/

        return $content
            ->header('Additional Returns')
            ->description('Additional returns for all')
            ->body(view('admin.additional_returns'));
    }

    public function userReturns(Content $content, $username)
    {
        $investor = User::where('username', $username)->get()->first();

        $results = [];

        if ($investor->invest_amount->status == 'paid') {
            $start = Carbon::parse($investor->invest_amount->paid_at);
            $end = Carbon::parse($investor->invest_amount->paid_at)->addYears(3);
            $period = CarbonPeriod::create($start, '1 month', $end);

            foreach ($period as $dt) {
                $results[] = $dt->format("F - y");
            }
        }

        return $content
            ->header($username)
            ->description('Additional returns for ' . $username)
            ->body(view('admin.investor_details', compact('investor', 'results')));
    }

    public function userReturnsPost(Request $request, $username)
    {
        $investor = User::where('username', $username)->get()->first();

//        dd($request->all());

        $request->validate([
            'amount' => 'required',
            'month' => 'required'
        ]);

        $month = Carbon::parse($request->month);

        $data = $request->only('amount');
        $data['month'] = $month;
        $data['user_id'] = $investor->id;

        if (AdditionalReturn::create($data)) {
            //done
        } else {
//            error
            return back();
        }

        return redirect()->route('user.returns.details', $investor->username);
    }
}

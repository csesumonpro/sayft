<?php

namespace App\Admin\Controllers;

use App\Http\Controllers\Controller;
use App\Package;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class PackageController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Packages')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Package);

        $grid->id('Id');
        $grid->title('Package Name');
        $grid->amount('Amount');
        $grid->referral_bonus('Referral bonus');
        $grid->additional_returns('Returns');
        $grid->total_months('Total months');
        $grid->badge('Badge')->image(asset('storage' . '/'), 50, 50);
//        $grid->created_at('Created at');
        $grid->updated_at('Updated at');

        return $grid;
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Package::findOrFail($id));

        $show->id('Id');
        $show->title('Title');
        $show->amount('Amount');
        $show->referral_bonus('Referral bonus');
        $show->total_months('Total months');
        $show->created_at('Created at');
        $show->updated_at('Updated at');

        return $show;
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Package);

        $form->text('title', 'Package Name')->placeholder('Package Name');
        $form->currency('amount', 'Invest Amount')->symbol('$');
        $form->number('total_months', 'Total months');
        $form->text('additional_returns', 'Additional Returns / Month')->placeholder('eg. $150 - $300');
        $form->image('badge', 'Badge')->required();

        $form->embeds('referral_bonus', function ($form) {

            $form->currency('first_level')->symbol('$')->rules('required');
            $form->currency('second_level')->symbol('$')->rules('required');
            $form->currency('third_level')->symbol('$')->rules('required');

        });

        return $form;
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }
}

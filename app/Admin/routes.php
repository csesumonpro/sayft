<?php

use Illuminate\Routing\Router;

Admin::registerAuthRoutes();

Route::group([
    'prefix' => config('admin.route.prefix'),
    'namespace' => config('admin.route.namespace'),
    'middleware' => config('admin.route.middleware'),
], function (Router $router) {

    $router->get('/', 'HomeController@index');

    //investors
    $router->resource('transactions', TransactionController::class);


    //investors
    $router->resource('users', InvestorController::class);

    //packages
    $router->resource('packages', PackageController::class);

    //news
    $router->resource('news', NewsController::class);

    //supports
    $router->resource('supports', SupportController::class);

    //training contents
    $router->resource('training_contents', TrainingContentController::class);

    //additional_returns
    $router->get('additional_returns', 'HomeController@returns');
    $router->get('additional_returns/{username}', 'HomeController@userReturns')->name('user.returns.details');
    $router->post('additional_returns/{username}', 'HomeController@userReturnsPost')->name('user.returns.post');

});

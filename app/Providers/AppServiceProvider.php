<?php

namespace App\Providers;

use App\InvestAmount;
use App\Observers\InvestAmountObserver;
use App\Observers\TransactionObserver;
use App\Transaction;
use Illuminate\Support\ServiceProvider;
use Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        //
        InvestAmount::observe(InvestAmountObserver::class);
        Transaction::observe(TransactionObserver::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}

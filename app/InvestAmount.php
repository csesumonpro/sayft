<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvestAmount extends Model
{
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function package()
    {
        return $this->belongsTo(Package::class);
    }
}

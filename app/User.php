<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'username', 'phone', 'email', 'password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function invest_amount()
    {
        return $this->hasOne(InvestAmount::class);
    }

    public function token()
    {
        return $this->hasOne(Token::class);
    }

    public function coupons()
    {
        return $this->hasMany(Coupon::class);
    }

    public function getNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function additional_returns()
    {
        return $this->hasMany(AdditionalReturn::class);
    }

    public function getReferralsAttribute()
    {
        return User::whereUsername($this->username)->get();
    }

    public function to_me()
    {
        return $this->hasMany(Transaction::class, 'to_id');
    }

    public function from_me()
    {
        return $this->hasMany(Transaction::class, 'from_id');
    }

    public function supports()
    {
        return $this->hasMany(Support::class);
    }
}

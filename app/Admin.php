<?php

namespace App;

use Encore\Admin\Auth\Database\Administrator;
use Illuminate\Notifications\Notifiable;

class Admin extends Administrator
{
    use Notifiable;
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    protected $casts = [
        'referral_bonus' => 'array',
    ];

    public function invest_amounts()
    {
        return $this->hasMany(InvestAmount::class);
    }
}

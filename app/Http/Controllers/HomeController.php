<?php

namespace App\Http\Controllers;

use App\Coupon;
use App\News;
use App\Support;
use App\TrainingContent;
use App\Transaction;
use App\User;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function verifyRequest(Request $request)
    {

        $request->validate([
            'verification_type' => 'required',
            'verification_image' => 'mimes:jpeg,jpg,png,gif | max:1000',
        ]);
        $user = User::find(Auth::user()->id);
        $file = $request->file('verification_image');
        if ($file != null) {
            if ($user->verification_image != null) {
                unlink('storage/' . $user->verification_image);
            }
            $name = time() . '_' . $file->getClientOriginalExtension();
            $upload_path = 'storage/images/';
            $file->move($upload_path, $name);
            $image = "images/" . $name;
        } else {
            $image = $user->verification_image;
        }

        $user->verification_type = $request->verification_type;
        $user->verification_image = $image;
        $user->verification_status = 'pending';
        $user->save();
        return redirect()->back();
    }
    public function verifyAccount()
    {
        return view('dashboard.verification');
    }
    public function userProfile()
    {
        $user = User::find(Auth::user()->id);
        return view('dashboard.profile', compact('user'));
    }
    public function profileUpdate(Request $request)
    {

        $user = User::find(Auth::user()->id);
        $request->validate([
            'first_name' => 'required|min:2|max:100',
            'last_name' => 'required|min:2|max:100',
            'phone' => 'required|min:9|max:20',
            'email' => 'required|email|unique:users,email,' . $user->id,
            'photo' => 'mimes:jpeg,jpg,png,gif | max:1000',
        ]);

        $file = $request->file('photo');
        if ($file != null) {
            if ($user->photo != null) {
                unlink('storage/' . $user->photo);
            }
            $name = time() . '_' . $file->getClientOriginalExtension();
            $upload_path = 'storage/images/';
            $file->move($upload_path, $name);
            $image = "images/" . $name;
        } else {
            $image = $user->photo;
        }

        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->phone = $request->phone;
        $user->email = $request->email;
        $user->photo = $image;
        $user->save();
        return redirect()->back();
    }
    public function changePassword()
    {
        return view('dashboard.changepassword');
    }
    public function updatePassword(Request $request)
    {
        $this->validate($request, [
            'old_password' => 'required|min:6|max:20',
            'password' => 'required|min:6|different:old_password|max:20',
            'password_confirmation' => 'required|min:6|same:password|max:20',
        ]);

        $user = User::find(Auth::user()->id);
        if (password_verify($request->old_password, $user->password)) {
            $user->password = bcrypt($request->password);
            $user->save();
            return back()->with('message_success', 'Password Changed Successfully..');
        } else {
            return back()->with('message_error', 'Old Password Incorrect..!');
        }
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function referrals()
    {
        $jorg = new JOrgChart();
        $response = $jorg->getChart(Auth::user());
//        dd($response);
        return view('referrals', ['tree' => $response['tree_string']]);
    }

    public function transactions($id = null)
    {
        $transactions = Transaction::orWhere('to_id', Auth::user()->id)->orWhere('from_id', Auth::user()->id)->latest()->get();
        return view('dashboard.transactions', compact('transactions', 'id'));
    }

    public function withdraws()
    {
        $coupons = Auth::user()->coupons()->whereDate('start', '>=', Carbon::today())->get();
        $token = Auth::user()->token->balance;
        return view('dashboard.withdraws', compact('coupons', 'token'));
    }

    public function guideline()
    {
        $guidelines = TrainingContent::latest()->get();
        return view('dashboard.guideline', compact('guidelines'));
    }

    public function withdrawsForm($type, $amount_type, $coupon = null)
    {
        if (!$type || !$amount_type) {
            return back();
        }

        if ($coupon) {
            $coupon = Coupon::find($coupon);
        }

        return view('dashboard.withdraws-form', compact('coupons', 'type', 'amount_type', 'coupon'));
    }

    public function read($id, $type = 'guide')
    {
        if ($type == 'news') {
            $item = News::find($id);
        } else {
            $item = TrainingContent::find($id);
        }
        return view('dashboard.read', compact('type', 'item'));
    }

    public
        function withdrawsFormPost(Request $request, $type, $amount_type, Coupon $coupon)
    {
        if (!$type || !$amount_type) {
            return back();
        }

        if ($coupon) {
            $request->validate([
                'username' => 'exists:users',
                'amount' => "numeric|max:{$coupon->amount}"
            ]);

            $to = User::where('username', $request->username)->get()->first();
            //coupon
            $trans = new Transaction();
            $trans->type = $type;
            $trans->amount_type = $amount_type;
            $trans->amount = $request->amount;
            $trans->status = 'pending';
            $trans->note = "Gift to {$to->username}";
            $trans->to_id = $type == 'gift' ? $to->id : null;
            $trans->from_id = Auth::user()->id;
            $trans->coupon_id = $coupon->id;

            if ($trans->save()) {
                flash('Your gift request is processing. Your will be notified when done')->success();
                return redirect()->route('dashboard.dash');
            }

            flash('Your gift request processing failed. Try again')->error();
            return back();
        }

        if ($amount_type == 'token') {
            $max = Auth::user()->token->balance;
        } else {
            $max = Auth::user()->coupons()->whereDate('start', '>=', Carbon::today())->get()->sum('amount');
        }

        $request->validate([
            'username' => 'exists:users.username',
            'amount' => "numeric|max:{$max}"
        ]);

        if ($request->has('username')) {
            $to = User::where('username', $request->username)->get()->first();
            //coupon
            $trans = new Transaction();
            $trans->type = $type;
            $trans->amount_type = $amount_type;
            $trans->amount = $request->amount;
            $trans->status = 'pending';
            $trans->note = "Gift to {$to->username}";
            $trans->to_id = $type == 'gift' ? $to->id : null;
            $trans->from_id = Auth::user()->id;
            if ($trans->save()) {
                return redirect()->route('dashboard.dash');
            }
        } else {
            //coupon
            $trans = new Transaction();
            $trans->type = $type;
            $trans->amount_type = $amount_type;
            $trans->amount = $request->amount;
            $trans->status = 'pending';
            $trans->note = "Withdraw request";
            $trans->to_id = null;
            $trans->from_id = Auth::user()->id;
            if ($trans->save()) {
                return redirect()->route('dashboard.dash');
            }
        }

        return back();

    }

    public function referralList()
    {
        $jorg = new JOrgChart();
        $response = $jorg->getChart(Auth::user());
        $tree = $response['tree_string'];

        return view('dashboard.referrals', compact('tree'));
    }

    public function dashboard()
    {
        $news = News::latest()->get();
        $coupons = Auth::user()->coupons()->whereDate('start', '>=', Carbon::today())->get();
        $token = Auth::user()->token->balance;
        $totalReturn = Auth::user()->additional_returns()->sum('amount') ?? 0.00;
        $totalEarn = $coupons->sum('amount') + $token + $totalReturn;
        return view('dashboard.dashboard', compact('coupons', 'token', 'totalReturn', 'news', 'totalEarn'));
    }

    public function readAll(Request $request)
    {
        Auth::user()->unreadNotifications->markAsRead();
        return response()->json(['code' => 200]);
    }

    public function support()
    {
        return view('dashboard.support');
    }

    public function supportPost(Request $request)
    {
        $request->validate([
            'subject' => 'required',
            'description' => 'required|min:10'
        ]);

//        dd($request->all());

        $support = new Support();
        $support->subject = $request->subject;
        $support->description = $request->description;
        $support->user_id = Auth::user()->id;

        if ($support->save()) {
            return redirect()->route('dashboard.dash');
        }

        return back()->withInput();
    }
}

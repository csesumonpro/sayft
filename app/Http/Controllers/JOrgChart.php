<?php
/**
 * Created by PhpStorm.
 * User: anwar
 * Date: 11/27/18
 * Time: 11:47 PM
 */

namespace App\Http\Controllers;


use App\InvestAmount;
use App\User;


class JOrgChart
{
    public $chain = [];
    public $tree_string = "";

    public function getChart(User $user)
    {

        $users = InvestAmount::where('referral_code', $user->username)->get()->pluck('id', 'id');

//        dd($users);

        $this->chain[$user->id] = $users;

        //[1]=>[2 ,3]
        $this->recursiveTree($users, $this->chain[$user->id]);

        return ['tree_array' => $this->chain, 'tree_string' => $this->tree_string];
    }

    public function recursiveTree($referral_ids, &$arr)
    {

        $this->tree_string .= "<ul>";

        // [1]=>[2,3] $arr is at [1]
        foreach ($referral_ids as $id) {

            //index 2
//            $referrals = DB::table('user_referrals')->where('referral_id', $referral_ids[$i])->where('user_id', '>', 0)->lists('user_id');
            $user = InvestAmount::find($id)->user;
            $referrals = InvestAmount::where('referral_code', $user->username)->get()->pluck('id', 'id');

            //index 2 users 4,5
            //by reference on 2 nd array

            $badge = asset('storage' . '/' . $user->invest_amount->package->badge);

            $this->tree_string .= "<li title='username: $user->name, email: $user->email'><label>$user->username</label><br><img src='{$badge}' alt='$user->username' width='100px' height='100px'>";

            $arr[$id] = [];

            $arr[$id] = $referrals;

            if (count($referrals) > 0) {
                //[2]=>[4,5] reference [2]=>
                $this->recursiveTree($referrals, $arr[$id]);
            }

            $this->tree_string .= "</li>";

        }

        $this->tree_string .= "</ul>";
    }
}

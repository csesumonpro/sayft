<?php

namespace App\Observers;

use App\Admin;
use App\Coupon;
use App\Notifications\NewTransaction;
use App\Notifications\UpdatedTransaction;
use App\Transaction;

class TransactionObserver
{
    /**
     * Handle the transaction "created" event.
     *
     * @param  \App\Transaction $transaction
     * @return void
     */
    public function created(Transaction $transaction)
    {
        //sending to admin
        \Notification::send(Admin::all(), new NewTransaction($transaction));
    }

    /**
     * Handle the transaction "updated" event.
     *
     * @param  \App\Transaction $transaction
     * @return void
     */
    public function updated(Transaction $transaction)
    {
        $changes = $transaction->isDirty() ? $transaction->getDirty() : false;

        if ($changes) {
            foreach ($changes as $attr => $value) {
                //check for status attribute has been changed
                if ($attr == 'status') {
                    if ($transaction->getOriginal($attr) != 'accepted' && $transaction->$attr == 'accepted') {
                        \DB::transaction(function () use ($transaction) {
                            if ($transaction->type == 'gift') {
                                $to = $transaction->to_user;
                                $from = $transaction->from_user;
                                if ($transaction->amount_type == 'token') {
                                    //token gifted
                                    $from->token->decrement('balance', $transaction->amount)->save();
                                    $to->token->increment('balance', $transaction->amount)->save();
                                } elseif ($transaction->amount_type == 'coupon') {
                                    //coupon gifted
                                    $coupon = $transaction->coupon;
                                    $c = new Coupon();
                                    $c->user_id = $to->id;
                                    $c->amount = $coupon->amount;
                                    $c->start = $coupon->start;
                                    $c->end = $coupon->end;
                                    $c->note = "Received {$from->username}";
                                    $c->save();

                                    $transaction->coupon->delete();
                                }
                            } elseif ($transaction->type == 'withdraw') {
                                if ($transaction->amount_type == 'token') {
                                    $transaction->from_user->token->decrement('balance', $transaction->amount)->save();
                                } elseif ($transaction->amount_type == 'coupon') {
                                    //coupon withdraw options
                                }
                            }
                        }, 5);

                        if ($transaction->from_user) {
                            $transaction->from_user->notify(new UpdatedTransaction($transaction));
                        }

                        if ($transaction->to_user) {
                            $transaction->to_user->notify(new UpdatedTransaction($transaction));
                        }
                    } elseif ($transaction->getOriginal($attr) != 'rejected' && $transaction->$attr == 'rejected') {
                        if ($transaction->from_user) {
                            $transaction->from_user->notify(new UpdatedTransaction($transaction));
                        }

                        if ($transaction->to_user) {
                            $transaction->to_user->notify(new UpdatedTransaction($transaction));
                        }
                    }
                }
            }
        }

    }

    /**
     * Handle the transaction "deleted" event.
     *
     * @param  \App\Transaction $transaction
     * @return void
     */
    public function deleted(Transaction $transaction)
    {
        //
    }

    /**
     * Handle the transaction "restored" event.
     *
     * @param  \App\Transaction $transaction
     * @return void
     */
    public function restored(Transaction $transaction)
    {
        //
    }

    /**
     * Handle the transaction "force deleted" event.
     *
     * @param  \App\Transaction $transaction
     * @return void
     */
    public function forceDeleted(Transaction $transaction)
    {
        //
    }
}

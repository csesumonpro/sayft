<?php

namespace App\Observers;

use App\Coupon;
use App\InvestAmount;
use App\Token;
use App\Transaction;
use App\User;
use Carbon\Carbon;

class InvestAmountObserver
{
    /**
     * Handle the invest amount "created" event.
     *
     * @param  \App\InvestAmount $investAmount
     * @return void
     */
    public function created(InvestAmount $investAmount)
    {
//        Log::info('tirggered', $investAmount);


        $user = $investAmount->user;

        //

        $user->token()->save(new Token());


        if ($investAmount->status == 'paid') {

            //first
            //
            $first = User::where('username', $investAmount->referral_code)->get()->first();

            if (!$first) {
                return;
            }

            $amount = $user->invest_amount->package->referral_bonus['first_level'] / 2.0;

            $user_token = $first->token;
            $user_token->increment('balance', $amount);
            $user_token->save();

            $trans = new Transaction();
            $trans->type = 'referral_in';
            $trans->amount_type = 'token';
            $trans->amount = $amount;
            $trans->status = 'accepted';
            $trans->note = "Referral bonus from {$user->username}";
            $trans->to_id = $first->id;
            $trans->from_id = $user->id;
            $trans->save();


            //
            $c = new Coupon();
            $c->user_id = $first->id;
            $c->amount = $amount;
            $c->start = Carbon::today();
            $c->end = Carbon::today()->addDay(30);
            $c->note = "Referral bonus from {$user->username}";
            $c->save();

            //coupon
            $trans = new Transaction();
            $trans->type = 'referral_in';
            $trans->amount_type = 'coupon';
            $trans->amount = $amount;
            $trans->status = 'accepted';
            $trans->note = "Referral bonus from {$user->username}";
            $trans->to_id = $first->id;
            $trans->from_id = $user->id;
            $trans->save();

            //second
            //
            $second = User::where('username', $first->invest_amount->referral_code)->get()->first();

            if (!$second) {
                return;
            }
            $amount = $user->invest_amount->package->referral_bonus['second_level'] / 2.0;

            $user_token = $second->token;
            $user_token->increment('balance', $amount);
            $user_token->save();

            $trans = new Transaction();
            $trans->type = 'referral_in';
            $trans->amount_type = 'token';
            $trans->amount = $amount;
            $trans->status = 'accepted';
            $trans->note = "Referral bonus from {$user->username}";
            $trans->to_id = $second->id;
            $trans->from_id = $user->id;
            $trans->save();


            //
            $c = new Coupon();
            $c->user_id = $second->id;
            $c->amount = $amount;
            $c->start = Carbon::today();
            $c->end = Carbon::today()->addDay(30);
            $c->note = "Referral bonus from {$user->username}";
            $c->save();

            //coupon
            $trans = new Transaction();
            $trans->type = 'referral_in';
            $trans->amount_type = 'coupon';
            $trans->amount = $amount;
            $trans->status = 'accepted';
            $trans->note = "Referral bonus from {$user->username}";
            $trans->to_id = $second->id;
            $trans->from_id = $user->id;
            $trans->save();

            //
            //third
            $third = User::where('username', $second->invest_amount->referral_code)->get()->first();
            if (!$third) {
                return;
            }
            $amount = $user->invest_amount->package->referral_bonus['third_level'] / 2.0;

            $user_token = $third->token;
            $user_token->increment('balance', $amount);
            $user_token->save();

            $trans = new Transaction();
            $trans->type = 'referral_in';
            $trans->amount_type = 'token';
            $trans->amount = $amount;
            $trans->status = 'accepted';
            $trans->note = "Referral bonus from {$user->username}";
            $trans->to_id = $third->id;
            $trans->from_id = $user->id;
            $trans->save();


            //
            $c = new Coupon();
            $c->user_id = $third->id;
            $c->amount = $amount;
            $c->start = Carbon::today();
            $c->end = Carbon::today()->addDay(30);
            $c->note = "Referral bonus from {$user->username}";
            $c->save();

            //coupon
            $trans = new Transaction();
            $trans->type = 'referral_in';
            $trans->amount_type = 'coupon';
            $trans->amount = $amount;
            $trans->status = 'accepted';
            $trans->note = "Referral bonus from {$user->username}";
            $trans->to_id = $third->id;
            $trans->from_id = $user->id;
            $trans->save();

        }
    }

    /**
     * Handle the invest amount "updated" event.
     *
     * @param  \App\InvestAmount $investAmount
     * @return void
     */
    public function updated(InvestAmount $investAmount)
    {
        $user = $investAmount->user;
        $changes = $investAmount->isDirty() ? $investAmount->getDirty() : false;

        if ($changes) {
            foreach ($changes as $attr => $value) {
                if ($attr == 'status') {
                    if ($investAmount->getOriginal($attr) != 'paid' && $investAmount->$attr == 'paid') {
                        $first = User::where('username', $investAmount->referral_code)->get()->first();

                        if (!$first) {
                            return;
                        }

                        $amount = $user->invest_amount->package->referral_bonus['first_level'] / 2.0;

                        $user_token = $first->token;
                        $user_token->increment('balance', $amount);
                        $user_token->save();

                        $trans = new Transaction();
                        $trans->type = 'referral_in';
                        $trans->amount_type = 'token';
                        $trans->amount = $amount;
                        $trans->status = 'accepted';
                        $trans->note = "Referral bonus from {$user->username}";
                        $trans->to_id = $first->id;
                        $trans->from_id = $user->id;
                        $trans->save();


                        //
                        $c = new Coupon();
                        $c->user_id = $first->id;
                        $c->amount = $amount;
                        $c->start = Carbon::today();
                        $c->end = Carbon::today()->addDay(30);
                        $c->note = "Referral bonus from {$user->username}";
                        $c->save();

                        //coupon
                        $trans = new Transaction();
                        $trans->type = 'referral_in';
                        $trans->amount_type = 'coupon';
                        $trans->amount = $amount;
                        $trans->status = 'accepted';
                        $trans->note = "Referral bonus from {$user->username}";
                        $trans->to_id = $first->id;
                        $trans->from_id = $user->id;
                        $trans->save();

                        //second
                        //
                        $second = User::where('username', $first->invest_amount->referral_code)->get()->first();

                        if (!$second) {
                            return;
                        }
                        $amount = $user->invest_amount->package->referral_bonus['second_level'] / 2.0;

                        $user_token = $second->token;
                        $user_token->increment('balance', $amount);
                        $user_token->save();

                        $trans = new Transaction();
                        $trans->type = 'referral_in';
                        $trans->amount_type = 'token';
                        $trans->amount = $amount;
                        $trans->status = 'accepted';
                        $trans->note = "Referral bonus from {$user->username}";
                        $trans->to_id = $second->id;
                        $trans->from_id = $user->id;
                        $trans->save();


                        //
                        $c = new Coupon();
                        $c->user_id = $second->id;
                        $c->amount = $amount;
                        $c->start = Carbon::today();
                        $c->end = Carbon::today()->addDay(30);
                        $c->note = "Referral bonus from {$user->username}";
                        $c->save();

                        //coupon
                        $trans = new Transaction();
                        $trans->type = 'referral_in';
                        $trans->amount_type = 'coupon';
                        $trans->amount = $amount;
                        $trans->status = 'accepted';
                        $trans->note = "Referral bonus from {$user->username}";
                        $trans->to_id = $second->id;
                        $trans->from_id = $user->id;
                        $trans->save();

                        //
                        //third
                        $third = User::where('username', $second->invest_amount->referral_code)->get()->first();
                        if (!$third) {
                            return;
                        }
                        $amount = $user->invest_amount->package->referral_bonus['third_level'] / 2.0;

                        $user_token = $third->token;
                        $user_token->increment('balance', $amount);
                        $user_token->save();

                        $trans = new Transaction();
                        $trans->type = 'referral_in';
                        $trans->amount_type = 'token';
                        $trans->amount = $amount;
                        $trans->status = 'accepted';
                        $trans->note = "Referral bonus from {$user->username}";
                        $trans->to_id = $third->id;
                        $trans->from_id = $user->id;
                        $trans->save();


                        //
                        $c = new Coupon();
                        $c->user_id = $third->id;
                        $c->amount = $amount;
                        $c->start = Carbon::today();
                        $c->end = Carbon::today()->addDay(30);
                        $c->note = "Referral bonus from {$user->username}";
                        $c->save();

                        //coupon
                        $trans = new Transaction();
                        $trans->type = 'referral_in';
                        $trans->amount_type = 'coupon';
                        $trans->amount = $amount;
                        $trans->status = 'accepted';
                        $trans->note = "Referral bonus from {$user->username}";
                        $trans->to_id = $third->id;
                        $trans->from_id = $user->id;
                        $trans->save();
                    }
                }
                \Log::info("updated invest $attr from {$investAmount->getOriginal($attr)} to {$investAmount->$attr}");
            }
        }
    }

    /**
     * Handle the invest amount "deleted" event.
     *
     * @param  \App\InvestAmount $investAmount
     * @return void
     */
    public function deleted(InvestAmount $investAmount)
    {
        //
    }

    /**
     * Handle the invest amount "restored" event.
     *
     * @param  \App\InvestAmount $investAmount
     * @return void
     */
    public function restored(InvestAmount $investAmount)
    {
        //
    }

    /**
     * Handle the invest amount "force deleted" event.
     *
     * @param  \App\InvestAmount $investAmount
     * @return void
     */
    public function forceDeleted(InvestAmount $investAmount)
    {
        //
    }
}

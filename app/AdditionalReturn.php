<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdditionalReturn extends Model
{
    protected $fillable = ['user_id', 'amount', 'month'];
}

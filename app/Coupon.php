<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{

    protected $fillable = ['amount', 'start', 'end', 'note'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
